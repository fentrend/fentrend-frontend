function setUp6Columns(data){
	data.addColumn('number', 'Avg. Cost / Sq Foot'); 
 data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
 data.addColumn( {'type': 'string', 'role': 'style'});
data.addColumn('number', 'PVC/uPVC');
data.addColumn('number', 'PVC/uPVC');
data.addColumn('number', 'Wood');
data.addColumn('number', 'Clad Wood');
data.addColumn('number', 'Aluminum');
data.addColumn('number', 'Steel'); 
data.addColumn('number', 'Not Selected'); 
       data.addRows([ 
				/**PVC/uPVC**/
             [72,createCustomHTMLContent( 'Zola Product 1' , 'PVC/uPVC', '$72', '1.103') ,null,  1.103 , null , null , null , null , null ,null],		       
			[ 73, createCustomHTMLContent( 'Zola, Product 1', 'PVC/uPVC' ,'$73' ,'6.65' ),null, 6.65 , null , null , null , null , null ,null ],
			[  84, createCustomHTMLContent('Zola, Product 4', 'PVC/uPVC' ,'$84', '0.65' ),null,  0.65 , null , null , null , null , null ,null ], 
			
			
			/**PVC/uPVC**/
		 [71,createCustomHTMLContent('Tanner Product 1', 'PVC/uPVC', '$71','5.503'),null,null , 5.503 ,  null , null , null , null ,null],  
         [72, createCustomHTMLContent('Zola, Product 1', 'PVC/uPVC' , '$72' ,'8.35') ,null, null , 8.35 ,  null , null , null , null ,null ],
		  [  78, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$78' ,'9.66'),null, null , 9.66 ,  null , null , null , null ,null ],         
		  [83,createCustomHTMLContent('Zola, Product 4', 'PVC/uPVC' , '$83','7.35'),null, null , 7.35 ,  null , null , null , null  ,null],
		
		/**Wood*/
		[71.75, createCustomHTMLContent('Zola, Product 1', 'Wood','$71', '2.35') ,null,  null  , null , 2.35  , null , null , null ,null ],
		[77, createCustomHTMLContent( 'Synergist, Product 1', 'Wood', '$77','4.35'),null, null  , null , 4.35  , null , null , null  ,null],
		[ 81,createCustomHTMLContent('Synergist Product 1', 'Wood' , '$81', '6.668') ,null, null  , null , 6.668  , null , null , null ,null],
		[82,createCustomHTMLContent('Zola, Product 1', 'Wood' , '$82', '3.35') ,null,  null  , null , 3.35  , null , null , null   ,null],  
		
		
		/**Clad Wood**/
		[60,  createCustomHTMLContent('Zola, Product 5', 'Clad Wood' , '$60' ,'7.75') , null,null  , null  , null , 7.75 , null , null ,null] ,
		 [61,  createCustomHTMLContent('Zola, Product 5', 'Clad Wood' , '$61' ,'8.75') ,null ,null  , null  , null , 8.75 , null , null ,null ] ,
		[68,  createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$68' ,'7.75') ,null, null  , null  , null , 7.75 , null , null ,null ] , 
         [ 70,  createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$70','7.75' ),null, null  , null  , null , 7.75 , null , null  ,null ],
		[76, createCustomHTMLContent('Synergist, Product 1', 'Wood' , '$76','7.75'),null, null  , null  , null , 7.75 , null , null  ,null],
		[81,createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$92','7.75'),null,null  , null  , null , 9.75 , null , null  ,null ],          
		[ 87, createCustomHTMLContent('Synergist Product 1', 'Clad Wood', '$87','7.88' ),null, null  , null  , null , 7.88 , null , null ,null ],
	
			/**Aluminum**/
	      [69.5,  createCustomHTMLContent('Zola, Product 5', 'Aluminum','$69.5' ,'3.35'),null, null  , null  , null ,  null , 3.35 ,  null ,null ]  ,
			[69,  createCustomHTMLContent('Zola, Product 5', 'Aluminum','$69' ,'6.35'),null, null  , null  , null ,  null , 6.35 ,  null ,null ] , 
		 [74,  createCustomHTMLContent('Zola, Product 1', 'Aluminum','$74', '6.99 ') ,null, null  , null  , null ,  null , 6.99  , null,null],
          [74.5,  createCustomHTMLContent('Zola, Product 1', 'Aluminum','$74', '3.99 ') ,null, null  , null  , null ,  null , 3.99  , null,null],
         [ 75, createCustomHTMLContent('Zola, Product 1','Aluminum', '$75', '3.99'),null,null  , null  , null ,  null , 3.99  , null  ,null],
         [ 75.5, createCustomHTMLContent('Zola, Product 1','Aluminum', '$75', '7.99'),null,null  , null  , null ,  null , 7.99  , null  ,null],
		 [80, createCustomHTMLContent('Zola, Product 1', 'Aluminum','$80','3.44'),null, null  , null  , null ,  null , 3.44    , null  ,null],
		 [ 85, createCustomHTMLContent( 'Zola, Product 4', 'Aluminum', '$85','8.29'),null, null  , null  , null ,  null , 8.29  , null ,null],
		 [86, createCustomHTMLContent('Zola, Product 1', 'Aluminum','$86', '6.69'),null,null  , null  , null ,  null , 6.69  , null   ,null ],
	
			/**Steel**/
	[ 63, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$63' ,'9.49'),null, null  , null  , null ,  null ,  null ,9.49  ,null  ],        	 
    [ 77, createCustomHTMLContent( 'Tanner, Product 1', 'Steel' , '$77' ,'7.14' ),null, null  , null  , null ,  null ,  null ,7.14 ,null  ],	  
     [ 79, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$79' ,'4.49'),null, null  , null  , null ,  null ,  null , 4.49  ,null  ] ,	 
		   
	/**Unselected**/
	[ 60, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$60' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null , 6.8 ],
	[  61, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$61' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 5.89  ] ,      
	[ 62, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$62' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null , 6.3 ],
    [  63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 5.19  ] ,     
    [ 63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null , 6.3 ],       
     [ 64, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$64' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null ,  4.02 ],     
     [  65, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$65' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 4.20  ] ,          
      [  66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 5.29  ] , 
          [  66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'7.59'),null, null  , null  , null ,  null ,  null   ,null, 7.39  ] ,         
                 
         [ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null , 6.6 ],
                  [ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null , 6.6 ],    
       [ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'8.0'),null, null  , null  , null ,  null ,  null   ,null , 8.50 ],

       [  69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 5.7  ] ,    
         [  69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 5.9  ] ,   
           [  69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'7.59'),null, null  , null  , null ,  null ,  null   ,null, 7.8  ] ,     
         [ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.3'),null, null  , null  , null ,  null ,  null   ,null , 2.3 ],
           [ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'8.0'),null, null  , null  , null ,  null ,  null   ,null ,3.0 ],       
            [ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null , 6.0 ],
       
                       
         [  71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 4.19  ] ,      
         [  71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'7.19'),null, null  , null  , null ,  null ,  null   ,null, 5.19  ] , 
          [  71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 6.19  ] ,     
         [ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null , 7.1 ],
           [ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'8.1'),null, null  , null  , null ,  null ,  null   ,null , 8.1 ],
         [ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null , 9.1 ],
          [  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null,  2.19  ] ,  
          [  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 3.19  ] ,    
          [  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'7.19'),null, null  , null  , null ,  null ,  null   ,null, 4.19  ] ,   
          [ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null , 6.1 ],
            [ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'8.1'),null, null  , null  , null ,  null ,  null   ,null , 8.1 ],      
  
         [  75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 4.49  ] ,       
          [  75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.49'),null, null  , null  , null ,  null ,  null   ,null, 5.49  ] ,
          [  75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null  , null ,  null ,  null   ,null, 6.49  ] ,  
                   [ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null ,  4.4 ],         
         [ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'66.4'),null, null  , null  , null ,  null ,  null   ,null ,  6.4 ],
         [ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  , null  , null ,  null ,  null   ,null ,  8.4 ],
           [  77 , createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$77' ,'7.59'),null, null  , null  , null ,  null ,  null   ,null, 7.59  ] ,           
         [ 78, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$78' ,'8.0'),null, null  , null  , null ,  null ,  null   ,null , 8.0 ],
          [  79, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$79' ,'7.59'),null, null  , null  , null ,  null ,  null   ,null, 7.59  ] ,  
           [ 80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'8.0'),null, null  , null  , null ,  null ,  null   ,null ,8.4 ],
          [  80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'7.19'),null, null  , null  , null ,  null ,  null   ,null, 7.19  ] ,      
           [ 81, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$81' ,'8.1'),null, null  , null  , null ,  null ,  null   ,null , 2.1 ],
             [  82, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$82' ,'7.19'),null, null  , null  , null ,  null ,  null   ,null, 7.19  ] , 
          [ 83, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$83' ,'8.1'),null, null  , null  , null ,  null ,  null   ,null , 8.1 ],
          [  84, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$84' ,'5.49'),null, null  , null  , null ,  null ,  null   ,null, 5.49  ] ,           
		  [85, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$85' ,'66.4'),null, null  , null  , null ,  null ,  null   ,null ,  6.4 ],
            [ 86, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$86' ,'8.0'),null, null  , null  , null ,  null ,  null   ,null ,8.0 ],
          [ 87, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$87' ,'7.19'),null, null  , null  , null ,  null ,  null   ,null, 7.19  ] ,    
		    [ 88, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$88' ,'8.1'),null, null  , null  , null ,  null ,  null   ,null , 1.1 ],
          [  89, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$89' ,'7.19'),null, null  , null  , null ,  null ,  null   ,null, 7.19  ] ,           
         [ 90, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$90' ,'8.1'),null, null  , null  , null ,  null ,  null   ,null , 8.1 ] 
     
 
       
       ]);
}
function setUp5Columns(data){
 
	data.addColumn('number', 'Avg. Cost / Sq Foot'); 
 data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
 data.addColumn( {'type': 'string', 'role': 'style'});
data.addColumn('number', 'PVC/uPVC');
data.addColumn('number', 'Wood');
data.addColumn('number', 'Clad Wood');
data.addColumn('number', 'Aluminum');
data.addColumn('number', 'Steel'); 
data.addColumn('number', 'Not Selected'); 
       data.addRows([ 
				/**PVC/uPVC**/
             [72,createCustomHTMLContent( 'Zola Product 1' , 'PVC/uPVC', '$72', '1.103') ,null,  1.103 , null , null , null , null , null ],		       
			[ 73, createCustomHTMLContent( 'Zola, Product 1', 'PVC/uPVC' ,'$73' ,'6.65' ),null, 6.65 , null , null , null , null , null ],
			[  84, createCustomHTMLContent('Zola, Product 4', 'PVC/uPVC' ,'$84', '0.65' ),null,  0.65 , null , null , null , null , null ], 
			
			
			/**PVC/uPVC**/
		 [71,createCustomHTMLContent('Tanner Product 1', 'PVC/uPVC', '$71','5.503'),null,null , 5.503 ,  null , null , null , null ],  
         [72, createCustomHTMLContent('Zola, Product 1', 'PVC/uPVC' , '$72' ,'8.35') ,null, null , 8.35 ,  null , null , null , null ],
		  [  78, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$78' ,'9.66'),null, null , 9.66 ,  null , null , null , null ],         
		  [83,createCustomHTMLContent('Zola, Product 4', 'PVC/uPVC' , '$83','7.35'),null, null , 7.35 ,  null , null , null , null ],
		
		/**Wood*/
		[71.75, createCustomHTMLContent('Zola, Product 1', 'Wood','$71', '2.35') ,null,  null  , null , 2.35  , null , null , null ],
		[77, createCustomHTMLContent( 'Synergist, Product 1', 'Wood', '$77','4.35'),null, null  , null , 4.35  , null , null , null ],
		[ 81,createCustomHTMLContent('Synergist Product 1', 'Wood' , '$81', '6.668') ,null, null  , null , 6.668  , null , null , null],
		[82,createCustomHTMLContent('Zola, Product 1', 'Wood' , '$82', '3.35') ,null,  null  , null , 3.35  , null , null , null   ],  
		
		
		/**Clad Wood**/
		[60,  createCustomHTMLContent('Zola, Product 5', 'Clad Wood' , '$60' ,'7.75') , null,null  , null  , null , 7.75 , null , null ] ,
		 [61,  createCustomHTMLContent('Zola, Product 5', 'Clad Wood' , '$61' ,'8.75') ,null ,null  , null  , null , 8.75 , null , null  ] ,
		[68,  createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$68' ,'7.75') ,null, null  , null  , null , 7.75 , null , null ] , 
         [ 70,  createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$70','7.75' ),null, null  , null  , null , 7.75 , null , null ],
		[76, createCustomHTMLContent('Synergist, Product 1', 'Wood' , '$76','7.75'),null, null  , null  , null , 7.75 , null , null ],
		[81,createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$92','7.75'),null,null  , null  , null , 9.75 , null , null   ],          
		[ 87, createCustomHTMLContent('Synergist Product 1', 'Clad Wood', '$87','7.88' ),null, null  , null  , null , 7.88 , null , null ],
	
			/**Aluminum**/
	      [69.5,  createCustomHTMLContent('Zola, Product 5', 'Aluminum','$69.5' ,'3.35'),null, null  , null  , null ,  null , 3.35 ,  null ]  ,
			[69,  createCustomHTMLContent('Zola, Product 5', 'Aluminum','$69' ,'6.35'),null, null  , null  , null ,  null , 6.35 ,  null  ] , 
		 [74,  createCustomHTMLContent('Zola, Product 1', 'Aluminum','$74', '6.99 ') ,null, null  , null  , null ,  null , 6.99  , null],
          [74.5,  createCustomHTMLContent('Zola, Product 1', 'Aluminum','$74', '3.99 ') ,null, null  , null  , null ,  null , 3.99  , null],
         [ 75, createCustomHTMLContent('Zola, Product 1','Aluminum', '$75', '3.99'),null,null  , null  , null ,  null , 3.99  , null  ],
         [ 75.5, createCustomHTMLContent('Zola, Product 1','Aluminum', '$75', '7.99'),null,null  , null  , null ,  null , 7.99  , null ],
		 [80, createCustomHTMLContent('Zola, Product 1', 'Aluminum','$80','3.44'),null, null  , null  , null ,  null , 3.44    , null ],
		 [ 85, createCustomHTMLContent( 'Zola, Product 4', 'Aluminum', '$85','8.29'),null, null  , null  , null ,  null , 8.29  , null],
		 [86, createCustomHTMLContent('Zola, Product 1', 'Aluminum','$86', '6.69'),null,null  , null  , null ,  null , 6.69  , null  ],
	
			/**Steel**/
	[ 63, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$63' ,'9.49'),null, null  , null  , null  ,  null ,9.49   ,  null ],        	 
    [ 77, createCustomHTMLContent( 'Tanner, Product 1', 'Steel' , '$77' ,'7.14' ),null, null  , null  , null  ,  null ,7.14  ,  null ],	  
     [ 79, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$79' ,'4.49'),null, null  , null  , null ,  null  , 4.49   ,  null ] ,	 
		   
	/**Unselected**/
	[ 60, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$60' ,'6.0'),null, null  , null   ,  null ,  null   ,null , 6.8 ],
	[  61, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$61' ,'5.59'),null, null  , null  ,  null ,  null   ,null, 5.89  ] ,      
	[ 62, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$62' ,'6.0'),null, null  , null   ,  null ,  null   ,null , 6.3 ],
    [  63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'5.59'),null, null  , null  ,  null ,  null   ,null, 5.19  ] ,     
    [ 63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'6.0'),null, null  , null  ,  null ,  null   ,null , 6.3 ],       
     [ 64, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$64' ,'6.0'),null, null  , null  , null ,  null   ,null ,  4.02 ],     
     [  65, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$65' ,'5.59'),null, null  , null  ,   null ,  null   ,null, 4.20  ] ,          
      [  66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'5.59'),null, null  , null  ,   null ,  null   ,null, 5.29  ] , 
          [  66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'7.59'),null, null  , null  , null ,  null   ,null, 7.39  ] ,         
                 
         [ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'),null, null  , null  , null ,  null   ,null , 6.6 ],
                  [ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'),null, null  , null ,  null ,  null   ,null , 6.6 ],    
       [ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'8.0'),null, null  , null  ,  null ,  null   ,null , 8.50 ],

       [  69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null, null  , null  ,   null ,  null   ,null, 5.7  ] ,    
         [  69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null, null  , null  ,  null ,  null   ,null, 5.9  ] ,   
           [  69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'7.59'),null, null  , null  ,   null ,  null   ,null, 7.8  ] ,     
         [ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.3'),null, null  , null  , null ,  null   ,null , 2.3 ],
           [ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'8.0'),null, null  , null  ,  null ,  null   ,null ,3.0 ],       
            [ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.0'),null, null  , null  ,   null ,  null   ,null , 6.0 ],
       
                       
         [  71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null  , null  ,  null ,  null   ,null, 4.19  ] ,      
         [  71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'7.19'),null, null  , null  ,  null ,  null   ,null, 5.19  ] , 
          [  71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null  , null  ,   null ,  null   ,null, 6.19  ] ,     
         [ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null  , null  ,   null ,  null   ,null , 7.1 ],
           [ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'8.1'),null, null  , null  , null ,  null   ,null , 8.1 ],
         [ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null  , null  , null ,  null   ,null , 9.1 ],
          [  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null  , null  ,  null ,  null   ,null,  2.19  ] ,  
          [  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null  , null  , null ,  null   ,null, 3.19  ] ,    
          [  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'7.19'),null, null  , null  ,  null ,  null   ,null, 4.19  ] ,   
          [ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'6.0'),null, null  , null  , null ,  null   ,null , 6.1 ],
            [ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'8.1'),null, null  , null  , null ,  null   ,null , 8.1 ],      
  
         [  75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null  , null , null   ,null, 4.49  ] ,       
          [  75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.49'),null, null  , null  , null , null   ,null, 5.49  ] ,
          [  75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null  , null ,  null   ,null, 6.49  ] ,  
                   [ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  , null  ,null ,  null   ,null ,  4.4 ],         
         [ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'66.4'),null, null  , null  , null ,  null   ,null ,  6.4 ],
         [ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  , null  , null , null   ,null ,  8.4 ],
           [  77 , createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$77' ,'7.59'),null, null  , null  , null ,  null   ,null, 7.59  ] ,           
         [ 78, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$78' ,'8.0'),null, null  , null  ,null ,  null   ,null , 8.0 ],
          [  79, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$79' ,'7.59'),null, null  , null  ,  null ,  null   ,null, 7.59  ] ,  
           [ 80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'8.0'),null, null  , null  ,  null ,  null   ,null ,8.4 ],
          [  80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'7.19'),null, null  , null  ,   null ,  null   ,null, 7.19  ] ,      
           [ 81, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$81' ,'8.1'),null, null  , null  ,  null ,  null   ,null , 2.1 ],
             [  82, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$82' ,'7.19'),null, null  , null   ,  null ,  null   ,null, 7.19  ] , 
          [ 83, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$83' ,'8.1'),null, null  , null   ,  null ,  null   ,null , 8.1 ],
          [  84, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$84' ,'5.49'),null, null  , null   ,  null ,  null   ,null, 5.49  ] ,           
		  [85, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$85' ,'66.4'),null, null  , null   ,  null ,  null   ,null ,  6.4 ],
            [ 86, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$86' ,'8.0'),null, null   , null ,  null ,  null   ,null ,8.0 ],
          [ 87, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$87' ,'7.19'),null, null  , null ,  null ,  null   ,null, 7.19  ] ,    
		    [ 88, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$88' ,'8.1'),null, null   , null ,  null ,  null   ,null , 1.1 ],
          [  89, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$89' ,'7.19'),null, null   , null ,  null ,  null   ,null, 7.19  ] ,           
         [ 90, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$90' ,'8.1'),null, null   , null ,  null ,  null   ,null , 8.1 ] 
     
 
       
       ]);
}
function setUp4Columns(data){
 
		data.addColumn('number', 'Avg. Cost / Sq Foot'); 
 data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
 data.addColumn( {'type': 'string', 'role': 'style'});
data.addColumn('number', 'PVC/uPVC');
data.addColumn('number', 'Wood');
data.addColumn('number', 'Clad Wood');
data.addColumn('number', 'Aluminum'); 
data.addColumn('number', 'Not Selected'); 
       data.addRows([ 
				/**PVC/uPVC**/
             [72,createCustomHTMLContent( 'Zola Product 1' , 'PVC/uPVC', '$72', '1.103') ,null,  1.103 , null , null , null , null ],		       
			[ 73, createCustomHTMLContent( 'Zola, Product 1', 'PVC/uPVC' ,'$73' ,'6.65' ),null, 6.65 , null , null , null , null  ],
			[  84, createCustomHTMLContent('Zola, Product 4', 'PVC/uPVC' ,'$84', '0.65' ),null,  0.65 , null , null , null , null  ], 
			
			
			/**PVC/uPVC**/
		 [71,createCustomHTMLContent('Tanner Product 1', 'PVC/uPVC', '$71','5.503'),null,null , 5.503 ,  null , null , null  ],  
         [72, createCustomHTMLContent('Zola, Product 1', 'PVC/uPVC' , '$72' ,'8.35') ,null, null , 8.35 ,  null , null , null  ],
		  [  78, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$78' ,'9.66'),null, null , 9.66 ,  null , null , null],         
		  [83,createCustomHTMLContent('Zola, Product 4', 'PVC/uPVC' , '$83','7.35'),null, null , 7.35 ,  null , null , null],
		
		/**Wood*/
		[71.75, createCustomHTMLContent('Zola, Product 1', 'Wood','$71', '2.35') ,null,  null  , null , 2.35  , null , null  ],
		[77, createCustomHTMLContent( 'Synergist, Product 1', 'Wood', '$77','4.35'),null, null  , null , 4.35  , null , null ],
		[ 81,createCustomHTMLContent('Synergist Product 1', 'Wood' , '$81', '6.668') ,null, null  , null , 6.668  , null , null ],
		[82,createCustomHTMLContent('Zola, Product 1', 'Wood' , '$82', '3.35') ,null,  null  , null , 3.35  , null , null  ],  
		
		
		/**Clad Wood**/
		[60,  createCustomHTMLContent('Zola, Product 5', 'Clad Wood' , '$60' ,'7.75') , null,null  , null  , null , 7.75 , null] ,
		 [61,  createCustomHTMLContent('Zola, Product 5', 'Clad Wood' , '$61' ,'8.75') ,null ,null  , null  , null , 8.75 , null ] ,
		[68,  createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$68' ,'7.75') ,null, null  , null  , null , 7.75 , null ] , 
         [ 70,  createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$70','7.75' ),null, null  , null  , null , 7.75 , null ],
		[76, createCustomHTMLContent('Synergist, Product 1', 'Wood' , '$76','7.75'),null, null  , null  , null , 7.75 , null ],
		[81,createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$92','7.75'),null,null  , null  , null , 9.75 , null  ],          
		[ 87, createCustomHTMLContent('Synergist Product 1', 'Clad Wood', '$87','7.88' ),null, null  , null  , null , 7.88 , null  ],
	
			/**Aluminum**/
	      [69.5,  createCustomHTMLContent('Zola, Product 5', 'Aluminum','$69.5' ,'3.35'),null, null  , null  , null  , 3.35,  null  ]  ,
			[69,  createCustomHTMLContent('Zola, Product 5', 'Aluminum','$69' ,'6.35'),null, null  , null   ,  null , 6.35 , null ] , 
		 [74,  createCustomHTMLContent('Zola, Product 1', 'Aluminum','$74', '6.99 ') ,null, null  , null  ,  null , 6.99, null  ],
          [74.5,  createCustomHTMLContent('Zola, Product 1', 'Aluminum','$74', '3.99 ') ,null, null  , null  ,  null , 3.99, null ],
         [ 75, createCustomHTMLContent('Zola, Product 1','Aluminum', '$75', '3.99'),null,null  , null   ,  null , 3.99, null  ],
         [ 75.5, createCustomHTMLContent('Zola, Product 1','Aluminum', '$75', '7.99'),null,null  , null ,  null , 7.99 , null  ],
		 [80, createCustomHTMLContent('Zola, Product 1', 'Aluminum','$80','3.44'),null, null  , null  , null  , 3.44    ,  null],
		 [ 85, createCustomHTMLContent( 'Zola, Product 4', 'Aluminum', '$85','8.29'),null, null  , null  , null , 8.29 ,  null ],
		 [86, createCustomHTMLContent('Zola, Product 1', 'Aluminum','$86', '6.69'),null,null  , null  , null  , 6.69 ,  null  ], 
	/**Unselected**/
	[ 60, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$60' ,'6.0'),null,  null   ,  null ,  null   ,null , 6.8 ],
	[  61, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$61' ,'5.59'),null, null  ,  null ,  null   ,null, 5.89  ] ,      
	[ 62, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$62' ,'6.0'),null, null   ,  null ,  null   ,null , 6.3 ],
    [  63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'5.59'),null, null  ,  null ,  null   ,null, 5.19  ] ,     
    [ 63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'6.0'),null,  null  ,  null ,  null   ,null , 6.3 ],       
     [ 64, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$64' ,'6.0'),null,  null  , null ,  null   ,null ,  4.02 ],     
     [  65, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$65' ,'5.59'),null,  null  ,   null ,  null   ,null, 4.20  ] ,          
      [  66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'5.59'),null, null  ,   null ,  null   ,null, 5.29  ] , 
          [  66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'7.59'),null,  null  , null ,  null   ,null, 7.39  ] ,         
                 
         [ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'),null,  null  , null ,  null   ,null , 6.6 ],
                  [ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'), null  , null ,  null ,  null   ,null , 6.6 ],    
       [ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'8.0'),null, null  ,  null ,  null   ,null , 8.50 ],

       [  69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null,  null  ,   null ,  null   ,null, 5.7  ] ,    
         [  69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null,  null  ,  null ,  null   ,null, 5.9  ] ,   
           [  69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'7.59'),null,  null  ,   null ,  null   ,null, 7.8  ] ,     
         [ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.3'),null, null  , null ,  null   ,null , 2.3 ],
           [ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'8.0'),null, null  ,  null ,  null   ,null ,3.0 ],       
            [ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.0'),null, null  ,  null ,  null   ,null , 6.0 ],
       
                       
         [  71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null  ,  null ,  null   ,null, 4.19  ] ,      
         [  71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'7.19'),null, null  , null ,  null   ,null, 5.19  ] , 
          [  71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null  ,    null ,  null   ,null, 6.19  ] ,     
         [ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null  ,   null ,  null   ,null , 7.1 ],
           [ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'8.1'),null, null  , null ,  null   ,null , 8.1 ],
         [ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null  ,  null ,  null   ,null , 9.1 ],
          [  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null  ,   null ,  null   ,null,  2.19  ] ,  
          [  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null  ,  null ,  null   ,null, 3.19  ] ,    
          [  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'7.19'),null, null  ,   null ,  null   ,null, 4.19  ] ,   
          [ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'6.0'),null, null  , null ,  null   ,null , 6.1 ],
            [ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'8.1'),null, null  ,  null ,  null   ,null , 8.1 ],      
  
         [  75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null  ,  null   ,null, 4.49  ] ,       
          [  75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.49'),null, null  , null  ,  null   ,null, 5.49  ] ,
          [  75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null  ,   null   ,null, 6.49  ] ,  
                   [ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  ,null ,  null   ,null ,  4.4 ],         
         [ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'66.4'),null, null  , null  ,   null   ,null ,  6.4 ],
         [ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  , null  ,null   ,null ,  8.4 ],
           [  77 , createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$77' ,'7.59'),null, null  , null ,  null   ,null, 7.59  ] ,           
         [ 78, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$78' ,'8.0'),null, null  ,null ,  null   ,null , 8.0 ],
          [  79, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$79' ,'7.59'),null, null  ,  null ,  null   ,null, 7.59  ] ,  
           [ 80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'8.0'),null, null  ,  null ,  null   ,null ,8.4 ],
          [  80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'7.19'),null, null  ,  null ,  null   ,null, 7.19  ] ,      
           [ 81, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$81' ,'8.1'),null, null  ,   null ,  null   ,null , 2.1 ],
             [  82, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$82' ,'7.19'),null,  null   ,  null ,  null   ,null, 7.19  ] , 
          [ 83, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$83' ,'8.1'),null, null  ,  null ,  null   ,null , 8.1 ],
          [  84, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$84' ,'5.49'),null, null  ,   null ,  null   ,null, 5.49  ] ,           
		  [85, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$85' ,'66.4'),null, null  ,  null ,  null   ,null ,  6.4 ],
            [ 86, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$86' ,'8.0'),null, null   ,   null ,  null   ,null ,8.0 ],
          [ 87, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$87' ,'7.19'),null, null  ,  null ,  null   ,null, 7.19  ] ,    
		    [ 88, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$88' ,'8.1'),null, null   ,null ,  null   ,null , 1.1 ],
          [  89, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$89' ,'7.19'),null,  null ,  null ,  null   ,null, 7.19  ] ,           
         [ 90, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$90' ,'8.1'),null,  null ,  null ,  null   ,null , 8.1 ] 
     
 
       
       ]);
}
function setUp3Columns(data){
 
		data.addColumn('number', 'Avg. Cost / Sq Foot'); 
 data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
 data.addColumn( {'type': 'string', 'role': 'style'});
data.addColumn('number', 'PVC/uPVC');
data.addColumn('number', 'Wood');
data.addColumn('number', 'Clad Wood'); 
data.addColumn('number', 'Not Selected'); 
       data.addRows( [ 
				/**PVC/uPVC**/
[72,createCustomHTMLContent( 'Zola Product 1' , 'PVC/uPVC', '$72', '1.103') ,null,  1.103 , null , null , null ],		       
[ 73, createCustomHTMLContent( 'Zola, Product 1', 'PVC/uPVC' ,'$73' ,'6.65' ),null, 6.65 , null , null , null   ],
[  84, createCustomHTMLContent('Zola, Product 4', 'PVC/uPVC' ,'$84', '0.65' ),null,  0.65 , null , null , null ], 
			 
		/**Wood*/
[71.75, createCustomHTMLContent('Zola, Product 1', 'Wood','$71', '2.35') ,null, null  ,2.35  ,  null , null ],
[77, createCustomHTMLContent( 'Synergist, Product 1', 'Wood', '$77','4.35'),null, null  ,4.35  , null ,  null  ],
[81,createCustomHTMLContent('Synergist Product 1', 'Wood' , '$81', '6.668') ,null,null  ,6.668  ,  null ,  null  ],
[82,createCustomHTMLContent('Zola, Product 1', 'Wood' , '$82', '3.35') ,null,  null  ,3.35  , null ,  null  ],  
		
		
		/**Clad Wood**/
[60,  createCustomHTMLContent('Zola, Product 5', 'Clad Wood' , '$60' ,'7.75') , null,null  , null   , 7.75, null] ,
[61,  createCustomHTMLContent('Zola, Product 5', 'Clad Wood' , '$61' ,'8.75') ,null ,null  , null  , 8.75  , null ] ,
[68,  createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$68' ,'7.75') ,null, null  , null   , 7.75, null] , 
[70,  createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$70','7.75' ),null, null  , null  , 7.75, null ],
[76, createCustomHTMLContent('Synergist, Product 1', 'Wood' , '$76','7.75'),null, null  , null   , 7.75, null],
[81,createCustomHTMLContent('Zola, Product 1', 'Clad Wood' , '$92','7.75'),null,null  , null   , 9.75, null],          
[87, createCustomHTMLContent('Synergist Product 1', 'Clad Wood', '$87','7.88' ),null, null  , null   , 7.88, null],
		/**Unselected**/
[ 60, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$60' ,'6.0'),null  ,  null ,  null   ,null , 6.8 ],
[ 61, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$61' ,'5.59'),null  ,  null ,  null   ,null, 5.89  ] ,      
[ 62, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$62' ,'6.0'),null   ,  null ,  null   ,null , 6.3 ],
[ 63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'5.59'),null  ,  null ,  null   ,null, 5.19  ] ,     
[ 63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'6.0'),null ,  null ,  null   ,null , 6.3 ],       
[ 64, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$64' ,'6.0'),null , null ,  null   ,null ,  4.02 ],     
[ 65, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$65' ,'5.59'),null ,   null ,  null   ,null, 4.20  ] ,          
[ 66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'5.59'),null ,   null ,  null   ,null, 5.29  ] , 
[ 66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'7.59'),null , null ,  null   ,null, 7.39  ] ,         
[ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'),null , null ,  null   ,null , 6.6 ],
[ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'), null   ,  null ,  null   ,null , 6.6 ],    
[ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'8.0'),null, null ,  null   ,null , 8.50 ],
[ 69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null ,   null ,  null   ,null, 5.7  ] ,    
[ 69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null ,  null ,  null   ,null, 5.9  ] ,   
[ 69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'7.59'),null ,   null ,  null   ,null, 7.8  ] ,     
[ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.3'),null, null,  null   ,null , 2.3 ],
[ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'8.0'),null, null ,  null   ,null ,3.0 ],       
[ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.0'),null, null,  null   ,null , 6.0 ],
[ 71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null,  null   ,null, 4.19  ] ,      
[ 71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'7.19'),null, null ,  null   ,null, 5.19  ] , 
[ 71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null  ,  null   ,null, 6.19  ] ,     
[ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null,  null   ,null , 7.1 ],
[ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'8.1'),null, null  ,  null   ,null , 8.1 ],
[ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null,  null   ,null , 9.1 ],
[  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null ,  null   ,null,  2.19  ] ,  
[ 73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null ,  null   ,null, 3.19  ] ,    
[ 73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'7.19'),null, null,  null   ,null, 4.19  ] ,   
[ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'6.0'),null, null ,  null   ,null , 6.1 ],
[ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'8.1'),null, null ,  null   ,null , 8.1 ],      
[ 75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null  ,null, 4.49  ] ,       
[ 75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.49'),null, null  , null  ,null, 5.49  ] ,
[ 75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null   ,null, 6.49  ] ,  
[ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  ,  null   ,null ,  4.4 ],         
[ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'66.4'),null, null  , null   ,null ,  6.4 ],
[ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  , null  ,null ,  8.4 ],
[ 77 , createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$77' ,'7.59'),null, null ,  null   ,null, 7.59  ] ,           
[ 78, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$78' ,'8.0'),null, null ,  null   ,null , 8.0 ],
[ 79, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$79' ,'7.59'),null, null ,  null   ,null, 7.59  ] ,  
[ 80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'8.0'),null, null   ,null ,null ,8.4 ],
[ 80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'7.19'),null, null  ,  null   ,null, 7.19  ] ,      
[ 81, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$81' ,'8.1'),null, null ,  null   ,null , 2.1 ],
[ 82, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$82' ,'7.19'),null,   null ,  null   ,null, 7.19  ] , 
[ 83, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$83' ,'8.1'),null, null ,  null   ,null , 8.1 ],
[ 84, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$84' ,'5.49'),null, null  ,  null   ,null, 5.49  ] ,           
[85, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$85' ,'66.4'),null, null  ,  null ,null ,  6.4 ],
[ 86, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$86' ,'8.0'),null,  null ,  null   ,null ,8.0 ],
[ 87, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$87' ,'7.19'),null, null   ,  null   ,null, 7.19  ] ,    
[ 88, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$88' ,'8.1'),null, null   ,  null   ,null , 1.1 ],
[  89, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$89' ,'7.19'),null,  null,  null   ,null, 7.19  ] ,           
[ 90, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$90' ,'8.1'),null,  null  ,  null   ,null , 8.1 ] 
       
       ]);
}
function setUp6ColumnOptions(){
   
			   var options = {
          chartArea : {left:0, 'width': '100%', 'height': '100%'},
				  animation: {
					'startup': 'true',
					 'duration': 1000,
					  'easing': 'out'},
				   pointShape: 'circle',
				   pointSize: 10,  
				  
				   series:{
										  0: {   pointShape: 'circle' ,dataOpacity: 1.0 },
										  1: {  pointShape: 'circle' ,dataOpacity: 1.0 },
										  2: {  pointShape: 'circle' ,dataOpacity: 1.0 },
										  3: { pointShape: 'circle',dataOpacity: 1.0  },
										  4: { pointShape: 'circle',dataOpacity: 1.0  },
										  5: { pointShape: 'circle',dataOpacity: 1.0 } ,
										  6: { pointSize: 1, groupWidth: "100%", dataOpacity: 0.45,type: 'area' } 
				  },
				  title : 'Total Window Performance vs. Price',
				  vAxis: {title: 'R-Value', titleTextStyle: {italic: false ,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  hAxis: {title: 'Avg. Cost / Sq Foot', titleTextStyle: {italic: false,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  seriesType: 'scatter' ,
				   focusTarget: 'category', 
				  dataOpacity: 0.4, 
					  legend: { position: 'right' },
			legend: {},
				  tooltip: { isHtml: true ,
							textStyle: 
								  {color: '#0000FF'}
							 , showColorCode: true     
						  }
				};
			return options;
   
   }
function setUp5ColumnOptions(){
   
			   var options = {
           chartArea : {left:0, 'width': '100%', 'height': '100%'},
				  animation: {
					'startup': 'true',
					 'duration': 1000,
					  'easing': 'out'},
				   pointShape: 'circle',
				   pointSize: 10,  
				  
				   series:{
										  0: {   pointShape: 'circle' ,dataOpacity: 1.0 },
										  1: {  pointShape: 'circle' ,dataOpacity: 1.0 },
										  2: {  pointShape: 'circle' ,dataOpacity: 1.0 },
										  3: { pointShape: 'circle',dataOpacity: 1.0  },
										  4: { pointShape: 'circle',dataOpacity: 1.0  }, 
										  5: { pointSize: 1, groupWidth: "100%", dataOpacity: 0.45,type: 'area' } 
				  },
				  title : 'Total Window Performance vs. Price',
				  vAxis: {title: 'R-Value', titleTextStyle: {italic: false ,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  hAxis: {title: 'Avg. Cost / Sq Foot', titleTextStyle: {italic: false,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  seriesType: 'scatter' ,
				   focusTarget: 'category', 
				  dataOpacity: 0.4, 
					  legend: { position: 'right' },
			legend: {},
				  tooltip: { isHtml: true ,
							textStyle: 
								  {color: '#0000FF'}
							 , showColorCode: true     
						  }
				};
			return options;
   
   }
function setUp4ColumnOptions(){
   
			   var options = {
           chartArea : {left:0, 'width': '100%', 'height': '100%'},
				  animation: {
					'startup': 'true',
					 'duration': 1000,
					  'easing': 'out'},
				   pointShape: 'circle',
				   pointSize: 10,  
				  
				   series:{
										  0: {   pointShape: 'circle' ,dataOpacity: 1.0 },
										  1: {  pointShape: 'circle' ,dataOpacity: 1.0 },
										  2: {  pointShape: 'circle' ,dataOpacity: 1.0 },
										  3: { pointShape: 'circle',dataOpacity: 1.0  }, 
										  4: { pointSize: 1, groupWidth: "100%", dataOpacity: 0.45,type: 'area' } 
				  },
				  title : 'Total Window Performance vs. Price',
				  vAxis: {title: 'R-Value', titleTextStyle: {italic: false ,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  hAxis: {title: 'Avg. Cost / Sq Foot', titleTextStyle: {italic: false,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  seriesType: 'scatter' ,
				   focusTarget: 'category', 
				  dataOpacity: 0.4, 
					  legend: { position: 'right' },
			legend: {},
				  tooltip: { isHtml: true ,
							textStyle: 
								  {color: '#0000FF'}
							 , showColorCode: true     
						  }
				};
			return options;
   
   }
function setUp3ColumnOptions(){
   
			   var options = {
          chartArea : {left:0, 'width': '100%', 'height': '100%'},
				  animation: {
					'startup': 'true',
					 'duration': 1000,
					  'easing': 'out'},
				   pointShape: 'circle',
				   pointSize: 10,  
				  
				   series:{
										  0: {   pointShape: 'circle' ,dataOpacity: 1.0 },
										  1: {  pointShape: 'circle' ,dataOpacity: 1.0 },
										  2: {  pointShape: 'circle' ,dataOpacity: 1.0 }, 
										  3: { pointSize: 1, groupWidth: "100%", dataOpacity: 0.45,type: 'area' } 
				  },
				  title : 'Total Window Performance vs. Price',
				  vAxis: {title: 'R-Value', titleTextStyle: {italic: false ,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  hAxis: {title: 'Avg. Cost / Sq Foot', titleTextStyle: {italic: false,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  seriesType: 'scatter' ,
				   focusTarget: 'category', 
				  dataOpacity: 0.4, 
					  legend: { position: 'right' },
			legend: {},
				  tooltip: { isHtml: true ,
							textStyle: 
								  {color: '#0000FF'}
							 , showColorCode: true     
						  }
				};
			return options;
   
   }
function setUp2ColumnOptions(){
   
			   var options = {
        
				  animation: {
					'startup': 'true',
					 'duration': 1000,
					  'easing': 'out'},
				   pointShape: 'circle',
				   pointSize: 10,  
				  
				   series:{
										  0: {   pointShape: 'circle' ,dataOpacity: 1.0 },
										  1: {  pointShape: 'circle' ,dataOpacity: 1.0 },
										   
										  2: { pointSize: 1, groupWidth: "100%", dataOpacity: 0.45,type: 'area' } 
				  },
				  title : 'Total Window Performance vs. Price',
				  vAxis: {title: 'R-Value', titleTextStyle: {italic: false ,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  hAxis: {title: 'Avg. Cost / Sq Foot', titleTextStyle: {italic: false,color: '#cccccc', fontName: 'Montserrat' , fontSize: 32 }},
				  seriesType: 'scatter' ,
				   focusTarget: 'category', 
				  dataOpacity: 0.4, 
				  legend: { position: 'right' },
	        enableInteractivity : true,
           'width':900,
           'height': 600,
				  tooltip: { isHtml: true ,
							textStyle: 
								  {color: '#0000FF'}
							 , showColorCode: true     
						  }
				};
			return options;
   
   }
function setUp2Columns(data){
 
		data.addColumn('number', 'Avg. Cost / Sq Foot'); 
 data.addColumn({'type': 'string', 'role': 'tooltip', 'p': {'html': true}});
 data.addColumn( {'type': 'string', 'role': 'style'});
data.addColumn('number', 'PVC/uPVC');
data.addColumn('number', 'Wood');
data.addColumn('number', 'Not Selected'); 
       data.addRows( [ 
				/**PVC/uPVC**/
[72,createCustomHTMLContent( 'Zola Product 1' , 'PVC/uPVC', '$72', '1.103') ,null,  1.103 ,null , null ],		       
[ 73, createCustomHTMLContent( 'Zola, Product 1', 'PVC/uPVC' ,'$73' ,'6.65' ),null, 6.65 ,  null , null   ],
[  84, createCustomHTMLContent('Zola, Product 4', 'PVC/uPVC' ,'$84', '0.65' ),null,  0.65 ,null , null ], 
			 
		/**Wood*/
[71.75, createCustomHTMLContent('Zola, Product 1', 'Wood','$71', '2.35') ,null, null  ,2.35  , null ],
[77, createCustomHTMLContent( 'Synergist, Product 1', 'Wood', '$77','4.35'),null, null  ,4.35 ,  null  ],
[81,createCustomHTMLContent('Synergist Product 1', 'Wood' , '$81', '6.668') ,null,null  ,6.668   ,  null  ],
[82,createCustomHTMLContent('Zola, Product 1', 'Wood' , '$82', '3.35') ,null,  null  ,3.35  ,  null  ],  
		
		
		/**Unselected**/
[ 60, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$60' ,'6.0'),null  ,  null ,  null   , 6.8 ],
[ 61, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$61' ,'5.59'),null  ,  null ,  null  , 5.89  ] ,      
[ 62, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$62' ,'6.0'),null   ,  null ,  null  , 6.3 ],
[ 63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'5.59'),null  ,  null ,  null  , 5.19  ] ,     
[ 63, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$63' ,'6.0'),null ,  null ,  null   , 6.3 ],       
[ 64, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$64' ,'6.0'),null , null ,  null ,  4.02 ],     
[ 65, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$65' ,'5.59'),null ,   null ,  null  , 4.20  ] ,          
[ 66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'5.59'),null ,   null ,  null , 5.29  ] , 
[ 66, createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$66' ,'7.59'),null , null ,  null , 7.39  ] ,         
[ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'),null , null ,  null   , 6.6 ],
[ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'6.0'), null   ,  null   ,null , 6.6 ],    
[ 68, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$68' ,'8.0'),null, null ,  null  , 8.50 ],
[ 69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null ,   null ,  null , 5.7  ] ,    
[ 69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'5.59'),null ,  null ,  null  , 5.9  ] ,   
[ 69, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$69' ,'7.59'),null ,   null ,  null  , 7.8  ] ,     
[ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.3'),null, null,  null    , 2.3 ],
[ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'8.0'),null, null ,  null   ,3.0 ],       
[ 70, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$70' ,'6.0'),null, null,  null    , 6.0 ],
[ 71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null,  null  , 4.19  ] ,      
[ 71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'7.19'),null, null ,  null   , 5.19  ] , 
[ 71, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$71' ,'5.59'),null, null  ,  null  , 6.19  ] ,     
[ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null,  null   , 7.1 ],
[ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'8.1'),null, null  ,  null , 8.1 ],
[ 72, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$72' ,'6.0'),null, null,  null   , 9.1 ],
[  73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null ,  null  ,  2.19  ] ,  
[ 73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'5.59'),null, null ,  null   , 3.19  ] ,    
[ 73, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$73' ,'7.19'),null, null,  null , 4.19  ] ,   
[ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'6.0'),null, null ,  null   , 6.1 ],
[ 74, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$74' ,'8.1'),null, null ,  null , 8.1 ],      
[ 75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null  , 4.49  ] ,       
[ 75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.49'),null, null  , null, 5.49  ] ,
[ 75, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$75' ,'5.59'),null, null  , null , 6.49  ] ,  
[ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  ,  null  ,  4.4 ],         
[ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'66.4'),null, null  , null  ,  6.4 ],
[ 76, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$76' ,'6.0'),null, null  , null  ,  8.4 ],
[ 77 , createCustomHTMLContent('Tanner, Product 1', 'Steel' , '$77' ,'7.59'),null, null ,  null  , 7.59  ] ,           
[ 78, createCustomHTMLContent('Tanner, Product 1', 'Clad Wood' , '$78' ,'8.0'),null, null ,  null   , 8.0 ],
[ 79, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$79' ,'7.59'),null, null ,  null   , 7.59  ] ,  
[ 80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'8.0'),null, null   ,null  ,8.4 ],
[ 80, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$80' ,'7.19'),null, null  ,  null  , 7.19  ] ,      
[ 81, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$81' ,'8.1'),null, null ,  null  , 2.1 ],
[ 82, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$82' ,'7.19'),null,   null ,  null , 7.19  ] , 
[ 83, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$83' ,'8.1'),null, null ,  null   , 8.1 ],
[ 84, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$84' ,'5.49'),null, null  ,  null , 5.49  ] ,           
[85, createCustomHTMLContent('Tanner, Product 1', 'Wood' , '$85' ,'66.4'),null, null  ,  null ,  6.4 ],
[ 86, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$86' ,'8.0'),null,  null ,  null   ,8.0 ],
[ 87, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$87' ,'7.19'),null, null   ,  null   , 7.19  ] ,    
[ 88, createCustomHTMLContent('Tanner, Product 1', 'PVC/uPVC' , '$88' ,'8.1'),null, null   ,  null    , 1.1 ],
[  89, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$89' ,'7.19'),null,  null,  null  , 7.19  ] ,           
[ 90, createCustomHTMLContent('Tanner, Product 1', 'Aluminum' , '$90' ,'8.1'),null,  null  ,  null  , 8.1 ] 
       
       ]);
}
  var createCustomHTMLContent = function( companyName , materialType, pricePerSquareFoot,  totalPerformance ) {
//'Tanner, Product 1, PVC/uPVC at $92 per square foot.  Total Window Performance is 1.35' 
  return '<div  class="ft-graph-tooltip">  <div class="ft=row"><div class="ft-header">fentrend</div></div><div class="ft-row"><div class="ft-lcol">Company</div><div class="ft-rcol">' +companyName +'</div></div>'+
    '<div class="ft-row"><div class="ft-lcol">Material</div><div class="ft-rcol">' +materialType +'</div></div>'+
     '<div class="ft-row"><div class="ft-lcol">Price $ Sq/Ft</div><div class="ft-rcol">' +pricePerSquareFoot +'</div></div>'+
     '<div class="ft-row"><div class="ft-lcol">Total Performance</div><div class="ft-rcol">' +totalPerformance +'</div></div>'+
        '</div>';
} ;




      function drawVisualization2Col(el) {
        // Some raw data (not necessarily accurate)
          	


var data = new google.visualization.DataTable();
        setUp2Columns(data);
 
    var options = setUp2ColumnOptions();
    var chart = new google.visualization.ComboChart(el[0]);
    chart.draw(data, options);
  }
      function drawVisualization3Col(el) {
        // Some raw data (not necessarily accurate)
          	


var data = new google.visualization.DataTable();
        setUp3Columns(data);
 
    var options = setUp3ColumnOptions();
    var chart = new google.visualization.ComboChart(el);
    chart.draw(data, options);
  }
      function drawVisualization4Col(el) {
        // Some raw data (not necessarily accurate)
          	


var data = new google.visualization.DataTable();
        setUp4Columns(data);
 
    var options = setUp4ColumnOptions();
    var chart = new google.visualization.ComboChart(el);
    chart.draw(data, options);
  }
      function drawVisualization5Col(el) {
        // Some raw data (not necessarily accurate)
          	


var data = new google.visualization.DataTable();
        setUp5Columns(data);
 
    var options = setUp5ColumnOptions();
    var chart = new google.visualization.ComboChart(el);
    chart.draw(data, options);
  }
      function drawVisualization6Col(el) {
        // Some raw data (not necessarily accurate)
          	


var data = new google.visualization.DataTable(attrval);
        setUp6Columns(data);
 
    var options = setUp6ColumnOptions();
    var chart = new google.visualization.ComboChart(document.getElementById(attrval));
    chart.draw(data, options);
  }

       


 