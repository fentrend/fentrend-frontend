function drawAxisTick5Colors(el) {
      var data = google.visualization.arrayToDataTable([
        ['Product', 'Glass R-Value', 'Frame  R-Value', 'Total  R-Value' ],
        ['Zola Product 1', 3.77, 8.0, 9.0],
        ['Zola Product 2', 8.43, 5.5, 8.0],
        ['Zola Product 3', 4.64, 7.4, 9.4],
        ['Tanner Product 1', 9.98, 8.2, 3.4],
        ['Tanner Product 2', 6.35, 9.1, 7.2]
      ]);
 
      var options = {
        width: 600,
        height: 600,
        title: 'Performance Analysis By Component Overview',
        chartArea: {width: '50%'},
        tooltip: {isHtml: true} ,
        hAxis: {
          title: 'R-Value',
          minValue: 0,
          textStyle: {
            bold: false,
            italic: false,
            fontSize: 12,
            color: '#4d4d4d'
          },
          titleTextStyle: {
            bold: true,
            fontSize: 18,
            color: '#4d4d4d'
          }
        } 
      };
      var chart = new google.visualization.BarChart(el);
      chart.draw(data, options);
    }
function drawAxisTick4Colors(el) {
      var data = google.visualization.arrayToDataTable([
        ['Product', 'Glass R-Value', 'Frame  R-Value', 'Total  R-Value' ],
        ['Zola Product 1', 3.77, 8.0, 9.0],
        ['Zola Product 2', 8.43, 5.5, 8.0],
        ['Zola Product 3', 4.64, 7.4, 9.4],
        ['Tanner Product 1', 9.98, 8.2, 3.4] 
      ]);
 
      var options = {
          width: 600,
        height: 600,
        title: 'Performance Analysis By Component Overview',
        chartArea: {width: '50%'},
        tooltip: {isHtml: true} ,
        hAxis: {
          title: 'R-Value',
          minValue: 0,
          textStyle: {
            bold: false,
            italic: false,
            fontSize: 12,
            color: '#4d4d4d'
          },
          titleTextStyle: {
            bold: true,
            fontSize: 18,
            color: '#4d4d4d'
          }
        } 
      };
      var chart = new google.visualization.BarChart(el);
      chart.draw(data, options);
    }
function drawAxisTick3Colors(el) {
      var data = google.visualization.arrayToDataTable([
        ['Product', 'Glass R-Value', 'Frame  R-Value', 'Total  R-Value' ],
        ['Zola Product 1', 3.77, 8.0, 9.0],
        ['Zola Product 2', 8.43, 5.5, 8.0],
        ['Zola Product 3', 4.64, 7.4, 9.4] 
      ]);
 
      var options = {
          width: 600,
        height: 600,
        title: 'Performance Analysis By Component Overview',
        chartArea: {width: '50%'},
        tooltip: {isHtml: true} ,
        hAxis: {
          title: 'R-Value',
          minValue: 0,
          textStyle: {
            bold: false,
            italic: false,
            fontSize: 12,
            color: '#4d4d4d'
          },
          titleTextStyle: {
            bold: true,
            fontSize: 18,
            color: '#4d4d4d'
          }
        } 
      };
      var chart = new google.visualization.BarChart(el);
      chart.draw(data, options);
    }
function drawAxisTick2Colors(el) {
      var data = google.visualization.arrayToDataTable([
        ['Product', 'Glass R-Value', 'Frame  R-Value', 'Total  R-Value' ],
        ['Zola Product 1', 3.77, 8.0, 9.0],
        ['Zola Product 2', 8.43, 5.5, 8.0] 
      ]);
 
      var options = {
          width: 600,
        height: 600,
        title: 'Performance Analysis By Component Overview',
        chartArea: {width: '50%'},
        tooltip: {isHtml: true} ,
        hAxis: {
          title: 'R-Value',
          minValue: 0,
          textStyle: {
            bold: false,
            italic: false,
            fontSize: 12,
            color: '#4d4d4d'
          },
          titleTextStyle: {
            bold: true,
            fontSize: 18,
            color: '#4d4d4d'
          }
        } 
      };
      var chart = new google.visualization.BarChart(el);
      chart.draw(data, options);
    }