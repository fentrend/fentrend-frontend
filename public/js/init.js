
(function( $ ){
    RP_HOME={};
    RP_HOME.utils={};
   RP_HOME.utils['INITALIZED']='INIT';
    RP_HOME.REMAINING=78;
    RP_HOME.CLOUDFRONT="d2ufpzvzzly9d.cloudfront.net";
     RP_HOME.utils.getMaxLength=function(o) {
	    if(o.getAttribute('maxLength'))
        {
          return o.getAttribute('maxLength');
        }
    };
   RP_HOME.utils.showSpinner=function(show_) {
       if(typeof show_ !=='undefined' && show_){
         $('.backgroundlayer').css('display','block'); 
         var width = $(window).width()/2;
         var height = $(window).height()/2;
         $('.backgroundlayer').find('.spinner').css('top',''+height+'px');
         $('.backgroundlayer').find('.spinner').css('left',''+width+'px');
         
       }
       else {
         $('.backgroundlayer').css('display','none');
       }
  };
   RP_HOME.utils.getSelectionStart=function(o) {
    if (o.createTextRange) {
      var r = document.selection.createRange().duplicate()
      r.moveEnd('character', o.value.length)
      if (r.text == '') return o.value.length
      return o.value.lastIndexOf(r.text)
    } else return o.selectionStart
  };
  RP_HOME.utils.loadSocialImage=function(img_url, img_name){
    if(img_url && img_url!==null && img_url!==undefined){
           var ioj = new Image();
           ioj.crossOrigin = 'Anonymous';

           ioj.onload = (function() {
                      
                        var canvasHeight = this.height < 32  ? this.height : 32;
                        var canvasWidth = this.width < 32 ? this.width : 32; 
                           var div_='<div style="display: inline-block;text-overflow: ellipsis;">'+
                      '<p class="imgpreview" style="height:32px;">'+
                          '<canvas  height="'+canvasHeight+'" data-img="'+img_url+'" style="max-height: 32px;">'+ 
                      '</p>'+
                      '</div>' ;
                    
                        $('#files').html(div_);
                       var imageObj = RP_HOME.utils.getImageObj(img_url);
            
                       if(imageObj.width < 600 || imageObj.height < 315){
                         
                         alert("Image must be 600x315 pixels minimum. Please upload a different image.");
                            $('#files').html('<div style="display: inline-block; visibility: visible; margin-left: 5px; vertical-align: top; float: none; margin-top: 6px; margin-right: 0px;">600x315 min.</div>'); 
                                  
                       }
                       else{
                         
                         $('.fileinput-button').css('display','none');
                         $('.removeimg').css('display','inline-block');
                        $('#imgprev').attr('src',img_url);
//                         $('canvas').css('cursor','pointer');
//                             $('canvas').css('cursor','hand'); 
                        var span_='' ;//used to be the image name
                        var $el = $('<span class="glyphicon removeimg glyphicon-remove" style="vertical-align: top;padding:0px;right: -2px;"></span>');
                          $el.on('click',function(e){
                            $('.fileinput-button').css('display','inline-block');
                           $('#files').html('<div style="display: inline-block; visibility: visible; margin-left: 5px; vertical-align: top; float: none; margin-top: 6px; margin-right: 0px;">600x315 min.</div>'); 
                           $('#imgprev').remove();
                            RP_HOME.utils.hide_embed();
                         });
                        $('canvas').after(span_).parent().parent().after($el);
                       }
                        

                       
           });
          ioj.src = img_url;
 
    }
 
  };
  RP_HOME.utils.getImageObj=function(url){
      var imageObj = new Image();
      var canvas_ = $('canvas')[0];
      var ctx = canvas_.getContext("2d");
      imageObj.onload = function() {
 
       canvas_.height =  canvas_.width * (imageObj.height / imageObj.width);
 
         /// step 1
        var oc = document.createElement('canvas'),
            octx = oc.getContext('2d');

//         oc.width = imageObj.width * 0.5;
//         oc.height = imageObj.height * 0.5;
        octx.drawImage(imageObj, 0, 0, oc.width, oc.height);

        /// step 2
        octx.drawImage(oc, 0, 0, oc.width , oc.height );

        ctx.drawImage(oc, 0, 0, oc.width  , oc.height  ,
        0, 0, canvas_.width, canvas_.height);
      };
      imageObj.crossOrigin = "Anonymous";
      imageObj.src = url;
      return imageObj;
  };
  RP_HOME.utils.drawImage=function(imageObj,canvas){ 
        var context = canvas.getContext('2d');
        var imageX = 0;
        var imageY = 0;
        var imageWidth = 25;
        var imageHeight = 25;
//         canvas.width=25;
//         canvas.height=25; 
     imageObj.onload = function() {
 
         var w=imageObj.width < 55 ? imageObj.width : 55;
        var h=imageObj.height < 55 ? imageObj.height : 55;
        canvas.width = w;
        canvas.height = h;

        context.drawImage(imageObj, imageX, imageY);

        var imageData = context.getImageData(imageX, imageY, w, h);
        var data = imageData.data;

        // iterate over all pixels
        for(var i = 0, n = data.length; i < n; i += 4) {
          var red = data[i];
          var green = data[i + 1];
          var blue = data[i + 2];
          var alpha = data[i + 3];
        }

        // pick out pixel data from x, y coordinate
        var x = 20;
        var y = 20;
        var red = data[((imageWidth * y) + x) * 4];
        var green = data[((imageWidth * y) + x) * 4 + 1];
        var blue = data[((imageWidth * y) + x) * 4 + 2];
        var alpha = data[((imageWidth * y) + x) * 4 + 3];
        
        // iterate over all pixels based on x and y coordinates
        for(var y = 0; y < imageHeight; y++) {
          // loop through each column
          for(var x = 0; x < imageWidth; x++) {
            var red = data[((imageWidth * y) + x) * 4];
            var green = data[((imageWidth * y) + x) * 4 + 1];
            var blue = data[((imageWidth * y) + x) * 4 + 2];
            var alpha = data[((imageWidth * y) + x) * 4 + 3];
          }
        }
      
     };
      
  };
   RP_HOME.utils.getSelectionEnd=function(o) {
    if (o.createTextRange) {
      var r = document.selection.createRange().duplicate()
      r.moveStart('character', -o.value.length)
      return r.text.length
    } else return o.selectionEnd
  };
    RP_HOME.utils.getSelectedText=function(){
            var text = "";
            if (window.getSelection) {
                text = window.getSelection().toString();
            } else if (document.selection && document.selection.type != "Control") {
                text = document.selection.createRange().text;
            }
            return text; 
    };
    RP_HOME.utils.center=  function (element_)
    {   
      element_.css("top", (($(window).height() - element_.outerHeight()) / 2) + (1.5*$(window).scrollTop() )+ "px");    
   element_.css("left", (($(window).width() - element_.outerWidth()) / 2) + $(window).scrollLeft() + "px");     return element_; 
    } ;
    RP_HOME.utils.validate_field=function(field_data){
      if(field_data && (typeof(field_data)!=='undefined') && (typeof(field_data)!=='null') && field_data !==undefined && field_data!==null){
        return field_data.trim().replace(/&nbsp;/gi,'').length > 0;
      }
      return false;
    };  
    RP_HOME.utils.character_count=function(page_obj)
    {
      var remaining_characters = page_obj.max_chars;
      var max_chars = page_obj.max_chars;
      var question_element = page_obj.question_element ;
      var answer_elements = page_obj.answer_elements;
      var answer_active = page_obj.answer_active;
      var question_text = question_element.val().replace(/&nbsp;/gi,'').length;
      var qmcount =0;
      if(question_element.val().indexOf("?") > -1){
        qmcount++;
      }
      if(answer_elements && answer_elements.length > 0){
            var max_answer_length = 0;
        if(answer_active){
          max_answer_length=answer_.val().replace(/&nbsp;/gi,'').length;
        }
        else{
          
          $(answer_elements).each(function(el){
              var text_=$($(answer_elements)[el]).val().replace(/&nbsp;/gi,'');
            if(text_==="")
              {
               text_= $($(answer_elements)[el]).val();
              }
            
              if(text_.trim().length > max_answer_length){
                max_answer_length=text_.length;
              }
            });
        }
      }
      return qmcount+remaining_characters-(question_text+max_answer_length);
    };
    RP_HOME.utils.copy=function(embed){
      
        var text_val=eval(embed.find('xmp'));
        text_val.focus();
        text_val.select(); 
        if (!document.all){
        
          return; // IE only
        } 
        r = text_val.createTextRange();
      
        alert(r.htmlText );
        r.execCommand('copy');
    
    }
    RP_HOME.utils.show_embed=function(){
           RP_HOME.utils['INITALIZED']='SAVED';
      var objectid = $('#reset').attr('data-objectid');
      
      if(objectid && (objectid.trim().length > 2)){
          $('#reset').css('opacity','0.07');
          $('#edit_warning').css('visibility','hidden');
 
      }
    $('#overlay_warning_text').remove();
//       $('.embed').unwrap( );
		$('.embed').css('display','block'); 
     $('.embed').removeClass('user-unselectable').addClass('user-selectable');
          $('.embed').css('opacity','1.0'); 
    };
    RP_HOME.warning_text="You have edited this poll but the changes have not been saved. You may either save your changes to update the embed code, or revert back to the last saved state.";
    RP_HOME.utils.hide_embed=function(){
         RP_HOME.utils['INITALIZED']='EDIT';
      $('.embed').css('opacity','0.07');
 
       var opacity = parseFloat($('#reset').css('opacity'));
            var objectid = $('#reset').attr('data-objectid');
           if(objectid && (objectid.trim().length > 2 ) && opacity.toFixed(2)==="0.07" ){
              $('#reset').css('opacity','1.0');
//               $('#edit_warning').css('visibility','visible');
             var preview_content_width_ =  '100%';//$('.embed').width();
               var preview_content_height_ =  2*$('.embed').height();
               var div_= '<div style="width:'+preview_content_width_+';height:'+preview_content_height_+'px;position:relative;display:block;margin-bottom:15px;"></div>';
             $('.embed').wrap(div_);
             $('.embed').after('<div id="overlay_warning_text" style="color: #bbbbbb;background-color: #ffffff;border: 1px solid #bbbbbb; position: absolute; font-size: 14px; top: 0; left: 0; bottom: 0; right: 0;margin-left: 0px;margin-right: 0px;text-align: center;"><div style="display:inline-block; width: 100%;position:relative;top: 28px;">'+ RP_HOME.warning_text +'</div></div>');
           }
        
        $('.embed').removeClass('user-selectable').addClass('user-unselectable');
      var doid= $('#reset').attr('data-objectid');
      if(doid){
         $('#reset').css('visibility','visible');
      }
     
    };
    RP_HOME.utils.login=function(){
//              Parse.initialize("UyiG0nOZ8HNxRhbiDbp8npkK50VCamFjLF6lVKs9", "bAWTl89YIryuOLk3p2lyyyF0lMiikarWhLMtBkFk");
                Parse.initialize("Fhq4NuqJRQ2TxXroPjpFi8ylt1Tm30GWIxJNy4vP", "hReeeK0l0QpCFk1Fg5MdqRYXM9LsWogMDwxrGolb");

              window.fbAsyncInit = function() {
                      FB.init({
                        appId      : '421599347990310',
                        xfbml      : true,
                        version    : 'v2.2'
                      });
                    };

                    (function(d, s, id){
                       var js, fjs = d.getElementsByTagName(s)[0];
                       if (d.getElementById(id)) {return;}
                       js = d.createElement(s); js.id = id;
                       js.src = "//connect.facebook.net/en_US/sdk.js";
                       fjs.parentNode.insertBefore(js, fjs);
                     }(document, 'script', 'facebook-jssdk'));
             Parse.FacebookUtils.init({
                        appId      : '421599347990310',
                        xfbml      : true,
                        version    : 'v2.2'
                      });
                Parse.FacebookUtils.logIn("user_likes,email",{
                  success: function(user){
                   if (!user.existed())
                     {
                       FB.api('/me', function(me) {
                          user.set("first_name", me.name);
                          user.set("email", me.email);
                          user.set("user_type","facebook");
                         user.save().then(function(user){
                              $(location).attr('href','dashboard.html' );
                         });
//                           console.log("/me response", me);
                        });
                     }
                     else{
                         $(location).attr('href','dashboard.html' );
                     }    
                     
                  },
                  error: function(user,err){
                      alert("User "+JSON.stringify(user)+" unable to log in "+JSON.stringify(err));
                    
                  }
                });

    };
    var methods = {
        init : function(options) 
        {
//               Parse.initialize("UyiG0nOZ8HNxRhbiDbp8npkK50VCamFjLF6lVKs9", "bAWTl89YIryuOLk3p2lyyyF0lMiikarWhLMtBkFk");
                 Parse.initialize("Fhq4NuqJRQ2TxXroPjpFi8ylt1Tm30GWIxJNy4vP", "hReeeK0l0QpCFk1Fg5MdqRYXM9LsWogMDwxrGolb");

              window.fbAsyncInit = function() {
                      FB.init({
                        appId      : '421599347990310',
                        xfbml      : true,
                        version    : 'v2.2'
                      });
                    };

                    (function(d, s, id){
                       var js, fjs = d.getElementsByTagName(s)[0];
                       if (d.getElementById(id)) {return;}
                       js = d.createElement(s); js.id = id;
                       js.src = "//connect.facebook.net/en_US/sdk.js";
                       fjs.parentNode.insertBefore(js, fjs);
                     }(document, 'script', 'facebook-jssdk'));
        },
        logout: function(url_){
              Parse.User.logOut();
             $(location).attr('href',url_ );
        },
        login : function( login_obj ) 
        {    
           if(typeof(login_obj['user'] )==='undefined' || login_obj['user'] ===null || login_obj['user'].trim().length  === 0){
             alert("Must enter user name.")
          }
          else if(typeof(login_obj['password'] )==='undefined' || login_obj['password'] ===null || login_obj['password'].trim().length  === 0){
            alert("Must enter password.");
          }
          else{
           Parse.User.logIn(login_obj['user'], login_obj['password'], {
					  success: function(user) { 
					  	 $(location).attr('href',login_obj['url'] );
              
					  },
					  error: function(user, error) { 
              if( typeof(login_obj['error_url'])==='undefined' || login_obj['error_url']===null || login_obj['error_url'].trim().length===0)
              {
                if(error.code===101){
                  alert("Username or password is invalid.");
                }
                else{
                   alert('User '+error.code+' '+error.message);
                }
                
              }
              else{
                 	 $(location).attr('href',login_obj['error_url'].trim() );
              }
					  }
          });
          }
      
          
          
          
        },
	getRunningTime: function(pollid,votecount,inactiveflag ,callback_, callback_error_)
	{
		///rollipoll/report/runtime/RSGMSXUusc/5

		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: '/rollipoll/report/runtime/'+pollid+'/'+votecount,
			contentType: 'application/json',
			success: function(msg){ callback_.apply(this,[msg]); }	,
			error: function(err){ callback_error_.apply(this,[err]); }	
		});
	},
      getReportData: function(pollid, callback_){
      
           $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url:   '/rollipoll/report/'+pollid,  
                    contentType: "application/json",
                   success: function(msg){ if(callback_){callback_.apply(this,[msg]);} },
                   error: function(err){ alert(JSON.stringify(err) );  }
           });
      },
      getPollData : function(){ 
        var poll={};
        var poll_settings = {};
        poll['question']=$($('.question')[0]).val().trim();
        poll['user']=currentUser;
        poll['answers']=[];
        poll_settings['questioncolor']=$('#question-color').text();
        poll_settings['bgcolor']=$('#bg-color').text();
        poll_settings['answer_font']=$($('.dropdown-toggle')[1]).text().trim();
        poll_settings['question_font']=$($('.dropdown-toggle')[0]).text().trim();
        poll_settings['linkcolor']=$('#link-color').text().trim(); 
        poll_settings['social_image']=$('#imgprev').attr('src') || $('.imgpreview').find('canvas').attr('data-img');
        if(poll_settings['social_image'] && poll_settings['social_image']!==null && poll_settings['social_image']!==undefined){
          
          poll_settings['social_image_name']=$('.imgpreview').find('span').text();
        }
        poll['poll_settings']=poll_settings;
         $('.answer').each(function(item){
             // $('.ans_num')[item].text(""+start_at_);
              if($('.answer')[item])
                { 
                  var answer_={};
                  var a_i_ =$(  $('.answer')[item] );
                   if(a_i_ && a_i_.val()){
                     answer_['order']=item;
                     answer_['text']=a_i_.val().trim();
                     poll['answers'].push(answer_);  
                   }
                }  
          }); 
          return poll;
        
    },
    clearSettingsCache : function(user_,  callback_){ 
              $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/rollipoll/clearsettings',
                data: JSON.stringify(  user_ ),
                contentType: "application/json",
                success: function(msg){
                  /**This is weird but don't do anything**/
                  if(typeof callback_ !=='undefined' && callback_!==null){
                    callback_.apply(this,[msg]);
                  }
                },
                 error: function(e){alert(JSON.stringify(e));}
              });
        },
        getSocialMediaReport : function(pollId,callback_){ 
          $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: '/rollipoll/socialrpt/'+pollId, 
                contentType: "application/json",
                success: function(msg){
                  if(typeof callback_!== 'undefined' && callback_!==null){
                    callback_.apply(this,[msg]);
                  }
                },
                error: function(e){alert(JSON.stringify(e));}
          });
        }, 
        getPolls : function(user_, append_,callback_){ 
              $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/rollipoll/polls/find',
                data: JSON.stringify(  user_ ),
                contentType: "application/json",
                success: function(msg){ 
                  var new_content = '';
                  var pie_charts='';
			var pollids=[];
                   $('#has_no_polls').css('display','block');
                  $('#has_polls').css('visibility','hidden');
                  $(msg).each(function(item){ 
                  $('#has_no_polls').css('display','none');
                  $('#has_polls').css('visibility','visible');
			pollids.push($(msg)[item]['objectId']);
                    
			var content='<tr data-objectid="'+$(msg)[item]['objectId']+'">'+
						'<td id="'+$(msg)[item]['objectId']+'" class="charthover" style="text-align: left;vertical-align: middle;cursor: default;">'+$(msg)[item]['question']+'</td>'+
						'<td id="'+$(msg)[item]['objectId']+'_runtime" style="vertical-align: middle;">0</td>'+
						'<td id="'+$(msg)[item]['objectId']+'_impressions" style="vertical-align: middle;">0</td>'+
						'<td id="'+$(msg)[item]['objectId']+'_votes" style="vertical-align: middle;">0</td>'+
						'<td id="'+$(msg)[item]['objectId']+'_fbshares" style="vertical-align: middle;">0</td>'+
						'<td id="'+$(msg)[item]['objectId']+'_tweets" style="vertical-align: middle;">0</td>'+
						'<td id="'+$(msg)[item]['objectId']+'_fbtraffic" style="vertical-align: middle;">0</td>'+
						'<td id="'+$(msg)[item]['objectId']+'_twittertraffic" style="vertical-align:middle;">0</td>'+
						'<td class="update-chooser" style="vertical-align: middle;" data-objectid="'+$(msg)[item]['objectId']+'"><div  data-objectid="'+$(msg)[item]['objectId']+'" class="glyphicon glyphicon-pencil" style="display:inline;" ></div></td>'+
						'<td class="update-chooser delete_update_chooser" style="vertical-align: middle;" data-objectid="'+$(msg)[item]['objectId']+'"><div data-objectid="'+$(msg)[item]['objectId']+'" id="drollipoll_'+$(msg)[item]['objectId']+'"" class="glyphicon glyphicon glyphicon-trash" style="display:inline;"></div></td>'+ 
						'</tr>'; 
             pie_charts = pie_charts+'<div id="piechartoutter_'+$(msg)[item]['objectId']+'" style="display:none;"><div id="piechart_'+$(msg)[item]['objectId']+'">Test Content ---- </div></div>';

                     new_content = new_content + content;  
                     
                  });
                  append_.html(new_content);
                  append_.append(pie_charts);
                  $(pollids).each(function(item_){
                  $(  '#'+pollids[item_] ).popover({
						     container:'body',
						    trigger: "hover",
                                                    placement: 'auto',
                                                    html: true,
                                                    content: function(){
                                                      
                                                         var data = new google.visualization.DataTable();
                                                          data.addColumn('string', 'Answer');
                                                          data.addColumn('number', 'Percent');
  
                                                            var dataitems=[    ];

                                                                           var answer_metrics= JSON.parse($('#'+pollids[item_]).attr('data-answer-metrics') );
                                                                            if(answer_metrics){
                                                                                $(answer_metrics).each(function(i){
                                                                                  dataitems.push([answer_metrics[i]['text'], parseFloat(answer_metrics[i]['percent']) ]  ) ; 
                                                                                });
                                                                            }
//                                                                              
                                                                             data.addRows(dataitems);                                                     
                                                                             var options = {
                                                                               enableInteractivity : true,
                                                                               pieSliceTextStyle: {color: '#000000', fontName: 'Arial' , fontSize: '12px'  },
                                                                               tooltip: { trigger: 'focus' },
                                                                               colors: ['#269ae4', '#51aee9', '#7dc2ef', '#a8d7f4', '#d4ebfa'  ],
                                                                              'width':200,
                                                                              'height':100 
                                                                              };
                                                                             var chart = new google.visualization.PieChart(document.querySelector('#piechart_'+pollids[item_]));
                                                                              chart.draw(data, options); 
                                                     			      return $('#piechartoutter_'+pollids[item_]).html();
                                                     
                                                         }, 
						     template: '<div class="popover"><h3 class="popover-title"></h3><div class="popover-content" style="padding:0px;"></div></div>',
						     animation: false
						}) ;
                  });
                  
                  
                  
                      if(callback_){
                        callback_.apply(this,[pollids]);
                      }
                },
                error: function(e){alert(JSON.stringify(e));}
              });
        }  , 
      getEmbedContent: function(poll_id){
      
        if(RP_HOME.CLOUDFRONT){
          hostname_=RP_HOME.CLOUDFRONT;
        }
        else{
           var location_bar_ = $(location).attr('href'); 
           if(location_bar_ && location_bar_.indexOf(":")>-1){
                    hostname_=location_bar_.substring(location_bar_.indexOf(":")+3 ); 
                    hostname_=hostname_.substring(0,hostname_.indexOf("/") ); 
                  }
                  else{
                    hostname_=location_bar_.subsring(0,location_bar_.indexOf("/")); 
                  }
        }
                 
        return '<div id="'+poll_id+'" class="rollipollcontent" data-oid="'+poll_id+'"></div>'+
          ' <script async type="text/javascript" src="//'+hostname_+'/js/embedpoll.prod.js" async></script>'; 
      },
      saveUserAccount: function( user_settings ){
       var email_=user_settings['email'];
           if(!email_){
            alert("Email is not valid");
          }
          else
          {
            
            Parse.User.current().fetch().then(function(u){
               
               $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/rollipoll/profile/save/email/'+email_+'/'+u.id, 
                contentType: "application/json",
                 data: JSON.stringify(user_settings),
                success: function(msg){
                  alert("User settings saved.");
                  Parse.User.current().fetch().then(function (user) {
                        user.get('email');
                        currentUser = user;  
                    });
                 
                  $("#question-color").attr('data-initial',user_settings['question_color']);
                  $("#question-color").text( user_settings['question_color']);
                  $("#question-color").val( user_settings['question_color']);
                  $('#questioncolor').attr('data-initial',user_settings['question_color']);
                  $('#questioncolor').text( user_settings['question_color']);
                  $('#questioncolor').val( user_settings['question_color']);
                  $('#linkcolor').attr('data-initial',user_settings['linkcolor']);
                   $('#linkcolor').text(user_settings['linkcolor']);
                  $('#bgcolor').attr('data-initial',user_settings['bgcolor']);
                   $('#bgcolor').text(user_settings['bgcolor']);
                  $($('.dropdown-toggle')[0]).attr('data-initial',user_settings['question_font']);
                  $($('.dropdown-toggle')[1]).attr('data-initial',user_settings['answer_font']);
                  $('#questioncolor').removeClass('isDirty');
                    $('#linkcolor').removeClass('isDirty');
                   $('#bgcolor').removeClass('isDirty');
                  $($('.dropdown-toggle')[0]).removeClass('isDirty');
                  $($('.dropdown-toggle')[1]).removeClass('isDirty');
                  $('input').each(function(i){ 
                        $($('input')[i]).removeClass('isDirty'); 
                  });
                },
                error: function(err){
                  alert("Error "+JSON.stringify(err));
                }
              });
          });
      }
      },
      saveData : function(user_,poll_data_, embed_div,embed_div_header ){
               poll_data_["user"]=user_;
              if(   $('#save').attr('data-poid') )
              { 
                poll_data_['old_pollid']= $('#save').attr('data-poid') ;
              }

        var answer_length_ = $('.answer').length;
 
        var viewportHeight = window.outerHeight;//$(window).height();
         var displayEmbed = $('.embed').css('display');
        if(answer_length_>2){
            viewportHeight = (   ( ( answer_length_ -1 )*120 ) ) + viewportHeight;
                  $('.showspinner').css('height',''+viewportHeight+'px');
              var perc_=70  ; 
              perc_ = perc_ + (( answer_length_ -2 )*6);
            $('.spinner').css('top',perc_+'%');
        }
     
            $('.showspinner').css('height',''+viewportHeight+'px');
 
      
// 		$('.showspinner').modal({show:true});
        RP_HOME.utils.showSpinner(true);
              $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: '/rollipoll/save',
                data: JSON.stringify(poll_data_),
                contentType: "application/json",
                success: function(msg){
                    var oid = typeof msg.poll.objectId==="undefined"?msg.poll.poll.objectId:msg.poll.objectId;
                    
                    embed_div.text(  
                      '<div  id="'+oid+'"  class="rollipollcontent" data-oid="'+oid+'"></div>'+
          '<script type="text/javascript" src="//'+hostname_+'/js/embedpoll.js" async></script>' );
                  var hostname_="";
                  var location_bar_ = $(location).attr('href'); 
                  if(location_bar_ && location_bar_.indexOf(":")>-1)
                  {
                    hostname_=location_bar_.substring(location_bar_.indexOf(":")+3 ); 
                    hostname_=hostname_.substring(0,hostname_.indexOf("/") ); 
                  }
                  else
                  {
                    hostname_=location_bar_.subsring(0,location_bar_.indexOf("/")); 
                  }
      $('#reset').css('opacity','0.07'); 
// 			$('.showspinner').modal('hide');
        RP_HOME.utils.showSpinner(false);
                  //alert("Poll successfuly saved.");
                  CRUD['cstate']='EDIT';
                  if(embed_div){ 
//                     alert(JSON.stringify(msg));
                    
                    var pid =   typeof msg.poll.objectId==="undefined"?msg.poll.poll.objectId:msg.poll.objectId;
//                      if(msg.poll.poll.previous_version  ){
//                        pid = msg.poll.poll.previous_version.objectId ;
//                      }
                            if(RP_HOME.CLOUDFRONT){
                                hostname_=RP_HOME.CLOUDFRONT;
                              }
                              else{
                                 var location_bar_ = $(location).attr('href'); 
                                 if(location_bar_ && location_bar_.indexOf(":")>-1){
                                          hostname_=location_bar_.substring(location_bar_.indexOf(":")+3 ); 
                                          hostname_=hostname_.substring(0,hostname_.indexOf("/") ); 
                                        }
                                        else{
                                          hostname_=location_bar_.subsring(0,location_bar_.indexOf("/")); 
                                        }
                              }

                    $('#reset').show();
                    $('#reset').attr('data-objectid',pid);
                    var oid = typeof msg.poll.objectId==="undefined"?msg.poll.poll.objectId:msg.poll.objectId;
                    $('#save').attr('data-poid',oid );
               
                     embed_div.text(  
                      '<div  id="'+oid+'" class="rollipollcontent" data-oid="'+oid+'"></div>'+
          '<script type="text/javascript" src="//'+hostname_+'/js/embedpoll.js" async></script>' );
                          $('#reset').css('visibility','visible');
                          RP_HOME.utils.show_embed(); 
                  }
                },
                error: function(e){alert(JSON.stringify(e) );}
              });
        },
        get_hostname : function(url) {
            var m = url.match(/^http:\/\/[^/]+/);
            return m ? m[0] : null;
        },
      loadSettings: function(callback_){
        Parse.User.current().fetch().then(function (user) {
//           alert(JSON.stringify(user));
                         $.ajax({
                              type: 'GET',
                              dataType: 'json',
                              url: '/rollipoll/user_settings/'+user.id,
                              contentType: "application/json",
                              success: function(msg){ 
                                  if(callback_){ 
                                    callback_.apply(this,[msg]);
                                  }
                              },
                              error: function(e){alert(JSON.stringify(e) );}
                          });
                    });
      },
        loadPoll : function(poll_id_, callback_){
              
            var poll_post={}; 
            poll_post['objectId']=poll_id_;
             
                $.ajax({
                      type: 'GET',
                      dataType: 'json',
                      url: '/rollipoll/poll/find/'+poll_id_,
                  contentType: "application/json",
                  success: function(msg){ 
                    load_saved_poll(msg); 
                    if(callback_){ 
                      callback_.apply(this,[msg]);
                    }
                  },
                  error: function(e){alert(JSON.stringify(e) );}
                });
          }  ,
      deletePoll : function(user_,poll_id_,callback_){
            var poll_post={};
            poll_post['user']=user_;
            poll_post['objectId']=poll_id_;
             
                $.ajax({
                      type: 'POST',
//                       dataType: 'json',
                      url: '/rollipoll/poll/delete',
                  data: JSON.stringify(poll_post),
                  contentType: "application/json",
                  success: function(msg){ 
                     if(callback_)
                      {
                        callback_.apply(this,[msg]);
                      }
                      else{
                         $('#rollipoll_'+poll_id_).hide();
                      }
                   
                  },
                  error: function(e){alert("ERROR on delete "+JSON.stringify(e) );}
                });
          }  ,
         deactivatePoll : function(user_,poll_id_,callback_){
            var poll_post={};
            poll_post['user']=user_;
            poll_post['objectId']=poll_id_;
             
                $.ajax({
                      type: 'POST',
//                       dataType: 'json',
                      url: '/rollipoll/poll/deactivate',
                  data: JSON.stringify(poll_post),
                  contentType: "application/json",
                  success: function(msg){  
                      if(callback_)
                      {
                        callback_.apply(this,[msg]);
                      }
                  },
                  error: function(e){alert("ERROR on delete "+JSON.stringify(e) );}
                });
          }  ,
         activatePoll : function(user_,poll_id_,callback_){
            var poll_post={};
            poll_post['user']=user_;
            poll_post['objectId']=poll_id_;
             
                $.ajax({
                      type: 'POST',
//                       dataType: 'json',
                      url: '/rollipoll/poll/activate',
                  data: JSON.stringify(poll_post),
                  contentType: "application/json",
                  success: function(msg){ 
                    if(callback_)
                      {
                        callback_.apply(this,[msg]);
                      } 
                  },
                  error: function(e){alert("ERROR on delete "+JSON.stringify(e) );}
                });
          }  ,
        signup : function( signup_obj ) 
        {   
           if(typeof(signup_obj['email'] )==='undefined' || signup_obj['email'] ===null || signup_obj['email'].trim().length  === 0){
             alert("Email must be filled out");
               $('#email').setCustomVality('Email must be filled out');
           }
          else if(typeof(signup_obj['first_name'] )==='undefined' || signup_obj['first_name'] ===null || signup_obj['first_name'].trim().length  === 0){
            alert("First Name must be filled out");
               $('#first_name').setCustomVality('First Name must be filled out');
           }
          else  if(typeof(signup_obj['last_name'] )==='undefined' || signup_obj['last_name'] ===null || signup_obj['last_name'].trim().length  === 0){
             alert("Last Name must be filled out");
               $('#last_name').setCustomVality('Last Name must be filled out');
           }
          else if(typeof(signup_obj['password'] )==='undefined' || signup_obj['password'] ===null || signup_obj['password'].trim().length  === 0 ){
            alert("Password must be filled out");
               $('#password').setCustomVality('Password must be filled out');
           } 
           else{
                  var user = new Parse.User();
                  user.set("username", signup_obj['email'] );
                  user.set("password", signup_obj['password']);
                  user.set("email", signup_obj['email'] );
                  user.set("first_name", signup_obj['first_name']);
                  user.set("last_name", signup_obj['last_name'] );
                  
                  user.signUp(null, {
                    success: function(user) {
                      alert("Sign Up successful");
                      $(location).attr('href',signup_obj['url']); 
                    },
                    error: function(user, error) {
                      // Show the error message somewhere and let the user try again.
                      if(error.code===202){
                        alert("User already exists, Please sign in.");
                        var l_obj ={};
                        l_obj['user']=signup_obj['email'];
                        l_obj['password']=signup_obj['password'];
                        l_obj['url']=signup_obj['url'];
                        l_obj['error_url']=signup_obj['already_exists_url']; 
                        $(location).attr('href',signup_obj['already_exists_url']); 
                      }
                      else{
                         alert("Error: " + error.code + " " + error.message);
                      }
                     
                    }
                  });
           }
         
        } 
    }; 
    
    $.fn.getCursorPosition = function() {  
        var el = $(this).get(0);  
        var pos = 0;  
        if ('selectionStart' in el) {  
            pos = el.selectionStart;  
        } else if ('selection' in document) {  
            el.focus();  
            var Sel = document.selection.createRange();  
            var SelLength = document.selection.createRange().text.length;  
            Sel.moveStart('character', -el.value.length);  
            pos = Sel.text.length - SelLength;  
        }  
        return pos;  
    }           
    $.fn.rollipollauth = function(methodOrOptions) {
        if ( methods[methodOrOptions] ) {
            return methods[ methodOrOptions ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof methodOrOptions === 'object' || ! methodOrOptions ) {
            // Default to "init"
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  methodOrOptions + ' does not exist on jQuery.tooltip' );
        }    
    };


})( jQuery );
