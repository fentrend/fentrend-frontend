var app = angular.module('mgcrea.ngStrapDocs', ['ngAnimate', 'ngSanitize', 'mgcrea.ngStrap','ngMaterial']);

app.controller('MainCtrl', function($scope) {
});

'use strict';

angular.module('mgcrea.ngStrapDocs').config(function($asideProvider) {
  angular.extend($asideProvider.defaults, {
    container: 'body',
    html: true
  });
}).directive('multipleSelect', function(){
  return {
        scope: {
            labelName: '@labelName',
            formName: '@formName',
            chooseItems: '=chooseItems',
            selectItems: '=selectItems'
        },
        replace: true,
        templateUrl: 'multiple-select.html'
  };
}).directive('singleSelect', function(){
  return {
        scope: {
            labelName: '@labelName',
            formName: '@formName',
            chooseItems: '=chooseItems',
            selectItems: '=selectItems'
        },
        replace: true,
        templateUrl: 'single-select.html'
  };
}).directive('commentsSection', function(){
  return {
        restrict: 'E',
        scope: { 
            formName: '@formName', 
            modelValue: '=modelValue'
        }, 
        templateUrl: 'comments-section.html'
  };
}).directive('uploadFile', function(){
  return {
        restrict: 'E',
        scope: {
            labelName: '@labelName',
            formName: '@formName', 
            modelValue: '=modelValue'
        }, 
        templateUrl: 'upload-file.html'
  };
}).directive('uploadFileRemovable', function(){
  return {
        restrict: 'E',
        scope: {
            labelName: '@labelName',
            formName: '@formName', 
            modelValue: '=modelValue'
        }, 
        templateUrl: 'upload-file-removable.html'
  };
}).directive('textItem', function(){
  return {
        restrict: 'E',
        scope: {
            labelName: '@labelName',
            formName: '@formName',
            placeholderText: '@placeholderText',
            modelValue: '=modelValue'
        }, 
        templateUrl: 'text-item.html'
  };
}).directive('numberItem', function(){
  return {
        restrict: 'E',
        scope: {
            labelName: '@labelName',
            formName: '@formName',
            minValue: '@minValue',
            placeholderText: '@placeholderText',
            modelValue: '=modelValue'
        }, 
        templateUrl: 'number-item.html'
  };
}).directive('textSelect', function(){
  return {
        restrict: 'E',
        scope: {
            labelName: '@labelName',
            formName: '@formName',
            formName: '@formNameSelect',
            placeholderText: '@placeholderText',
            modelValue: '=modelValue',
            chooseItems: '=chooseItems',
            selectItems: '=selectItems'
        }, 
        templateUrl: 'text-select.html'
  };
}).directive('textSelectFull', function(){
  return {
        restrict: 'E',
        scope: {
            labelName: '@labelName',
            formName: '@formName',
            formName: '@formNameSelect',
            placeholderText: '@placeholderText',
            modelValue: '=modelValue',
            chooseItems: '=chooseItems',
            selectItems: '=selectItems'
        }, 
        templateUrl: 'text-select-full.html'
  };
}).directive('fentrendFooter',function(){
  return {
      restrict: 'E',
      templateUrl: 'fentrend-footer.html'
  };
}).directive('saveButton',function(){
  return {
      restrict: 'E',
      templateUrl: 'save-button.html'
  };
}).directive('polymerTypeahead', function(){
  return {
        
        restrict: 'E',   
        link: function (scope, element, attrs) {
            attrs.$observe("labelName", function (newValue) {
                element.find('label').text(newValue);
            });
           attrs.$observe("formName", function (newValue) {
                element.find('label').attr('for',newValue);
                element.find('input').attr('id',newValue);
                element.find('input').attr('name',newValue); 
            });
           attrs.$observe("placeholderText", function (newValue) { 
                element.find('input').attr('placeholder',newValue);  
            });
        }, 
        replace: true,
        templateUrl: 'polymer-typeahead.html'
  };
}).directive('performanceChart', function(){
  return { 
        restrict: 'E',   
         scope: {
            chartId: '=' 
        }, 
        link: function (scope, element, attrs) { 
         
         var createNewChart =  function(numCompares,elem){ 
                 
                      if(numCompares===2){
                        drawAxisTick2Colors(elem); 
                      }
                      else if(numCompares===3){
                          drawAxisTick3Colors(elem); 
                      }
                      else if(numCompares===4){
                          drawAxisTick4Colors(elem); 
                      }
                      else if(numCompares===5){
                          drawAxisTick5Colors(elem); 
                      } 
            };
                createNewChart(2,element);
        }, 
        replace: true,
        templateUrl: 'r-value-chart.html'
  };
}).directive('rValueChart', function(){
  return { 
        restrict: 'E',   
         scope: {
            chartId: '=' 
        }, 
        link: function (scope, element, attrs) { 
          var sizeCharts = function(el){
             
                var w = el.width();
                var h = el.height(); // or just change this to var h = 40
                el.find('img').attr("src","http://chart.apis.google.com/chart?chs=" +   escape(w) + "x" + escape(h) + "&[etc]").appendTo(el); 
        };
         var createNewChart =  function(numCompares,elem){ 
                 
                      if(numCompares===2){
                        drawVisualization2Col(elem); 
                      }
                      else if(numCompares===3){
                          drawVisualization3Col(elem); 
                      }
                      else if(numCompares===4){
                          drawVisualization4Col(elem); 
                      }
                      else if(numCompares===5){
                          drawVisualization5Col(elem); 
                      }
                      else if(numCompares===6){
                          drawVisualization6Col(elem);
                      } 
 
            };
           sizeCharts(element);
                    var shouldResize = true;
                    $(window).bind("resize",function(){
                        if(!shouldResize){
                            return;
                        }
                        shouldResize = false;
                        setTimeout(function(){
                            sizeCharts(element);
                            shouldResize = true;
                        },1000);
                    });
                createNewChart(2,element);
        }, 
        replace: true,
        templateUrl: 'r-value-chart.html'
  };
}).directive('rValueChartDoor', function(){
  return { 
        restrict: 'E',   
         scope: {
            chartId: '=' 
        }, 
        link: function (scope, element, attrs) { 
         var createNewChart =  function(numCompares, elem){ 
                 
                      if(numCompares===2){
                        drawVisualization2Col(elem); 
                      }
                      else if(numCompares===3){
                          drawVisualization3Col(elem); 
                      }
                      else if(numCompares===4){
                          drawVisualization4Col(elem); 
                      }
                      else if(numCompares===5){
                          drawVisualization5Col(elem); 
                      }
                      else if(numCompares===6){
                          drawVisualization6Col(elem);
                      } 
 
            };
                createNewChart(2, element);
        }, 
        replace: true,
        templateUrl: 'r-value-chart.html'
  };
}).directive('rValueChartSkylight', function(){
  return { 
        restrict: 'E',   
         scope: {
            chartId: '=' 
        }, 
        link: function (scope, element, attrs) { 
         var createNewChart =  function(numCompares,elem){ 
                 
                      if(numCompares===2){
                        drawVisualization2Col(elem); 
                      }
                      else if(numCompares===3){
                          drawVisualization3Col(elem); 
                      }
                      else if(numCompares===4){
                          drawVisualization4Col(elem); 
                      }
                      else if(numCompares===5){
                          drawVisualization5Col(elem); 
                      }
                      else if(numCompares===6){
                          drawVisualization6Col(elem);
                      } 
 
            };
                createNewChart(2,element);
        }, 
        replace: true,
        templateUrl: 'r-value-chart.html'
  };
}).directive('addressLookup', function(){
  return {
        
        restrict: 'E',   
        link: function (scope, element, attrs) {
            attrs.$observe("labelName", function (newValue) {
                element.find('label').text(newValue);
            });
           attrs.$observe("formName", function (newValue) {
                element.find('label').attr('for',newValue);
                element.find('input').attr('id',newValue);
                element.find('input').attr('name',newValue); 
            });
           attrs.$observe("placeholderText", function (newValue) { 
                element.find('input').attr('placeholder',newValue);  
            });
        },
//         scope: {
//             labelName: '@labelName',
//             formName: '@formName',
//             placeholderText: '@placeholderText',
//             modelValue: '=modelValue'
//         }, 
        replace: true,
        templateUrl: 'address-lookup.html'
  };
}).directive('textSearchItem', function() {
  return {
    restrict: 'E', 
    scope: {
      formName:'=',
      formId:'=',
      formType:'='
    },
     template: '  <div class="input-group custom-search-form">  <input type="{{formType}}" class="form-control" name="{{formId}}" id="{{formId}}" placeholder="{{formName}}" >  <span class="input-group-btn"> <button class="btn btn-default" type="button"  > <i class="fa fa-times"></i> </button>  </span> </div>'
  };  
}).controller('FtNavController',function($scope,  $mdBottomSheet,  $mdToast, $mdSidenav,$mdUtil,$log){
           $scope.showGridBottomSheet = function($event) {
            $scope.alert = '';
            $mdBottomSheet.show({
             templateUrl: 'bottom-sheet-grid-template.html',
              controller: 'GridBottomSheetCtrl',
              targetEvent: $event
            }).then(function(clickedItem) {
              $mdToast.show(
                    $mdToast.simple()
                      .content(clickedItem.name + ' clicked!')
                      .position('top right')
                      .hideDelay(1500)
                  );
            });
          };
  
  
  
  // Menu items
 	$scope.menu = [
    {
      link : '',
      listitem:'Window',
      title: 'Window',
      icon: 'action:ic_dashboard_24px' // we have to use Google's naming convention for the IDs of the SVGs in the spritesheet
    },
    {
      link : '',
      listitem: 'Door',
      title: 'Door',
      icon: 'social:ic_group_24px'
    },
    {
      link : '',
      listitem: 'SkyLight',
      title: 'Sky Light',
      icon: 'communication:ic_message_24px'
    }
    
  ];
  $scope.admin = [
    {
      link : '',
      listitem:'Company',
      title: 'Company',
      icon: 'action:ic_delete_24px'
    },
    {
      link : 'showListBottomSheet($event)',
      title: 'Glazing Company',
      listitem:'GlazingCompany',
      icon: 'action:ic_settings_24px'
    }
  ]; 
  
  
  
  
 $scope.onSwipeLeft = function(ev) {
      $('#nav-icon1').toggleClass('open');
       $scope.toggleLeft();
    };
 
    $scope.onSwipeRight = function(ev) {
      alert('You swiped right!!');
    };
    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
 $scope.showGridBottomSheet = function($event) {
    $scope.alert = '';
    $mdBottomSheet.show({
     templateUrl: 'bottom-sheet-grid-template.html',
      controller: 'GridBottomSheetCtrl',
      targetEvent: $event
    }).then(function(clickedItem) {
      $mdToast.show(
            $mdToast.simple()
              .content(clickedItem.name + ' clicked!')
              .position('top right')
              .hideDelay(1500)
          );
    });
  };
    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildToggler(navID) { 
      var debounceFn =  $mdUtil.debounce(function(){
        
            $mdSidenav(navID)
              .toggle()
              .then(function () {
                $log.debug("toggle " + navID + " is done");
              
              });
          },200);

      return debounceFn;
    }
}).controller('GridBottomSheetCtrl', function($scope, $mdBottomSheet) {
  $scope.items = [
    { name: 'Create', icon: 'add' },
    { name: 'Edit', icon: 'create' },
    { name: 'Delete', icon: 'close' },
    { name: 'Copy', icon: 'content_copy' },
  ];

  $scope.listItemClick = function($index) {
    var clickedItem = $scope.items[$index];
    $mdBottomSheet.hide(clickedItem);
  };
}).controller('AsideDemoCtrl', function($scope) {
  $scope.aside = {title: 'fentrend', content: 'Hello Aside<br />This is a multiline message!'};
}).controller('TypeaheadDemoCtrl', function($scope, $templateCache, $http) {

  $scope.selectedSearch = '';
  $scope.searches = [
    {name: 'Company Name', isSelected: false , formType: 'text', formId:'companyName'},
    {name: 'Company Website', isSelected: false, formType: 'text',formId:'companyWebsite'},
    {name: 'Company Address', isSelected: false, formType: 'text',formId: 'companyAddress'},
    {name: 'Company City', isSelected: false, formType: 'text',formId:'companyCity'},
    {name: 'US Affiliate Phone', isSelected: false, formType: 'text',formId:'hasAffiliatePhone'},
    {name: 'US Affiliate Address', isSelected: false, formType: 'text',formId:'hasAffiliateAddress'},
    {name: 'US Affiliate City', isSelected: false, formType: 'text',formId:'usAffiliateCity'},
    {name: 'US Affiliate Zipcode', isSelected: false, formType: 'text',formId:'usAffiliateZipcode'},
    {name: 'US Affiliate State', isSelected: false, formType: 'text',formId: 'usAffiliateState'},
    {name: 'US Affiliate Revenue', isSelected: false, formType: 'text',formId: 'usAffiliateRevenue'},
    {name: 'US Affiliate Country', isSelected: false, formType: 'text',formId:'usAffiliateCountry'},
    {name: 'US Affiliate Sales Contact First Name', isSelected: false, formType: 'text',formId:'usAffiliateSalesContactFirstName'},
    {name: 'US Affiliate Sales Contact Last Name', isSelected: false, formType: 'text',formId:'usAffiliateSalesContactLastName'},
    {name: 'US Affiliate Sales Contact Phone', isSelected: false, formType: 'text',formId:'usAffiliateSalesContactPhone'},
    {name: 'US Affiliate Sales Contact Skype', isSelected: false, formType: 'text',formId:'usAffiliateSalesContactSkype'},
    {name: 'US Affiliate Service Partners Name', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersName'},
    {name: 'US Affiliate Service Partners Website', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersWebsite'},
    {name: 'US Affiliate Service Partners Phone', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersPhone'},
    {name: 'US Affiliate Service Partners Address', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersAddress'},
    {name: 'US Affiliate Service Partners City', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersCity'},
    {name: 'US Affiliate Service Partners State', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersState'},
    {name: 'US Affiliate Service Partners Zipcode', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersZipcode'},
    {name: 'US Affiliate Service Partners Country', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersCountry' },
    {name: 'US Affiliate Service Partners Number', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersNumber' },
    {name: 'US Affiliate Misc Info Pricing Email', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersPricingEmail' },
    {name: 'US Affiliate Misc Info Quoting Email', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersQuotingEmail' },
    {name: 'US Affiliate Misc Info Order Lead Time', isSelected: false, formType: 'text',formId:'usAffiliateServicePartnersOrderLeadTime' },
    {name: 'US Affiliate Warranty Period', isSelected: false, formType: 'text',formId:'usAffiliateWarrantyPeriod' }, 
   
    
    
    {name: 'Glazing Company Name', isSelected: false, formType: 'text',formId:'glazingCompanyName' }, 
    {name: 'Glazing Company Website', isSelected: false, formType: 'text',formId:'glazingCompanyWebsite' }, 
    {name: 'Glazing Company Type', isSelected: false, formType: 'text',formId:'glazingCompanyType' }, 
    {name: 'Glazing Company Spacer Type', isSelected: false, formType: 'text',formId:'glazingCompanySpacerType' }, 
    {name: 'Glazing Company Spacer Performance', isSelected: false, formType: 'text',formId:'glazingCompanySpacerPerformance' }, 
    {name: 'Glazing Company Gas Type', isSelected: false, formType: 'text',formId:'glazingCompanyGasType' }, 
    {name: 'Glazing Company Glass Used', isSelected: false, formType: 'text',formId:'glazingCompanyGlassUsed' }, 
    {name: 'Glazing Company Film Used', isSelected: false, formType: 'text',formId:'glazingCompanyFilmUsed' }, 
    {name: 'Glazing Company Film Surface', isSelected: false, formType: 'text',formId:'glazingCompanyFilmSurface' }, 
    {name: 'Glazing Company SHGC', isSelected: false, formType: 'text',formId:'glazingCompanySHGC' }, 
    {name: 'Glazing Company Visual Percent', isSelected: false, formType: 'text',formId:'glazingCompanyVisualPercent' }, 
    {name: 'Glazing Company Visual Total OD', isSelected: false, formType: 'text',formId:'glazingCompanyTotalOD' }, 
    {name: 'Glazing Company Parent Company Name', isSelected: false, formType: 'text',formId:'glazingCompanyParentCompanyName' },  
    {name: 'Glazing Company Comments', isSelected: false, formType: 'text',formId:'glazingCompanyComments' },  
    {name: 'Company Comments', isSelected: false, formType: 'text',formId:'companyComments' },   
    {name: 'US Affiliate Years In Business', isSelected: false, formType: 'text',formId:'companyUSAffiliateYearsInBusiness'},
    {name: 'US Affiliate Revenue', isSelected: false, formType: 'text',formId:'companyUSAffiliateRevenue'},
    {name: 'US Affiliate Number Of Employees', isSelected: false, formType: 'text',formId:'companyUSAffiliateNumberOfEmployees'},
    
    {name: 'Window Name', isSelected: false, formType: 'text',formId:'windowName'},
    {name: 'Window Company', isSelected: false, formType: 'text',formId: 'windowCompany'},  
    {name: 'Window Detailed Design Operation Type', isSelected: false, formType: 'text',formId: 'windowDetailedDesignOperationType'}, 

    {name: 'Window Design Details', isSelected: false, formType: 'text',formId: 'windowDesignDetails'},
    {name: 'Window Width', isSelected: false, formType: 'text',formId: 'windowWidth' },
    {name: 'Window Height', isSelected: false, formType: 'text',formId: 'windowHeight' },
    {name: 'Window Exceptions per review', isSelected: false, formType: 'text',formId: 'windowExceptionsPerReview'},
    {name: 'Window Frame Width Sides', isSelected: false, formType: 'text',formId: 'windowFrameWidthSides'},
    {name: 'Window Frame Width Top', isSelected: false, formType: 'text',formId: 'windowFrameWidthTop'},
    {name: 'Window Frame Width Bottom', isSelected: false, formType: 'text',formId: 'windowFrameWidthBottom'},
    {name: 'Window Frame Width Frame Depth', isSelected: false, formType: 'text',formId: 'windowFrameWidthFrameDepth'},
    {name: 'Window Hardware Type', isSelected: false, formType: 'text',formId: 'windowHardwareType'},
    {name: 'Window Mulled Glass to Glass Dimension', isSelected: false, formType: 'text',formId: 'windowMulledGlassToGlassDimension'},
    {name: 'Window Operable to Operable', isSelected: false, formType: 'text',formId: 'windowOperableToOperable'},
    {name: 'Window Fixed to Fixed', isSelected: false, formType: 'text',formId: 'windowFixedToFixed'},
 
    {name: 'Company Has US Affiliate', isSelected: false, formType: 'select',formId:'hasAffiliate',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    }, 
    {name: 'Company Has Service Partners', isSelected: false, formType: 'select',formId:'hasServicePartners',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },
    {name: 'Company Has Measurement Services', isSelected: false, formType: 'select',formId:'hasMeasurementServices',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },
    {name: 'Company Has Comments', isSelected: false, formType: 'select',formId:'hasCompanyComments',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },
    {name: 'Company Has Warranty Document', isSelected: false, formType: 'select',formId:'hasWarrantyDocument',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },
    {name: 'Glazing Company Has Comments', isSelected: false, formType: 'select',formId:'hasWarrantyDocument',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },
    {name: 'Glazing Comany Has Test Reports', isSelected: false, formType: 'select',formId:'hasTestReports',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },  
    {name: 'Glazing Comany Has Purchasing Prices', isSelected: false, formType: 'select',formId:'hasPurchasingPrices',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },    
    {name: 'Glazing Comany Has Installation Docs', isSelected: false, formType: 'select',formId:'hasInstallationDocs',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },  
    {name: 'Glazing Comany Has Shipping and Handling Docs', isSelected: false, formType: 'select',formId:'hasShippingAndHandlingDocs',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },  
    {name: 'Glazing Comany Has Misc Docs', isSelected: false, formType: 'select',formId:'hasMicsDocs',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },  
    {name: 'Window Fabrication Classes', isSelected: false, formType: 'select',formId: 'windowFabricationClasses',
     icons : [
        {value: 'Assembly Class 1', label: '<i class="fa fa-glass"></i> Assembly Class 1'},
        {value: 'Assembly Class 2', label: '<i class="fa fa-glass"></i>  Assembly Class 2'},
        {value: 'Assembly Class 3', label: '<i class="fa fa-glass"></i>  Assembly Class 3'},
        {value: 'Assembly Class 4', label: '<i class="fa fa-glass"></i>  Assembly Class 4'},
        {value: 'Assembly Class 5', label: '<i class="fa fa-glass"></i>  Assembly Class 5'},
        {value: 'Assembly Class 6', label: '<i class="fa fa-glass"></i>  Assembly Class 6'}
      ],
      selectedIcon:''},    
    {name: 'Window Has Clad Interior', isSelected: false, formType: 'select',formId:'hasCladInterior',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },    
    {name: 'Window Current or Discontinued', isSelected: false, formType: 'select',formId:'windowIsCurrent',
     icons : [
        {value: 'Current', label: '<i class="fa fa-thumbs-up"></i> Current'},
        {value: 'Discontinued', label: '<i class="fa fa-thumbs-down"></i> Discontinued'}
      ],
     selectedIcon:''
    },   
    {name: 'Window Requires Updated Info', isSelected: false, formType: 'select',formId:'windowRequiresUpdatedInfo',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },  
    {name: 'Window  Design Details Has Casement', isSelected: false, formType: 'select',formId:'windowDesignDetailsHasCasement',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },    
    {name: 'Window  Design Details Has Till-Turn', isSelected: false, formType: 'select',formId:'windowDesignDetailsHasTillTurn',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },     
    {name: 'Window Frame Material Has Wood', isSelected: false, formType: 'select',formId:'windowFrameMaterialHasWood',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },    
    {name: 'Window Frame Material Has Vinyl', isSelected: false, formType: 'select',formId:'windowFrameMaterialHasVinyl',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },        
    {name: 'Window Frame Material Has Aluminum', isSelected: false, formType: 'select',formId:'windowFrameMaterialHasAluminum',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },        
    {name: 'Window Has Exceptions Per Review', isSelected: false, formType: 'select',formId:'windowHasExceptionsPerReview',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },    
    {name: 'Window Has Clad Interior', isSelected: false, formType: 'select',formId:'windowHasCladInterior',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },      
    {name: 'Window Has Clad Exterior', isSelected: false, formType: 'select',formId:'windowHasCladExterior',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },       
    {name: 'Window Has Exterior Install Flange', isSelected: false, formType: 'select',formId:'windowHasExteriorInstallFlange',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },       
    {name: 'Window Unit comes 100% factory installed', isSelected: false, formType: 'select',formId:'windowComes100FactoryInstalled',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },       
    {name: 'Window Has Exterior Included', isSelected: false, formType: 'select',formId:'windowHasExteriorIncluded',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },        
    {name: 'Window Has Interior Included', isSelected: false, formType: 'select',formId:'windowHasInteriorIncluded',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },        
    {name: 'Window Has DH Profile', isSelected: false, formType: 'select',formId:'windowHasDhProfile',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },       
    {name: 'Window Has Interior Shade', isSelected: false, formType: 'select',formId:'windowHasInteriorShade',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },       
    {name: 'Window Has Exterior Shade', isSelected: false, formType: 'select',formId:'windowHasExteriorShade',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },       
    {name: 'Window Has Vertical Glass to Glass Dimension at Mullion', isSelected: false, formType: 'select',formId:'windowHasVerticalGlassToGlassDimensionAtMullion',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },        
    {name: 'Window Has A Threshold', isSelected: false, formType: 'select',formId:'windowHasAThreshold',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },        
    {name: 'Window Is ADA Compliant', isSelected: false, formType: 'select',formId:'windowIsAdaCompliant',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },      
    {name: 'Window Has Threshold Testing Data', isSelected: false, formType: 'select',formId:'windowHasThresholdTestingData',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },       
    {name: 'Window Has Standard Handle Image', isSelected: false, formType: 'select',formId:'windowHasStandardHandleImage',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },     
    {name: 'Window Has Color Options Data', isSelected: false, formType: 'select',formId:'windowHasStandardColorOptionsData',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },     
    {name: 'Window Has Hardware Catalog', isSelected: false, formType: 'select',formId:'windowHasHardwareCatalogData',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },      
    {name: 'Window Has Performance Data', isSelected: false, formType: 'select',formId:'windowHasPerformanceData',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },      
    {name: 'Window Has Total Window Performance Data', isSelected: false, formType: 'select',formId:'windowHasTotalWindowPerformanceData',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },      
    {name: 'Window Has Frame U-Value Test Data', isSelected: false, formType: 'select',formId:'windowHasFrameUValueTestData',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },     
    {name: 'Window Has PH Certification Report', isSelected: false, formType: 'select',formId:'windowHasPHCertificationReport',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },     
    {name: 'Window Has Acpistoc Performance Data', isSelected: false, formType: 'select',formId:'windowHasAcpistocPerformanceData',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },   
    {name: 'Window Is Fire Rated', isSelected: false, formType: 'select',formId:'windowIsFireRated',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },    
    {name: 'Window Has Fire Rated Components Offerred', isSelected: false, formType: 'select',formId:'windowHasFireRatedComponentsOfferred',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    },   
    {name: 'Window Has Fire Rated Certification Data', isSelected: false, formType: 'select',formId:'windowHasFireRatedCertificationData',
     icons : [
        {value: 'Yes', label: '<i class="fa fa-thumbs-up"></i> Yes'},
        {value: 'No', label: '<i class="fa fa-thumbs-down"></i> No'}
      ],
     selectedIcon:''
    }   
 ];
  
  $scope.selectedState = '';
  $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

  $scope.selectedIcon = '';
  $scope.icons = [
    {value: 'Gear', label: '<i class="fa fa-gear"></i> Gear'},
    {value: 'Globe', label: '<i class="fa fa-globe"></i> Globe'},
    {value: 'Heart', label: '<i class="fa fa-heart"></i> Heart'},
    {value: 'Camera', label: '<i class="fa fa-camera"></i> Camera'}
  ];

  $scope.selectedAddress = '';
  $scope.getAddress = function(viewValue) {
    var params = {address: viewValue, sensor: false};
    return $http.get('https://maps.googleapis.com/maps/api/geocode/json', {params: params})
    .then(function(res) {
      return res.data.results;
    });
  };
   $scope.$watch("selectedSearch", function(value) {
 
     if(typeof value ==='object' && !value.isSelected){
       value.isSelected=true;
     }
//       $scope.isDisabled = value ? true : false;

  });
}).filter('itemIsSelected', function () {
  return function (items) {
    var filtered = [];
    for (var i = 0; i < items.length; i++) {
      var item = items[i];
      if (typeof item ==='object' && item.isSelected) {
        filtered.push(item);
      }
    }
    return filtered;
  };
}).controller('CompanyCreateController', function($scope, $templateCache, $http) {
     $scope.master = {};
     
      $scope.update = function(company) {
        $scope.master = angular.copy(company);
      };

      $scope.reset = function() {
        $scope.company = angular.copy($scope.company);
      };
      
      $scope.getRequestedAddress = function(viewValue) {
        var params = {address: viewValue, sensor: false};
        return $http.get('https://maps.googleapis.com/maps/api/geocode/json', {params: params})
        .then(function(res) {
          return res.data.results;
        });
      };
      
      $scope.removeServicePartners = function(index){
          // remove the row specified in index
          $scope.company.servicePartners.splice( index, 1);
          // if no rows left in the array create a blank array
          if ($scope.company.servicePartners.length === 0){
            $scope.company.servicePartners = [];
          }
        };  
      $scope.addServicePartner = function(){
          // remove the row specified in index
          $scope.company.servicePartners.push({website:'',name:'',address:''}); 
      };  
      
      $scope.removeRevenueYear=function(index){
          $scope.company.revenueYears.splice(index,1);
          if($scope.company.revenueYears.length === 0){
            $scope.company.reveneueYears=[];
          }
      };
      $scope.addRevenueYear=function(){
          $scope.company.revenueYears.push({year:'',revenue: 0});
      };
      $scope.company={name:'',website:'',address:''};
        $scope.company.shipping = {name: 'shipping', isSelected: false, formType: 'select',formId:'shipping',
             icons : [
                {value: 'Included in price', label: '<i class="fa   fa-windows"></i> Included in price'},
                {value: 'Offered at additional cost', label: '<i class="fa   fa-windows"></i> Offered at additional cost'}  
              ],
             selectedIcon:''
            }; 
  $scope.company.shopDrawing = {name: 'ShopDrawings', isSelected: false, formType: 'select',formId:'shopDrawings',
             icons : [
                {value: 'Included Insured', label: '<i class="fa   fa-windows"></i> Included Insured'},
                {value: 'Quoted Insured', label: '<i class="fa   fa-windows"></i> Quoted Insured'} ,
                {value: 'Third Party', label: '<i class="fa   fa-windows"></i> Third Party'}   
              ],
             selectedIcon:''
            }; 

      $scope.company.orderLeadTime = {name: 'Units', isSelected: false, formType: 'select',formId:'orderLeadTime',
             icons : [
                {value: '< 6 Weeks', label: '<i class="fa   fa-windows"></i> < 6 Weeks'},
                {value: '6-8 Weeks', label: '<i class="fa   fa-windows"></i> 6-8 Weeks'} ,
                {value: '8-12 Weeks', label: '<i class="fa   fa-windows"></i> 8-12 Weeks'}  ,
                {value: '12+ Weeks', label: '<i class="fa   fa-windows"></i> 12+ Weeks'}  
              ],
             selectedIcon:''
            };  
      $scope.company.servicePartners=[];
      $scope.company.revenueYears=[{year: '2014', revenue: 0}];
      $scope.company.warranty={period:'1Y'};
      $scope.company.products =  ['Windows'];
       $scope.company.hasServicePartnersAvailable=false;
      $scope.hSP=function(){
          $scope.company.hasServicePartnersAvailable=! $scope.company.hasServicePartnersAvailable;
          return $scope.company.hasServicePartnersAvailable;
      };

      $scope.company.services =  ['Service Partners'];
       $scope.company.servicesOfferred={name: 'servicesOfferred', isSelected: false,  formType: 'select',formId:'servicesOfferred',
           icons : [
              {value: 'Service Partners', label: '<i class="fa  fa-male"></i> Service Partners'},
              {value: 'Measurement Services', label: '<i class="fa fa-building-o"></i> Measurement Services'},
              {value: 'Install Services', label: '<i class="fa fa-lightbulb-o"></i> Install Services'} ]
          };
      $scope.company.productsOfferred={name: 'productsOfferred', isSelected: false,  formType: 'select',formId:'productsOfferred',
           icons : [
              {value: 'Windows', label: '<i class="fa fa-windows"></i> Windows'},
              {value: 'Doors', label: '<i class="fa fa-building-o"></i> Doors'},
              {value: 'Skylights', label: '<i class="fa fa-lightbulb-o"></i> Skylights'},
              {value: 'Historic', label: '<i class="fa fa-history"></i> Historic'},
              {value: 'Solid Panel', label: '<i class="fa fa-diamond"></i> Solid Panel'},
              {value: 'Glass only', label: '<i class="fa fa-glass"></i> Glass only'} ]
          };
      $scope.company.yearSelect={name: 'Years', isSelected: false, formType: 'select',formId:'yearSelect',
           icons : [
              {value: '2005', label: '<i class="fa fa-calendar"></i> 2005'},
              {value: '2006', label: '<i class="fa fa-calendar"></i> 2006'},
              {value: '2007', label: '<i class="fa fa-calendar"></i> 2007'},
              {value: '2008', label: '<i class="fa fa-calendar"></i> 2008'},
              {value: '2009', label: '<i class="fa fa-calendar"></i> 2009'},
              {value: '2010', label: '<i class="fa fa-calendar"></i> 2010'},
              {value: '2011', label: '<i class="fa fa-calendar"></i> 2011'},
              {value: '2012', label: '<i class="fa fa-calendar"></i> 2012'},
              {value: '2013', label: '<i class="fa fa-calendar"></i> 2013'},
              {value: '2014', label: '<i class="fa fa-calendar"></i> 2014'}  
            ],
           selectedIcon:''
          };
      $scope.reset();
  
}).controller('ProductCreateController', function($scope, $templateCache, $http) {
     $scope.master = {};
     
      $scope.update = function(product) {
        $scope.master = angular.copy(product);
      };

      $scope.reset = function() {
        $scope.product = angular.copy($scope.product);
      };
      
      $scope.getRequestedAddress = function(viewValue) {
        var params = {address: viewValue, sensor: false};
        return $http.get('https://maps.googleapis.com/maps/api/geocode/json', {params: params})
        .then(function(res) {
          return res.data.results;
        });
      };
      
//       $scope.removeRevenueYear=function(index){
//           $scope.company.revenueYears.splice(index,1);
//           if($scope.company.revenueYears.length === 0){
//             $scope.company.reveneueYears=[];
//           }
//       };
//       $scope.addRevenueYear=function(){
//           $scope.company.revenueYears.push({year:'',revenue: 0});
//       };
      $scope.product={name:'', company:'',glazingcompany:'',current:'Current' ,requiresUpdatedInfo: 'No' , operationType: 'window', units: '' , unitsType: 'inches',comments: '', isoUValueType: 'SI', nfrcUValueType:'IP',standardAcousticRatingType:'OITC',minAcousticRatingType:'OITC',maxAcousticRatingType:'OITC'};
      $scope.product.operationOptions=[];
      $scope.product.frameMaterials=[];
      $scope.product.fabricationClasses=[];
      $scope.product.operationTypes={};
      $scope.product.designDetails={};
      $scope.product.performance={};
      $scope.product.hardwareTypes=[];
      $scope.product.fireRatedCertifications=[];
      $scope.product.mulled={
                                    operableToOperable: {width: 0, height: 0},
                                    operableToFixed: {width: 0, height: 0},
                                    fixedToFixed: {width: 0, height: 0}
                            };
      $scope.product.framewidth={sides: 0, top: 0, bottom: 0};
   $scope.product.hardwareTypeValues = {name: 'HardwareTypes', isSelected: false, formType: 'select',formId:'hardwareType',
             icons : [
                {value: 'Clad Interior', label: '<i class="fa  fa-windows"></i> Clad Interior'},
                {value: 'Clad Exterior', label: '<i class="fa  fa-windows"></i> Clad Exterior'},
                {value: 'Exterior Included', label: '<i class="fa  fa-windows"></i> Exterior Included'}  ,
                {value: 'Interior Included', label: '<i class="fa  fa-windows"></i> Interior Included'}  ,
                {value: 'Historic DH Profile', label: '<i class="fa  fa-windows"></i> Historic DH Profile'}  ,
                {value: 'Exterior Install Flange', label: '<i class="fa  fa-windows"></i> Exterior Install Flange'}  ,
                {value: 'Unit comes 100% Factory Installed', label: '<i class="fa  fa-windows"></i> Unit comes 100% Factory Installed'}  ,
                {value: 'Interior Shade', label: '<i class="fa  fa-windows"></i> Interior Shade'}  ,
                {value: 'Exterior Shade', label: '<i class="fa  fa-windows"></i> Exterior Shade'}  ,
                {value: 'Vertical Glass to Glass Dimension at Mullion', label: '<i class="fa  fa-windows"></i> Vertical Glass to Glass Dimension at Mullion'}  
              ],
             selectedIcon:''
            }; 
      $scope.product.operationTypes.window = {name: 'WindowOperationTypes', isSelected: false, formType: 'select',formId:'windowOperationType',
             icons : [
                {value: 'Casement', label: '<i class="fa  fa-windows"></i> Casement'},
                {value: 'Tilt-Turn', label: '<i class="fa  fa-windows"></i> Tilt-Turn'}  ,
                {value: 'Double Hung', label: '<i class="fa  fa-windows"></i> Double Hung'}  ,
                {value: 'Lift Slide', label: '<i class="fa  fa-windows"></i> Lift Slide'}  ,
                {value: 'Sliding', label: '<i class="fa  fa-windows"></i> Sliding'}    ,
                {value: 'Awning', label: '<i class="fa  fa-windows"></i> Awning'}   ,
                {value: 'Bay-Bow', label: '<i class="fa  fa-windows"></i> Bay-Bow'}   ,
                {value: 'Corner', label: '<i class="fa  fa-windows"></i> Corner'}     ,
                {value: 'Historic', label: '<i class="fa  fa-windows"></i> Historic'}   ,
                {value: 'Simulated DH', label: '<i class="fa  fa-windows"></i> Simulated DH'} ,
                {value: 'Hopper', label: '<i class="fa  fa-windows"></i> Hopper'}   ,
                {value: 'Replacement', label: '<i class="fa  fa-windows"></i> Replacement'}  ,
                {value: 'Shaped', label: '<i class="fa  fa-windows"></i> Shaped'}   ,
                {value: 'Center Pivot', label: '<i class="fa  fa-windows"></i> Center Pivot'}  
              ],
             selectedIcon:''
            }; 
          $scope.product.operationTypes.door = {name: 'DoorOperationTypes', isSelected: false, formType: 'select',formId:'doorOperationType',
             icons : [
                {value: 'Solid', label: '<i class="fa  fa-windows"></i> Solid'},
                {value: 'Steel', label: '<i class="fa  fa-windows"></i> Steel'},
                {value: 'Entry', label: '<i class="fa  fa-windows"></i> Entry'},
                {value: 'Utility', label: '<i class="fa  fa-windows"></i> Utility'}  
              ],
             selectedIcon:''
            }; 
            $scope.product.operationTypes.skylight = {name: 'SkylightOperationTypes', isSelected: false, formType: 'select',formId:'skylightOperationType',
                       icons : [
                          {value: 'Fixed', label: '<i class="fa  fa-windows"></i> Fixed'},
                          {value: 'Operable', label: '<i class="fa  fa-windows"></i> Operable'},
                          {value: 'Integrated Curb', label: '<i class="fa  fa-windows"></i> Integrated Curb'},
                          {value: 'Roof Hatch', label: '<i class="fa  fa-windows"></i> Roof Hatch'}  ,
                          {value: 'Motor Operated', label: '<i class="fa  fa-windows"></i> Motor Operated'}  
                        ],
                       selectedIcon:''
                      }; 
           $scope.product.performanceFireData = {name: 'fireData', isSelected: false, formType: 'select',formId:'fireData',
                                 icons : [
                                    {value: 'Fire Rated', label: '<i class="fa  fa-fire"></i> Fire Rated'},
                                    {value: 'Fire Rated Components Offerred', label: '<i class="fa  fa-fire"></i> Fire Rated Components Offerred'}  
                                  ],
                                 selectedIcon:''
                                };   
            $scope.product.frameMaterialValues = {name: 'frameMaterials', isSelected: false, formType: 'select',formId:'frameMaterials',
                                 icons : [
                                    {value: 'Wood', label: '<i class="fa  fa-windows"></i> Wood'},
                                    {value: 'Vinyl', label: '<i class="fa  fa-windows"></i> Vinyl'}  
                                  ],
                                 selectedIcon:''
                                }; 
            $scope.product.fabricationClassValues = {name: 'fabricationClasses', isSelected: false, formType: 'select',formId:'fabricationClasses',
                                 icons : [
                                    {value: 'Finger joint', label: '<i class="fa  fa-windows"></i> Finger joint'},
                                    {value: 'Dowel joint', label: '<i class="fa  fa-windows"></i> Dowel joint'} ,
                                    {value: 'Steel reinforced', label: '<i class="fa  fa-windows"></i> Steel reinforced'} 
                                  ],
                                 selectedIcon:''
                                }; 
           $scope.product.currentStatus = {name: 'current', isSelected: false, formType: 'select',formId:'current',
                                 icons : [
                                    {value: 'Current', label: '<i class="fa  fa-thumbs-up" style="color: green;"></i> Current'},
                                    {value: 'Discontinued', label: '<i class="fa  fa-thumbs-down" style="color:red;"></i> Discontinued'} 
                                  ],
                                 selectedIcon:''
                                }; 
    $scope.product.requiresNewInfo = {name: 'requiresNewInfo', isSelected: false, formType: 'select',formId:'requiresNewInfo',
                                 icons : [
                                    {value: 'Yes', label: '<i class="fa  fa-thumbs-down" style="color:red;"></i> Yes'},
                                    {value: 'No', label: '<i class="fa  fa-thumbs-up" style="color:green;"></i> No'} 
                                  ],
                                 selectedIcon:''
                                }; 
//       $scope.company.servicePartners=[{name:'',address:'',website:''}];
          $scope.product.documents=[{document:''}];//{warranty:'',msic2doc:'',misc2doc:'',installDoc:''};
//       $scope.company.revenueYears=[{year: '2014', revenue: 0}];
//       $scope.company.warranty={period:'1Y'};
         $scope.product.glazingcompanies = {name: 'GlazingCompanies', isSelected: false, formType: 'select',formId:'GlazingCompany',
                     icons : [
                        {value: 'Glazing Company 1', label: '<i class="fa   fa-building-o"></i> Glazing Company 1'},
                        {value: 'Glazing Company 2', label: '<i class="fa  fa-building-o"></i> Glazing Company 2'}  ,
                        {value: 'Glazing Company 3', label: '<i class="fa  fa-building-o"></i> Glazing Company 3'}  ,
                        {value: 'Glazing Company 4', label: '<i class="fa  fa-building-o"></i> Glazing Company 4'}  
                      ],
                     selectedIcon:''
                    };
                    $scope.product.acousticRatingTypes = {name: 'AcousticRatingTypes', isSelected: false, formType: 'select',formId:'acousticRatingType',
                               icons : [
                                  {value: 'STC', label: '<i class="fa   fa-windows"></i> STC'},
                                  {value: 'OITC', label: '<i class="fa   fa-windows"></i> OITC'}  
                                ],
                               selectedIcon:''
                              };
                   $scope.product.uValueTypes = {name: 'uValueTypes', isSelected: false, formType: 'select',formId:'uValueType',
                               icons : [
                                  {value: 'SI', label: '<i class="fa   fa-windows"></i> SI'},
                                  {value: 'IP', label: '<i class="fa   fa-windows"></i> IP'}   
                                ],
                               selectedIcon:''
                              };   
          $scope.product.units = {name: 'Units', isSelected: false, formType: 'select',formId:'Unit',
             icons : [
                {value: 'mm', label: '<i class="fa   fa-windows"></i> mm'},
                {value: 'inches', label: '<i class="fa   fa-windows"></i> inches'}  
              ],
             selectedIcon:''
            };
  
           $scope.product.companies = {name: 'Companies', isSelected: false, formType: 'select',formId:'Company',
             icons : [
                {value: 'Zola', label: '<i class="fa   fa-building-o"></i> Zola'},
                {value: ' Company 2', label: '<i class="fa  fa-building-o"></i> Company 2'}  ,
                {value: ' Company 3', label: '<i class="fa  fa-building-o"></i> Company 3'}  ,
                {value: ' Company 4', label: '<i class="fa  fa-building-o"></i> Company 4'}  
              ],
             selectedIcon:''
            };
       
            
      $scope.reset();
  
}).controller('LeftCtrl',function($scope,$http,$templateCache,$mdSidenav){
  
  // Menu items
 	$scope.menu = [
    {
      link : '',
      listitem: 'Window',
      title: 'Window',
      icon: 'action:ic_dashboard_24px' // we have to use Google's naming convention for the IDs of the SVGs in the spritesheet
    },
    {
      link : '',
      listitem:'Door',
      title: 'Door',
      icon: 'social:ic_group_24px'
    },
    {
      link : '',
      listitem: 'skylight',
      title: 'Sky Light',
      icon: 'communication:ic_message_24px'
    }
    
  ];
  $scope.admin = [
    {
      link : '',
     listitem:'Company',
      title: 'Company',
      icon: 'action:ic_delete_24px'
    },
    {
      link : 'showListBottomSheet($event)',
      listitem:'GlazingCompany',
      title: 'Glazing Company',
      icon: 'action:ic_settings_24px'
    }
  ]; 
  
  
  
  
 $scope.onSwipeLeft = function(ev) {
      $('#nav-icon1').toggleClass('open');
       $scope.toggleLeft();
    };
 
    $scope.onSwipeRight = function(ev) {
      alert('You swiped right!!');
    };
    $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');
 $scope.showGridBottomSheet = function($event) {
    $scope.alert = '';
    $mdBottomSheet.show({
     templateUrl: 'bottom-sheet-grid-template.html',
      controller: 'GridBottomSheetCtrl',
      targetEvent: $event
    }).then(function(clickedItem) {
      $mdToast.show(
            $mdToast.simple()
              .content(clickedItem.name + ' clicked!')
              .position('top right')
              .hideDelay(1500)
          );
    });
  };
    /**
     * Build handler to open/close a SideNav; when animation finishes
     * report completion in console
     */
    function buildToggler(navID) { 
      var debounceFn =  $mdUtil.debounce(function(){
        
            $mdSidenav(navID)
              .toggle()
              .then(function () {
                $log.debug("toggle " + navID + " is done");
              
              });
          },200);

      return debounceFn;
    }

}).controller('GlazingCompanyCreateController', function($scope, $templateCache, $http) {
     $scope.master = {};
     
      $scope.update = function(glazingcompany) {
        $scope.master = angular.copy(glazingcompany);
      };

      $scope.reset = function() {
        $scope.glazingcompany = angular.copy($scope.glazingcompany);
      };
      
      $scope.getRequestedAddress = function(viewValue) {
        var params = {address: viewValue, sensor: false};
        return $http.get('https://maps.googleapis.com/maps/api/geocode/json', {params: params})
        .then(function(res) {
          return res.data.results;
        });
      };
      
//       $scope.removeRevenueYear=function(index){
//           $scope.company.revenueYears.splice(index,1);
//           if($scope.company.revenueYears.length === 0){
//             $scope.company.reveneueYears=[];
//           }
//       };
//       $scope.addRevenueYear=function(){
//           $scope.company.revenueYears.push({year:'',revenue: 0});
//       };
      $scope.glazingcompany={name:'',website:'',address:'',type: '1',spacerType:'', gasType:'',filmSurface:'',glassType:'' , filmUsed:'', uValue:'',uValueType:'SI',acousticRating:'',acousticRatingType:'STC',maxXYPrice: 0.0, maxXYPossible: 0.0, maxXYUnit  : 'mm',company:'',shgc:'',visualPercentage:'',comments:''};
      $scope.glazingcompany.testreports=[];
     $scope.removeTestReport = function(index){
          // remove the row specified in index
          $scope.glazingcompany.testreports.splice( index, 1);
          // if no rows left in the array create a blank array
          if ($scope.glazingcompany.testreports.length === 0){
            $scope.glazingcompany.testreports = [];
          }
        };  
      $scope.addTestReport = function(){
          // remove the row specified in index
          $scope.glazingcompany.testreports.push({document:''}); 
      };
//       $scope.company.servicePartners=[{name:'',address:'',website:''}];
      $scope.glazingcompany.documents=[{document:''}];//{warranty:'',msic2doc:'',misc2doc:'',installDoc:''};
//       $scope.company.revenueYears=[{year: '2014', revenue: 0}];
//       $scope.company.warranty={period:'1Y'};
      $scope.glazingcompany.types={name: 'Types', isSelected: false, formType: 'select',formId:'type',
             icons : [
                {value: '1', label: '<i class="fa  fa-venus-double"></i> 1'},
                {value: '2', label: '<i class="fa  fa-venus-double"></i> 2'}  
              ],
             selectedIcon:''
            }; 
          $scope.glazingcompany.maxXYUnits = {name: 'MaxXYUnits', isSelected: false, formType: 'select',formId:'MaxXYUnit',
             icons : [
                {value: 'mm', label: '<i class="fa   fa-windows"></i> mm'},
                {value: 'inches', label: '<i class="fa   fa-windows"></i> inches'}  
              ],
             selectedIcon:''
            };
           $scope.glazingcompany.companies = {name: 'Companies', isSelected: false, formType: 'select',formId:'Company',
             icons : [
                {value: 'Zola', label: '<i class="fa   fa-building-o"></i> Zola'},
                {value: ' Company 2', label: '<i class="fa  fa-building-o"></i> Company 2'}  ,
                {value: ' Company 3', label: '<i class="fa  fa-building-o"></i> Company 3'}  ,
                {value: ' Company 4', label: '<i class="fa  fa-building-o"></i> Company 4'}  
              ],
             selectedIcon:''
            };
        $scope.glazingcompany.acousticRatingTypes = {name: 'AcousticRatingTypes', isSelected: false, formType: 'select',formId:'acousticRatingType',
             icons : [
                {value: 'STC', label: '<i class="fa   fa-windows"></i> STC'},
                {value: 'OITC', label: '<i class="fa   fa-windows"></i> OITC'}  
              ],
             selectedIcon:''
            };
      $scope.glazingcompany.spacerTypes = {name: 'SpacerTypes', isSelected: false, formType: 'select',formId:'spacerType',
             icons : [
                {value: 'phA (4.6-6)', label: '<i class="fa   fa-windows"></i> phA (4.6-6)'},
                {value: 'phB (3.1-4.5)', label: '<i class="fa   fa-windows"></i> phB (3.1-4.5)'} ,
                {value: 'phC (1.5-3.0)', label: '<i class="fa   fa-windows"></i> phC (1.5-3.0)'} , 
                {value: 'General (<1.5)', label: '<i class="fa   fa-windows"></i> General (<1.5)'}  
              ],
             selectedIcon:''
            };
     $scope.glazingcompany.gasTypes = {name: 'gasTypes', isSelected: false, formType: 'select',formId:'gasType',
             icons : [
                {value: 'Arg/Air (90/10)', label: '<i class="fa   fa-windows"></i> Arg/Air (90/10)'},
                {value: 'Air (100)', label: '<i class="fa   fa-windows"></i> Air (100)'} ,
                {value: 'Kry/Air (90/10)', label: '<i class="fa   fa-windows"></i> Kry/Air (90/10)'} , 
                {value: 'Xen/Air (90/10)', label: '<i class="fa   fa-windows"></i> Xen/Air (90/10)'}  
              ],
             selectedIcon:''
            }; 
       $scope.glazingcompany.glassTypes = {name: 'glassTypes ', isSelected: false, formType: 'select',formId:'glassType',
             icons : [
                {value: 'Decorative', label: '<i class="fa   fa-windows"></i> Decorative'},
                {value: 'Stained', label: '<i class="fa   fa-windows"></i> Stained'} ,
                {value: 'High Performance', label: '<i class="fa   fa-windows"></i> High Performance'} , 
                {value: 'Heat Resistant', label: '<i class="fa   fa-windows"></i> Heat Resistant'}  
              ],
             selectedIcon:''
            }; 
     $scope.glazingcompany.filmsUsed = {name: 'filmUsed', isSelected: false, formType: 'select',formId:'filmsUsed',
             icons : [
                {value: 'LowE', label: '<i class="fa   fa-windows"></i> LowE'},
                {value: 'None', label: '<i class="fa   fa-windows"></i> None'}  
              ],
             selectedIcon:''
            }; 
         $scope.glazingcompany.filmSurfaces = {name: 'filmSurfaces', isSelected: false, formType: 'select',formId:'filmSurface',
             icons : [
                {value: '2/5', label: '<i class="fa   fa-windows"></i> 2/5'},
                {value: '2/3', label: '<i class="fa   fa-windows"></i> 2/3'} ,
                {value: '2/7', label: '<i class="fa   fa-windows"></i> 2/7'}  ,
                {value: '1', label: '<i class="fa   fa-windows"></i> 1'}  ,
                {value: '2', label: '<i class="fa   fa-windows"></i> 2'}   ,
                {value: '3', label: '<i class="fa   fa-windows"></i> 3'}   ,
                {value: '4', label: '<i class="fa   fa-windows"></i> 4'}   ,
                {value: '5', label: '<i class="fa   fa-windows"></i> 5'}   ,
                {value: '6', label: '<i class="fa   fa-windows"></i> 6'}   ,
                {value: '7', label: '<i class="fa   fa-windows"></i> 7'}  ,
                {value: '8', label: '<i class="fa   fa-windows"></i> 8'}   
              ],
             selectedIcon:''
            }; 
           $scope.glazingcompany.uValueTypes = {name: 'uValueTypes', isSelected: false, formType: 'select',formId:'uValueType',
             icons : [
                {value: 'SI', label: '<i class="fa   fa-windows"></i> SI'},
                {value: 'IP', label: '<i class="fa   fa-windows"></i> IP'}   
              ],
             selectedIcon:''
            }; 
          $scope.glazingcompanyyearSelect={name: 'Years', isSelected: false, formType: 'select',formId:'yearSelect',
           icons : [
              {value: '2005', label: '<i class="fa fa-calendar"></i> 2005'},
              {value: '2006', label: '<i class="fa fa-calendar"></i> 2006'},
              {value: '2007', label: '<i class="fa fa-calendar"></i> 2007'},
              {value: '2008', label: '<i class="fa fa-calendar"></i> 2008'},
              {value: '2009', label: '<i class="fa fa-calendar"></i> 2009'},
              {value: '2010', label: '<i class="fa fa-calendar"></i> 2010'},
              {value: '2011', label: '<i class="fa fa-calendar"></i> 2011'},
              {value: '2012', label: '<i class="fa fa-calendar"></i> 2012'},
              {value: '2013', label: '<i class="fa fa-calendar"></i> 2013'},
              {value: '2014', label: '<i class="fa fa-calendar"></i> 2014'}  
            ],
           selectedIcon:''
          };
      $scope.reset();
  
}).directive('ServicePartnerItem', function() {
  return {
    restrict: 'E', 
    scope: {
      formName:'=',
      formId:'=',
      formType:'='
    },
     template: '  <div class="input-group custom-search-form">  <input type="{{formType}}" class="form-control" name="{{formId}}" id="{{formId}}" placeholder="{{formName}}" >  <span class="input-group-btn"> <button class="btn btn-default" type="button"  > <i class="fa fa-times"></i> </button>  </span> </div>'
  };  
});

