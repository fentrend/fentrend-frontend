// call the packages we need
var express    = require('express'); 		// call express
var ntwitter = require('ntwitter');
var bodyParser = require('body-parser');
var Q =          require('q');
var busboy = require('connect-busboy'); //middleware for form/file upload
var path = require('path');     //used for file path
var fs = require('fs-extra');       //File System - for file manipulation
var nodeStatic = require('node-static');       //File System - for file manipulation 
 
var cookieParser = require('cookie-parser');
var Parse = require('parse').Parse;
var _ = require("underscore");
var redis = require('redis'); 
var redisClient = redis.createClient(16660,"pub-redis-16660.us-east-1-3.3.ec2.garantiadata.com", {no_ready_check: true});
redisClient.auth("ScBKi4tf4efG7al0");
// var mongo = require('mongodb');
// var monk = require('monk');
var BitlyAPI = require("node-bitlyapi");
// var db = monk('mongodb://tpapi:tpapi@linus.mongohq.com:10002/app29194626');

// var client = mubsub('mongodb://tpapi:tpapi@linus.mongohq.com:10002/app29194626');
var AWS = require('aws-sdk');
var HOSTNAME_="fentrend.com";
var HOSTNAME_ADMIN_="admin.fentrend.com";
var Bitly = new BitlyAPI({
    client_id: "cfd8438be790032ee223e152f0e465f9b8ed18cc",
    client_secret: "2ec872a92c5da6357a306f4c517632ba892a39e3"    
});
Bitly.setAccessToken("d1f524e0a371bbcf11b1f637fba8d83a0c661f58");
 
var app        = express(); 				// define our app using express
var FILTER_COUNT=1;
var webshot = require('webshot'); 
var fs      = require('fs');
var Client = require('node-rest-client').Client;

client = new Client();
Q.longStackJumpLimit = 0;

var upload = require('./upload'); 


// Set your region for future requests.
AWS.config.region = 'us-east-1';
AWS.config.loadFromPath('./config.json');
// Create a bucket using bound parameters and put something in it.
// Make sure to change the bucket name from "myBucket" to something unique.
var s3bucket = new AWS.S3();
 




  
var webshot_options = {
  screenSize: {
    width: 320
  , height: 480
  }
, shotSize: {
    width: 320
  , height: 'all'
  }
, userAgent: 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 3_2 like Mac OS X; en-us)'
    + ' AppleWebKit/531.21.20 (KHTML, like Gecko) Mobile/7B298g'
}

var application_name="fentrend"; 
//application context to use in this application
var application_context='/fentrend';


//port to bind to for this application
var port = process.env.PORT || 8080; 		// set our port

var CURRENT_HASHES_=[];
// Parse.initialize("UyiG0nOZ8HNxRhbiDbp8npkK50VCamFjLF6lVKs9", "bAWTl89YIryuOLk3p2lyyyF0lMiikarWhLMtBkFk");
Parse.initialize("Fhq4NuqJRQ2TxXroPjpFi8ylt1Tm30GWIxJNy4vP", "hReeeK0l0QpCFk1Fg5MdqRYXM9LsWogMDwxrGolb");

 //CORS middleware
 

 
  
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public', { cache: 10 }));
app.use(busboy());
app.use('/static', express.static(__dirname + '/public'));
app.enable("jsonp callback"); 
 
  
app.use(bodyParser.json());





 
 
// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); 				// get an instance of the express Router


exports.preview_upload_website=function(url_){
   var deferred=Q.defer();
   console.log("exports.preview_upload_website url-->"+url_);
   var ts_ = Math.floor(new Date() / 1000);
   var fn_="RPPIC_"+ts_+".jpeg";
  console.log("exports.preview_upload_website url--> Writing to "+fn_);
      webshot(url_, webshot_options, function(err, renderStream) {
              var file = fs.createWriteStream(fn_, {encoding: 'binary'});

                renderStream.on('data', function(data) {
                  console.log("Sending to s3 aws.");
                  exports.upload_aws(data, fn_);
                   deferred.resolve('https://s3.amazonaws.com/rollipoll/'+fn_ );
                
        }); 
      });
    return deferred.promise;
};
exports.clear_validdomains=function(questionList){
 var deferred=Q.defer();
  var clears = [];
  questionList.forEach(function(q){
       clears.push(  exports.delete_redis('rp_vd.'+q) ); 
  });
  Q.all(clears).then(function(vals){
      deferred.resolve(vals);
  });
  return deferred.promise;
}
exports.delete_redis=function(key){
   var deferred=Q.defer();
   var keyStatus={'id': key, status: 'deleted'};
  if(typeof key !== 'undefined'){
            redisClient.exists( key , function(err, reply) {
                if (reply === 1) {
                    redisClient.del( key , function(err,object){
                       deferred.resolve(keyStatus);

                    });
                } else {
                    keyStatus.status='Nothing found for '+key;
                     deferred.resolve(keyStatus);
                }
            }); 
  }
  return deferred.promise;
}
exports.upload_aws=function(data_,file_name_){
       var deferred=Q.defer();
  var re = /(?:\.([^.]+))?$/;
  var ext = re.exec(file_name_)[1];  
  var content_type_ = "image/png";
  if(ext==="jpg" || ext==="jpeg"){
    content_type_="image/jpeg";
  }
  else if(ext==="gif"){
    content_type_ = "image/gif";
  } 
  file_name_=file_name_.split(' ').join('_');
  console.log("Uploading "+file_name_);
  s3bucket.putObject({
        Bucket: 'rollipoll',
        Key: file_name_,
        ACL:'public-read',
        ContentType:content_type_,
        Body: data_
    },function (resp) {
          console.log('Successfully uploaded package.'+JSON.stringify(resp));
          deferred.resolve(resp);
        }); 
     return deferred.promise;
}
 
exports.get_all_polls=function(shareable){
          var deferred=Q.defer();
          var paged_polls = [];
          for(var i =0 ; i < 10; i++){
              paged_polls.push(exports.get_all_polls_paginated(i,shareable));
          }
          Q.all(paged_polls).then(function(polls){
                deferred.resolve(polls);
          }); 
          return deferred.promise;
};
exports.get_all_polls_paginated=function(page,shareable) {
      var deferred=Q.defer();
      var RolliPoll  =  Parse.Object.extend("RolliPoll"); 
      var query = new Parse.Query(RolliPoll);   
      query.equalTo("is_deleted","N");
      query.limit(1000);
      query.include("user");
      if(typeof shareable !== 'undefined' && shareable!==null){
        query.greaterThan("vote_count",0);
      }
      query.descending('updatedAt');
      query.skip(page * 1000);
        query.find({
             success: function(items){  
                 return deferred.resolve(items);
             },
              error: function(err){
                
                  console.log(JSON.stringify(err)); 
                  deferred.reject(err);
              }
        });
       return deferred.promise;
};
exports.count_impressions=function(poll)
{
  var total_impressions={};
     var deferred=Q.defer();
//   console.log("Retrieving rollipoll impression for poll ");
//   console.log(JSON.stringify(poll));
    var RolliPollImpression =  Parse.Object.extend("RolliPollImpression"); 
    var query = new Parse.Query(RolliPollImpression);   
      query.equalTo("poll",poll);
      query.limit(999);
      query.find({
             success: function(items){  
//                console.log("Items found "+JSON.stringify(items));
               total_impressions.count=(items)?items.length:0;
               total_impressions.poll=poll;
                     deferred.resolve(total_impressions); 
              },
              error: function(err){
                
                  console.log(JSON.stringify(err)); 
                  deferred.reject(err);
              }
      }); 
  return deferred.promise; 
}
exports.preview=function(url){
  webshot(url, function(err, renderStream) {
  var file = fs.createWriteStream('./public/img/ws.png', {encoding: 'binary'});

  renderStream.on('data', function(data) {
    file.write(data.toString('binary'), 'binary');
  });
});
}
exports.save_bitly_external=function(url){
//  console.log("Save bitly External "+url);
       var deferred=Q.defer();
    Bitly.shortenLink(url, function(err, results) {
//     console.log(JSON.stringify(err));
//     console.log(JSON.stringify(results));
        deferred.resolve(results);
    });
    return deferred.promise;
};
exports.save_bitly_external_obj=function(url){ 
       var deferred=Q.defer();
  if(typeof url==='undefined' || url===null){
        console.log("Bitly External Obj = Undefined/Null ");
        deferred.resolve({});
  }
  else{
       redisClient.exists(url,function(err,reply){
                    if(reply===1) {
                      redisClient.get(url, function(err,reply){
                        if(typeof reply === undefined || reply === null || reply.length === 0){
                           Bitly.shortenLink(url, function(err, results) { 
                                redisClient.set(url,results);
                                deferred.resolve(JSON.parse(results));
                            });
                        }
                        else{
//                             console.log("Bitly redis hit "+url+" - "+reply);
                           deferred.resolve(JSON.parse(reply));
                        }
                      });
                    }
                     else{
                        Bitly.shortenLink(url, function(err, results) { 
                            redisClient.set(url,results);
                            deferred.resolve(JSON.parse(results));
                        });
                     }
       }); 
  }
    
    return deferred.promise;
};
exports.save_bitly_parse=function(bitly_response,poll){
 
    var deferred=Q.defer();  
    var RolliPollBitlyImpression =Parse.Object.extend("RolliPollBitlyImpression");  
    var query = new Parse.Query(RolliPollBitlyImpression);
    var bitly_response_obj=JSON.parse(bitly_response);
//     console.log('global_hash==>'+bitly_response_obj['data']['global_hash']);
//    console.log('hash==>'+bitly_response_obj['data']['hash']);
//   console.log(JSON.stringify(poll));

    query.equalTo("hash", bitly_response_obj['data']['hash']);
    query.equalTo("global_hash", bitly_response_obj['data']['global_hash']);
 
   query.equalTo("poll",poll); 

  console.log("exports.save_bitly_parse::saving "+JSON.stringify(bitly_response)+" to parse.");
    query.find({
      success: function(items){
        console.log("exports.save_bitly_parse:: find with items. ")
        console.log(JSON.stringify(items));
        if(items && items.length > 0 ){
          console.log("Items found "+JSON.stringify(items));
          deferred.resolve(items[0]); 
        }
        else{
          console.log("exports.save_bitly_parse::No Items found .");
          var bitlyImpression = new RolliPollBitlyImpression();
          var $bitly= JSON.parse(bitly_response);
          console.log(JSON.stringify(bitly_response));
          bitlyImpression.set("long_url",  $bitly['data']['long_url']);
          bitlyImpression.set("url",  $bitly['data']['url']);
          bitlyImpression.set("hash",  $bitly['data']['hash']);
          bitlyImpression.set("global_hash",  $bitly['data']['global_hash']);
          bitlyImpression.set("new_hash",  $bitly['data']['new_hash']);
          bitlyImpression.set("poll", poll);
          console.log("exports.save_bitly_parse::saing bitly imrpession");
          bitlyImpression.save(null, 
                               {
                                 success: function(imp){
                                   console.log("Saving bitly impression.");
                                    deferred.resolve(imp);
                                },
                                 error: function(err){
                                   console.log(err.message);
                                   console.log(JSON.stringify(err));
                                   deferred.reject(err);
                                 }
                               });
        }
      },
      error: function(error){
        console.log("Error "+error.message);
        deferred.reject(error);
      }
    });
  
    return deferred.promise;
}
exports.count_votes_page=function(answer,page)
{
  var limit = 1000;
  var anser_metrics={};
       var deferred=Q.defer();
    var RolliPollVote =   Parse.Object.extend("RolliPollVote"); 
    var query = new Parse.Query(RolliPollVote);   
      query.equalTo("answer",answer);
      query.equalTo("IS_ACTIVE","Y");
      query.descending('updatedAt');
      query.limit(limit); 
      query.skip(page*limit);
      query.find({
             success: function(items){  
               anser_metrics.vote_count=items?items.length:0;
               anser_metrics.answer=answer;
                     deferred.resolve(anser_metrics); 
              },
              error: function(err){
                  console.log(JSON.stringify(err)); 
                  deferred.reject(err);
              }
      }); 
  return deferred.promise; 
}


exports.count_votes=function(answer)
{
  var anser_metrics={};
       var deferred=Q.defer();
    var RolliPollVote =   Parse.Object.extend("RolliPollVote"); 
    var query = new Parse.Query(RolliPollVote);   
      query.equalTo("answer",answer);
      query.equalTo("IS_ACTIVE","Y");
      query.descending('updatedAt');
      query.limit(1000);
      query.find({
             success: function(items){  
//                console.log("COUNT_VOTES :: Item length is "+items.length+" for "+JSON.stringify(answer));
               anser_metrics.vote_count=(typeof items!=="undefined")?items.length:0;
               anser_metrics.answer=answer;
                     deferred.resolve(anser_metrics); 
              },
              error: function(err){
                  console.log(JSON.stringify(err)); 
                  deferred.reject(err);
              }
      }); 
  return deferred.promise; 
}
exports.fifty_fifty=function(report){
   var deferred=Q.defer(); 
  if(report.votes < FILTER_COUNT){
    var anser_percent = parseFloat((1 / report.answer_metrics.length)*100).toFixed(2);
    report.answer_metrics.forEach(function(am){
        am.percent=anser_percent;
    });
  }
  deferred.resolve(report);
  return deferred.promise;
}
exports.percent_filter=function(report){
     var deferred=Q.defer(); 
  exports.fifty_fifty(report).then(function(r){
          if(r.votes >= FILTER_COUNT )
         {
           var total_votes = r.votes;

           r.answer_metrics.forEach(function(am){
               am.percent=parseFloat((am.vote_count / r.votes)*100).toFixed(2);
           });
         }
          deferred.resolve(r);
  });
   deferred.resolve(report);
   return deferred.promise;
} ;
exports.get_bitly=function(pollid,fullUrl){
    var deferred=Q.defer(); 
  var bitlyKey = 'bitly.'+pollid+'.'+fullUrl;
  redisClient.get(bitlyKey, function(err,reply){
        if(typeof reply === undefined || reply === null || reply.length === 0){
            exports.get_poll_by_id(pollid).then(function(poll){  
                  var RolliPollBitlyImpression =Parse.Object.extend("RolliPollBitlyImpression");  
                  var query = new Parse.Query(RolliPollBitlyImpression);
                  query.equalTo("poll" , poll) ;
                  query.equalTo("long_url",fullUrl);
                  query.first({
                    success: function(bitly_impression){
                        redisClient.set(bitlyKey,JSON.stringify(bitly_impression));
                        deferred.resolve(bitly_impression);
                    },
                    error: function(bitly_impression, err_){
                        deferred.reject(err_);
                    }
                  });
          });
        }
        else{
               var parseObject =Parse.Object.extend("RolliPollBitlyImpression"); 
               var instanceObject = new parseObject(JSON.parse(reply)); 
               deferred.resolve(instanceObject); 
        }
  }); 
  return deferred.promise;
}
exports.get_bitly_impressions=function(poll){
    var deferred=Q.defer(); 
   
          var RolliPollBitlyImpression =Parse.Object.extend("RolliPollBitlyImpression");  
          var query = new Parse.Query(RolliPollBitlyImpression); 
          query.equalTo("poll",poll);
          query.find({
            success: function(bitly_impression){ 
                deferred.resolve(bitly_impression);
            },
            error: function(  err_){
 
              console.log(err_.message);
                deferred.reject(err_);
            }
          }); 
  return deferred.promise;
}
exports.get_bitly_by_id=function(pollid,bitlyId){
    var deferred=Q.defer(); 
  exports.get_poll_by_id(pollid).then(function(poll){
          console.log("Poll fund for poll id "+pollid);
          var RolliPollBitlyImpression =Parse.Object.extend("RolliPollBitlyImpression");  
          var query = new Parse.Query(RolliPollBitlyImpression);
//           query.equalTo("poll" , poll) ;
          query.equalTo("objectId",bitlyId);
          query.first({
            success: function(bitly_impression){
//                 console.log("Found bitly impression "+JSON.stringify(bitly_impression));
                deferred.resolve(bitly_impression);
            },
            error: function(bitly_impression, err_){
              console.log(err_.message);
                deferred.reject(err_);
            }
          });
  });
  return deferred.promise;
}
exports.bitly_clicks=function(rollup_,bitly_url){
    var deferred=Q.defer();
  
   client.get("https://api-ssl.bitly.com/v3/link/clicks?access_token=d1f524e0a371bbcf11b1f637fba8d83a0c661f58&rollup="+rollup_+"&link="+bitly_url, function(data, response){
                     
                     deferred.resolve(data);
                });
  return deferred.promise;
}
exports.bitly_links=function(action_,bitly_url){
    var deferred=Q.defer();
  
   client.get("https://api-ssl.bitly.com/v3/link/"+action_+"?access_token=d1f524e0a371bbcf11b1f637fba8d83a0c661f58&link="+bitly_url, function(data, response){ 
                     deferred.resolve(data);
                });
  return deferred.promise;
};
exports.fb_shares=function(url) {
  var deferred=Q.defer();
  
   client.get("https://graph.facebook.com/v2.2/?id="+url+"&access_token=421599347990310%7C5a7bc4c2fa3141538afb7f8bc89ecdfb", function(data, response){ 
                     deferred.resolve(data);
                });
  return deferred.promise;
}; 
exports.report_bitly_facebook=function(bitly_links){
  
};
exports.report_bitly_twitter=function(bitly_links){
  
};
exports.report_facebook_share=function(bitly_links){
  
};
exports.get_list_bitly_links=function(full_urls){ 
  var bitly_calls =[];
  var deferred=Q.defer();
  full_urls.forEach(function(newUrl){
      bitly_calls.push(exports.save_bitly_external_obj(newUrl));
  });
  var bitlyUrls=[];
  Q.all(bitly_calls).then(function(urls){ 
      urls.forEach(function(u){ 
          var url = u['data']['url'];
          if(typeof url !=='undefined' && url!==null){
             bitlyUrls.push(url);
          } 
      });  
      deferred.resolve(bitlyUrls);
  });
  return deferred.promise;
};
exports.bitly_fb_share_count=function(bitlyUrls){
      var deferred=Q.defer();
          var bitlyUrlCount=[];
          bitlyUrls.forEach(function(burl){
            bitlyUrlCount.push(exports.fb_shares(burl));
          }); 
          Q.all(bitlyUrlCount).then(function(results){
            var shareCount=0;
            results.forEach(function(clickData){
              var clickDataObject = JSON.parse(clickData); 
             var cd = clickDataObject['share']['share_count']; 
               if(typeof cd !== "undefined" && cd !== null){
                     shareCount = shareCount + parseInt(cd);
               }
            });
            deferred.resolve(shareCount);
          });
      return deferred.promise;
};
exports.bitly_tw_share_count=function(bitlyUrls){
      var deferred=Q.defer();
          var bitlyUrlCount=[];
          bitlyUrls.forEach(function(burl){
            bitlyUrlCount.push(exports.bitly_links("shares", burl ));
          }); 
          Q.all(bitlyUrlCount).then(function(results){
            var shareCount=0;
            results.forEach(function(clickData){
              var clickDataObject = JSON.parse(clickData); 
             var cd = clickDataObject['data']['total_shares']; 
               if(typeof cd !== "undefined" && cd !== null){
                     shareCount = shareCount + parseInt(cd);
               }
            });
            deferred.resolve(shareCount);
          });
      return deferred.promise;
};

          
exports.bitly_click_count=function(bitlyUrls){
      var deferred=Q.defer();
          var bitlyUrlCount=[];
          bitlyUrls.forEach(function(burl){
            bitlyUrlCount.push(exports.bitly_clicks(true,burl));
          }); 
          Q.all(bitlyUrlCount).then(function(results){
            var clickCount=0;
            results.forEach(function(clickData){
              var clickDataObject = JSON.parse(clickData);
              var cd = clickDataObject['data']['link_clicks']; 
               if(typeof cd !== "undefined" && cd !== null){
                     clickCount = clickCount + parseInt(cd);
               }
            });
            deferred.resolve(clickCount);
          });
      return deferred.promise;
};
exports.get_full_urls=function(pollid){
    var bitly_fb_urls=[];
    var bitly_twitter_urls =[];
    var fbUrl="http://"+HOSTNAME_+"/rollipoll/staticfb/";
    var twUrl="http://"+HOSTNAME_+"/rollipoll/staticftw/";
    var twitterCalls=[];
    var facebookCalls=[];
    var deferred=Q.defer();
    var socialClicks = {};
    exports.get_poll_by_id(pollid).then(function(poll){
      exports.get_poll_answers(poll).then(function(answers){
          exports.get_bitly_impressions(poll).then(function(bitly_impressions){
                answers.forEach(function(answer){
                      bitly_impressions.forEach(function(impression){
                              var newFbUrl = fbUrl+poll.id+'/'+impression.id+'/'+answer.id;
                              var newTwUrl = twUrl+poll.id+'/'+impression.id+'/'+answer.id;
                              twitterCalls.push(newTwUrl);
                              facebookCalls.push(newFbUrl); 
                      });  
                });
                socialClicks['twitterUrls']=twitterCalls;
                socialClicks['facebookUrls']=facebookCalls; 
                deferred.resolve(socialClicks);
             
             
                
//                 Q.all(twitterCalls).then(function(twitterData){ 
//                   if( typeof twitterData === "undefined" || twitterData===null ){
//                      cosnole.log("No twitter data resolved");
//                   }
//                   else {   
//                       Q.all(facebookCalls).then(function(facebookData){
//                               socialClicks['twitter_urls']=[]; 
//                               socialClicks['facebook_urls']=[]; 
//                             twitterData.forEach(function(bitt){
//                                       socialClicks['twitter_urls'].push(bitt['data']['url']);
//                             });
//                             facebookData.forEach(function(bitf){
//                                       socialClicks['facebook_urls'].push(bitf['data']['url']);
//                             });
//                             var twitterCount = [];
//                             var facebookCount = [];
//                             var facebookShareCount=[];
//                             socialClicks['twitter_urls'].forEach(function(burl){
//                                   twitterCount.push(exports.bitly_clicks(true,burl) );
//                             });
//                             socialClicks['facebook_urls'].forEach(function(burl){
//                                       facebookCount.push(exports.bitly_clicks(true,burl) );
//                                       facebookShareCount.push(exports.fb_shares(burl));
//                             }); 
//                             Q.all(twitterCount).then(function(twitterCountData){  
//                                   Q.all(facebookCount).then(function(facebookCountData){   
//                                   if(typeof twitterCountData === "undefined" || twitterCountData===null){
//                                     console.log("Twitter Count Data is null.");
//                                   }
//                                   else { 
//                                     var twitterClicks = 0;
//                                     var facebookClicks = 0; 
//                                     var facebookShares = 0; 
//                                     twitterCountData.forEach(function(clickData){ 
//                                       var clickDataObject = JSON.parse(clickData);
//                                           var cd = clickDataObject['data']['link_clicks']; 
//                                           if(typeof cd !== "undefined" && cd !== null){
//                                             twitterClicks = twitterClicks + parseInt(cd);
//                                           }
//                                     });
//                                    facebookCountData.forEach(function(clickData){
//                                           var clickDataObject = JSON.parse(clickData);
//                                           var cd = clickDataObject['data']['link_clicks']; 
//                                           if(typeof cd !== "undefined" && cd !== null){
//                                             facebookClicks = facebookClicks + parseInt(cd);
//                                           }
//                                     });  
//                                     socialClicks['twitter_count']=twitterClicks;
//                                     socialClicks['facebook_count']=facebookClicks;
//                                     socialClicks['facebook_share_count']=0; 
//                                     deferred.resolve(socialClicks);
//                                   }
//                                });
//                                });     
//                             });
                    
//                   } 
//                 });
  
          });
      });
    });
    return deferred.promise;  
};
 
exports.report_bitly_socialmedia_twc=function(pollid)
{
     var deferred=Q.defer();
      exports.get_full_urls(pollid).then(function(fullUrls){
        console.log(JSON.stringify(fullUrls));
        var bitlyUrlResolve=[];
        bitlyUrlResolve.push();
        bitlyUrlResolve.push(); 
                exports.get_list_bitly_links(fullUrls['twitterUrls']).then(function(bitlyTwitterUrls){
//                   exports.get_list_bitly_links(fullUrls['facebookUrls']).then(function(bitlyFacebookUrls){
                             fullUrls['twitterBitlyUrls']=bitlyTwitterUrls;
//                              fullUrls['facebookBitlyUrls']=bitlyFacebookUrls;
//                           console.log(JSON.stringify(bitlyTwitterUrls));
//                           console.log(JSON.stringify(bitlyFacebookUrls));
                            exports.bitly_click_count(bitlyTwitterUrls).then(function(countTwitterClicks){
//                                exports.bitly_click_count(bitlyFacebookUrls).then(function(countFacebookClicks){
//                                  exports.bitly_fb_share_count(bitlyFacebookUrls).then(function(facebookShareCount){
//                                        exports.bitly_tw_share_count(bitlyTwitterUrls).then(function(twitterShareCount){
//                                               fullUrls['facebookClicks']=countFacebookClicks;
                                              fullUrls['twitterClicks']=countTwitterClicks;
//                                               fullUrls['facebookShares']=facebookShareCount;
//                                               fullUrls['twitterShares']=twitterShareCount;
                                              deferred.resolve(fullUrls);
//                                        });
                                
//                                  });
                                   
//                                 });
//                             }); 
                  }); 
          });
      });
    return deferred.promise;
}; 
exports.report_bitly_socialmedia_fbc=function(pollid)
{
     var deferred=Q.defer();
      exports.get_full_urls(pollid).then(function(fullUrls){
//         console.log(JSON.stringify(fullUrls));
        var bitlyUrlResolve=[];
        bitlyUrlResolve.push();
        bitlyUrlResolve.push(); 
//                 exports.get_list_bitly_links(fullUrls['twitterUrls']).then(function(bitlyTwitterUrls){
                  exports.get_list_bitly_links(fullUrls['facebookUrls']).then(function(bitlyFacebookUrls){
//                              fullUrls['twitterBitlyUrls']=bitlyTwitterUrls;
                             fullUrls['facebookBitlyUrls']=bitlyFacebookUrls;
//                           console.log(JSON.stringify(bitlyTwitterUrls));
//                           console.log(JSON.stringify(bitlyFacebookUrls));
//                             exports.bitly_click_count(bitlyTwitterUrls).then(function(countTwitterClicks){
                               exports.bitly_click_count(bitlyFacebookUrls).then(function(countFacebookClicks){
//                                  exports.bitly_fb_share_count(bitlyFacebookUrls).then(function(facebookShareCount){
//                                        exports.bitly_tw_share_count(bitlyTwitterUrls).then(function(twitterShareCount){
                                              fullUrls['facebookClicks']=countFacebookClicks;
//                                               fullUrls['twitterClicks']=countTwitterClicks;
//                                               fullUrls['facebookShares']=facebookShareCount;
//                                               fullUrls['twitterShares']=twitterShareCount;
                                              deferred.resolve(fullUrls);
//                                        });
                                
//                                  });
                                   
//                                 });
//                             }); 
                  }); 
          });
      });
    return deferred.promise;
}; 
exports.report_bitly=function(pollid)
{
   var deferred=Q.defer();
  exports.get_poll_by_id(pollid).then(function(poll){
                var report_={};
      var RolliPollBitlyImpression =   Parse.Object.extend("RolliPollBitlyImpression"); 
      var query = new Parse.Query(RolliPollBitlyImpression);  
      query.equalTo("poll" ,poll);
      query.find({
        success: function(items){
          var queue_=[];
          var items_=[];

                report_['shares']={};
                report_['visits']={};
          if(items && items.length > 0){
             items.forEach(function(item){
           
                var bitly_url=encodeURIComponent( item.get("url") );
               queue_.push(exports.bitly_links("shares", bitly_url )); 
               queue_.push(exports.bitly_links("referrers_by_domain", bitly_url ));
//                queue_.push(exports.bitly_clicks(false,bitly_url));
               queue_.push(exports.bitly_clicks(true,bitly_url)); 
                Q.all(queue_).then(function(shares,referrers,referrers_by_domain){
//                       console.log("referrers_by_domain" + shares[1]); 
                      var sharesObject = JSON.parse(shares[0]);
                  if(sharesObject && sharesObject['data'] && sharesObject['data']['shares']){
                        sharesObject['data']['shares'].forEach(function(it_){
                                console.log(JSON.stringify(it_));
                                var c_ =0;
                                  if(  report_['shares'][it_['share_type']] ){
                                        c_=report_['shares'][it_['share_type']];
                                  }
                                    c_=c_+it_['shares']  ;
                                  report_['shares'][it_['share_type']]=c_;
                  
                      });
                  }
                  
                      if( report_['shares']['total']){
                        report_['shares']['total']=report_['shares']['total']+sharesObject['data']['total_shares']; 
                      }
                      else{
                         report_['shares']['total']=sharesObject['data']['total_shares']; 
                      }
                         var c__=0;
                      sharesObject = JSON.parse(shares[1]);
                    if(sharesObject && sharesObject['data'] && sharesObject['data']['referrers'] && sharesObject['data']['referrers']['twitter.com']){
                                    
                      sharesObject['data']['referrers']['twitter.com'].forEach(function(cl_){ 
                         c__=c__+cl_['clicks']; 
                      }); 
                      if(report_['visits']['tw']){
                        report_['visits']['tw']=report_['visits']['tw']+c__;
                      }
                      else{
                        report_['visits']['tw']=c__;
                      }
 
                    }
                    if(sharesObject && sharesObject['data'] && sharesObject['data']['referrers'] && sharesObject['data']['referrers']['www.facebook.com']){
                                    c__=0;
                      sharesObject['data']['referrers']['www.facebook.com'].forEach(function(cl_){ 
                         c__=c__+cl_['clicks']; 
                      }); 
                      if(report_['visits']['fb']){
                        report_['visits']['fb']=report_['visits']['fb']+c__;
                      }
                      else{
                        report_['visits']['fb']=c__;
                      }
                    }
                     if(sharesObject && sharesObject['data'] && sharesObject['data']['referrers'] && sharesObject['data']['referrers']['m.facebook.com']){
                             c__=0;
                            sharesObject['data']['referrers']['m.facebook.com'].forEach(function(cl_){ 
                               c__=c__+cl_['clicks']; 
                            }); 
                            if(report_['visits']['fb']){
                              report_['visits']['fb']=report_['visits']['fb']+c__;
                            }
                            else{
                              report_['visits']['fb']=c__;
                            }
                     }
                      if(sharesObject && sharesObject['data'] && sharesObject['data']['referrers'] && sharesObject['data']['referrers']['direct']){
                                    c__=0;
                                  sharesObject['data']['referrers']['direct'].forEach(function(cl_){ 
                                     c__=c__+cl_['clicks']; 
                                  }); 
                                  if(report_['visits']['direct']){
                                    report_['visits']['direct']=report_['visits']['direct']+c__;
                                  }
                                  else{
                                    report_['visits']['direct']=c__;
                                  } 
                      }
            
                   
                sharesObject = JSON.parse(shares[2]);
                if(sharesObject['data']['link_clicks']){
                                   if(report_['visits']['total']){
                    report_['visits']['total']=report_['visits']['total']+sharesObject['data']['link_clicks'];
                  }
                  else{
                    report_['visits']['total']=sharesObject['data']['link_clicks'];
                  }
                }
 
                      
                   deferred.resolve(report_);
                });
                    
              }); 
          }
          else{
            //{"shares":{"tw":2,"fb":2,"total":4},"visits":{"tw":4,"fb":6,"direct":2,"total":12}}}
            report_['shares']['tw']=0;
            report_['shares']['fb']=0;
            report_['shares']['direct']=0; 
            report_['shares']['total']=0;
            report_['visits']['tw']=0;
            report_['visits']['fb']=0;
            report_['visits']['direct']=0; 
            report_['visits']['total']=0;
            deferred.resolve(report_);
          }
        },
        error: function(err){
               deferred.reject(err); 
        }
      });
      
  });
  return deferred.promise;
}
exports.report=function(pollid,bitly_)
{
  var report_={};
    var deferred=Q.defer(); 
  console.log("Report...");
exports.get_poll_by_id(pollid).then(function(updatedPoll){
  console.log("Fetching poll "+updatedPoll.id);
  updatedPoll.fetch({   
 
                                                  success: function(poll){
                                                      console.log("Fetched poll "+poll.id);
                                                      console.log(JSON.stringify(poll));
                                                      exports.get_poll_answers(poll).then(function(answers){
                                                    //           console.log("Have answers returned for poll "+pollid);
                                                              if(answers){
                                                    //             console.log("Answers length is "+answers.length);
                                                              }    
                                                              report_.poll=poll;
                                                              report_.answers=answers;
                                                              var _answer_metrics = [];
                                                    //                   console.log("Populating answer metrics");
                                                                     exports.count_impressions(poll).then(function(imp_count){

                                                                           report_.impressions=imp_count["count"];
                                                                            exports.count_total_votes(poll).then(function(vote_count){ 
                                                                                report_.votes=vote_count.count;  
                                                                                 answers.forEach(function(a_){

                                                                                            _answer_metrics.push(exports.count_votes(a_));
                                                                                  });
                                                                                      console.log("Getting answer metrics");
                                                                                      Q.all(_answer_metrics).then(function(count_objs ){
                                                                                        
                                                                                            if(count_objs && count_objs.length>0){
                                                                                              var a_met=[];
                                                                                              count_objs.forEach(function(ao){
                                                                                                  var answer_metric = {};
                                                                                                   answer_metric.objectId=ao.answer.get("objectId");
                                                                                                   answer_metric.text=ao.answer.get("text");
                                                                                                   answer_metric.order=ao.answer.get("order");
                                                                                                   answer_metric.vote_count=ao.vote_count;

                                                                                                  a_met.push(answer_metric);

                                                                                              });
                                                                                                report_.answer_metrics = a_met;
                                                                                            }
                                                                                            console.log("Answer metrics retrieved ");
//                                                                                             report_['bitly']['shares']['tw']=poll.get("twitterShares");
//                                                                                             report_['bitly']['shares']['fb']=poll.get("facebookShares");
                                                                                           report_['bitly']={};
                                                                                             report_['bitly']['visits']={};
                                                                                             report_['bitly']['shares']={};
                                                                                            report_['bitly']['shares']['fb']=poll.get("facebookShares");
                                                                                            report_['bitly']['shares']['tw']=poll.get("twitterShares");
                                                                                            report_['bitly']['visits']['fb']=poll.get("facebookClicks");
                                                                                            report_['bitly']['visits']['tw']=poll.get("twitterClicks");
                                                                                            report_['runtime']=poll.get("runtime");
                                                                                         console.log("Report returned.");
                                                                                         console.log(poll.get("twitterShares")+","+poll.get("facebookShares")+","+poll.get("facebookClicks")+","+poll.get("twitterClicks"));
                                                                                         deferred.resolve(report_);
 

                                                                                      });

                                                                            });
                                                                    });  
                                                      });                                                    
                                                  },
                                                  error: function(poll,error){
                                                    if(typeof poll !== 'undefined' && poll!==null){
                                                      console.log("Error fb clicks 1  "+JSON.stringify(poll));
                                                    }
                                                    if(typeof error !== 'undefined' && error!==null){
                                                      console.log("Error fb clicks 2  "+JSON.stringify(error));
                                                    } 
                                                  }
 }); 

}); 
  return deferred.promise;
};
exports.count_total_votes=function(poll)
{
  var total_votes={};
   var deferred=Q.defer();
    var RolliPollVote =   Parse.Object.extend("RolliPollVote"); 
    var query = new Parse.Query(RolliPollVote);   
      query.equalTo("poll",poll);
      query.equalTo("IS_ACTIVE","Y"); 
      query.find({
             success: function(items){  
               total_votes.poll=poll;
               total_votes.count=(items)?items.length:0;
                     deferred.resolve(total_votes); 
              },
              error: function(err){
                  console.log(JSON.stringify(err)); 
                  deferred.reject(err);
              }
      }); 
  return deferred.promise; 
}
exports.save_items=function(array_of_items, s_str_arr, e_str_arr)
{
    var deferred=Q.defer();
      var friend=[];
      var c_=0;
  console.log("save_items::saving "+array_of_items.length);
      array_of_items.forEach(function(item){
            friend.push(exports.save_parse_item(item,s_str_arr[c_],e_str_arr[c_]));
        c_++;
      });
      Q.all(friend).then(function(items){
          
        deferred.resolve("save_items::Successfully saved all items.");
 
      });
  return deferred.promise;
}
exports.save_vote=function(vote_to_save)
{
    var deferred = Q.defer();
    if(vote_to_save){
        vote_to_save.save(null,{
          success: function(v){
              deferred.resolve(v);
          },
          error: function(err){
            deferred.error(err);
          }
        });
  return deferred.promise;
    }  
};
exports.archive_votes=function(votes_to_archive)
{
          var deferred = Q.defer();
              var save_array_ = [];
              votes_to_archive.forEach(function(v){
                    save_array_.push(exports.save_vote(v));
              });
              Q.all(save_array_).then(function(results){
                  deferred.resolve(results);
              });
          return deferred.promise;
};
exports.save_parse_item=function(item_to_save,success_str,err_str)
{
  var deferred = Q.defer();
   item_to_save.save(null, {
                                               success:function(a){
                                                               
                                                           console.log(success_str);
                                                           deferred.resolve(a);
                                               },
                                                error: function(a,err){
                                                          console.log(err_str+' '+err.message);
                                                          deferred.reject(err);
                                                }
        });
    return deferred.promise; 
}
exports.get_poll_by_id=function(poll_object_id, lazy_load)
{
    var deferred = Q.defer();//promise to return all data.
  redisClient.get(poll_object_id , function(err,reply){
        if(typeof reply === undefined || reply === null || reply.length === 0){
                    var RolliPoll = Parse.Object.extend("RolliPoll");
                    var query = new Parse.Query(RolliPoll);        
                    query.include("user"); 
                    query.include("settings"); 
                    query.equalTo("objectId",poll_object_id);
                    query.first({
                      success: function(item){  
                        if(typeof item !==undefined && item !== undefined && item !== null){
                          console.log("Settign redis for "+poll_object_id);
                          redisClient.set(poll_object_id,JSON.stringify(item));
                          deferred.resolve(item);
                        }
                         else{ 
                           var errorMessage = JSON.parse( '{"status":"Error" , "message": "Not found for '+poll_object_id+'" }' );
                           deferred.reject(errorMessage);
                         }      
                      },
                      error: function(err){
                          console.log(JSON.stringify(err)); 
                          deferred.reject(err);
                      }
                    });                        
        }
        else { 
            var parseObject = Parse.Object.extend("RolliPoll"); 
            var instanceObject = new parseObject(JSON.parse(reply)); 
            deferred.resolve(instanceObject); 
        }
  }); 
      return deferred.promise;
}
exports.get_poll_answers=function(poll_object)
{
    var deferred = Q.defer();//promise to return all data.
            var RolliPollAnswer = Parse.Object.extend("RolliPollAnswer");
            var query = new Parse.Query(RolliPollAnswer);       
       
            query.equalTo("question",poll_object);
            query.find({
              success: function(items){  
                     deferred.resolve(items); 
              },
              error: function(err){
                  console.log(JSON.stringify(err)); 
                  deferred.reject(err);
              }
            });                                      
      return deferred.promise;
}

exports.get_poll_answer=function(answer_id)
{
    var deferred = Q.defer();//promise to return all data.
            var RolliPollAnswer = Parse.Object.extend("RolliPollAnswer");
            var query = new Parse.Query(RolliPollAnswer);       
       
            query.equalTo("objectId",answer_id);
            query.first({
              success: function(item){  
                     deferred.resolve(item); 
              },
              error: function(err){
                  console.log(JSON.stringify(err)); 
                  deferred.reject(err);
              }
            });                                      
      return deferred.promise;
}
exports.get_poll_impression=function(impression_id)
{
    var deferred = Q.defer();//promise to return all data.
            var RolliPollImpression = Parse.Object.extend("RolliPollImpression");
            var query = new Parse.Query(RolliPollImpression);       
       
            query.equalTo( "objectId" , impression_id );
            query.first({
              success: function(item){  
                     deferred.resolve(item); 
              },
              error: function(err){
                  console.log(JSON.stringify(err)); 
                  deferred.reject(err);
              }
            });                                      
      return deferred.promise;
}
exports.save_poll=function(poll_data,user)
{
  var deferred = Q.defer();//promise to return all data.
  var passedInPollId = poll_data['objectId'] || poll_data['old_pollid']
  redisClient.del( passedInPollId , function(err,object){
    
                console.log("Redis remove completed ."+passedInPollId); 
       if(poll_data['old_pollid']){
      exports.get_poll_by_id(poll_data['old_pollid'],false).then(function( old_poll ){
            exports.save_poll_formatted_data(poll_data,user,old_poll).then(function(poll){ 
              console.log("save_poll_formatted_data returned "+JSON.stringify(poll));
              var pollid = poll.id;
//               if(typeof pollid === "undefined" || pollid===null){
//                 poll=poll['objectId'];
//               }
//                if(typeof pollid === "undefined" || pollid===null){
//                 poll=poll.id;
//               }
                    console.log("Settign redis for "+pollid);
                  redisClient.exists(pollid,function(err,reply){
                    if(reply===1) {
                       console.log("1 - Poll "+pollid+" exists - removing from redis first.");
                       redisClient.del(pollid, function(err1, object){
                         if( typeof poll.get("poll") === "undefined" || poll.get("poll") === null ) {
                             console.log("Saving poll external."+JSON.stringify(poll));
                             redisClient.set(pollid,JSON.stringify(poll));
                             
                             deferred.resolve(poll); 
                         }
                         else {
                             console.log("Saving poll internal."+JSON.stringify(poll.get("poll")));
                              var parseObject = Parse.Object.extend("RolliPoll"); 
                              var instanceObject = new parseObject(poll.get("poll"));   
                             
                             redisClient.set(pollid,JSON.stringify(poll.poll));
                             deferred.resolve(instanceObject); 
                         }
                       });
                    }
                    else {
                       redisClient.set(pollid,JSON.stringify(poll));
                       deferred.resolve(poll); 
                    }
                        
                  });
                  
                  
           });  
     });
   }  
   else{
      exports.save_poll_formatted_data(poll_data,user).then(function(poll){
        console.log("1 - Saving poll formatted data...");
        console.log(JSON.stringify(poll));
          var pollid = typeof poll.poll!=="undefined"? poll.poll.get('objectId'):poll.get('objectId');
        console.log("Setting redis for "+pollid);
                redisClient.exists(pollid,function(err,reply){
                    if(reply===1) {
                       console.log("2 - Poll "+pollid+" exists - removing from redis first.");
                       redisClient.del(pollid, function(err1, object){
                         redisClient.set(pollid,JSON.stringify(poll));
                         deferred.resolve(poll); 
                       });
                    }
                    else {
                       redisClient.set(pollid,JSON.stringify(poll));
                       deferred.resolve(poll); 
                    }
                        
                  });
               
       });
   }
      });
   return deferred.promise;
}

exports.get_vote=function(poll,impression)
{
   var deferred = Q.defer();//promise to return all data.
   var RolliPollVote = Parse.Object.extend("RolliPollVote"); 
     var query = new Parse.Query(RolliPollVote);
              query.equalTo("impression", impression);
              query.equalTo("poll",poll);
              query.equalTo("IS_ACTIVE","Y");
                query.find({
                  success: function(items){  
                    console.log("Found votes 1: "+JSON.stringify(items)); 
                    deferred.resolve(items);
                  },
                  error: function(err){
                      console.log("Error ,"+JSON.stringify(err));
                     deferred.reject(err); 
                  }
                });
                                  
   return deferred.promise;
}      
exports.save_poll_formatted_data=function(poll,u,old_poll)
{
   var deferred = Q.defer();//promise to return all data.
       var RolliPoll = Parse.Object.extend("RolliPoll");
       var RolliPollAnswer = Parse.Object.extend("RolliPollAnswer");
       var RolliPollSettings = Parse.Object.extend("RolliPollSettings");
       var RolliPollImpression = Parse.Object.extend("RolliPollImpression");
       var RolliPollVote = Parse.Object.extend("RolliPollVote");
        // Create a new instance of that class.
      
  var rolliPoll = new RolliPoll();
  var oldPollSettingsObject;
  if(old_poll){
    console.log("Old Poll settings are being set.");
      oldPollSettingsObject=old_poll.get("settings");
//       rolliPoll.set("question" , old_poll.get("question"));
//       rolliPoll.set("user" , u );
//       rolliPoll.set("answers" , old_poll.get("answers"));
//       rolliPoll.set("poll_settings" , old_poll.get('poll_settings'));
//       rolliPoll.set("is_deleted", "V");  
      old_poll.set("question", poll['question']);
      old_poll.set("user" , u );
      old_poll.set("answers" , poll['answers']);
      old_poll.set("poll_settings" , poll['poll_settings']);
      old_poll.set("is_deleted", "N");
      console.log("Setting old poll poll_settings");
      var old_poll_nested = old_poll.get('poll');
      if(old_poll_nested !== null && typeof old_poll_nested !== "undefined") {
          old_poll_nested["question"]=poll['question'] ;
          old_poll_nested["user"]=u;
          old_poll_nested["answers"]=poll['answers'];
          old_poll_nested["poll_settings"]=poll['poll_settings'];
          old_poll_nested["is_deleted"]="N";
          old_poll.set('poll',old_poll_nested);
          console.log("Old Poll has been set to Nested "+JSON.stringify(old_poll));
      }
      console.log("Poll is set "+JSON.stringify(old_poll.get('poll')));
    
      console.log("Poll settings are "+JSON.stringify(poll['poll_settings']));
      
      
  }
  else { 
     console.log("New Poll settings are being set.");
      rolliPoll.set("question" , poll['question']);
      rolliPoll.set("user" , u );
      rolliPoll.set("answers" , poll['answers']);
      rolliPoll.set("poll_settings" , poll['poll_settings']);
      rolliPoll.set("is_deleted", "N");
  
  }
                var returned_callback = function(pollObject, cb_callback){
//                       console.log("Returned callback.");
                      var message_r={status: 'success',poll: pollObject};
//                       console.log("Returning "+JSON.stringify(message_r)); 
                      cb_callback.apply(this,[message_r]);
                };
  
               var callback_savesettings_versioned = function(pollObj,message_r){
                  var settings = new  RolliPollSettings(); 
                          console.log("Saving settings - object is null or undefined");
                          settings.set("question_font",poll['poll_settings']["question_font"]);
                          settings.set("linkcolor", poll['poll_settings']["linkcolor"]);
                          settings.set("questioncolor",poll['poll_settings']["questioncolor"]);
                          settings.set("bgcolor",poll['poll_settings']["bgcolor"]);
                          settings.set("answer_font",poll['poll_settings']["answer_font"]); 
                          settings.set("social_image",poll['poll_settings']["social_image"]); 
                          settings.set("social_image_name",poll['poll_settings']["social_image_name"]);  
                          settings.set("question", pollObj);
                          pollObj.set("settings", settings);
                          settings.save(null, {
                              success: function(sett) {
//                                 console.log("successfully saved settings .");
                                returned_callback(pollObj,function(responseMsg){
//                                       console.log("Returned response message "+JSON.stringify(responseMsg));
                                      deferred.resolve(responseMsg);
                                });
                              },
                              error: function(sett,err) {
                                 console.log("Error saving settings "+err.message); 
                                 message_r['status']='error';
                                 message_r['message']='Error saving ' + err.message;
                                 deferred.reject(message_r);
                              }
                          });
                };
                var callback_savesettings = function(pollObj,message_r){
                  var settings = new  RolliPollSettings(); 
                  if(pollObj.get("settings")!==null && typeof pollObj.get("settings") !== "undefined"){
                    settings=pollObj.get("settings");
                  }
                          
                          settings.set("question_font",poll['poll_settings']["question_font"]);
                          settings.set("linkcolor", poll['poll_settings']["linkcolor"]);
                          settings.set("questioncolor",poll['poll_settings']["questioncolor"]);
                          settings.set("bgcolor",poll['poll_settings']["bgcolor"]);
                          settings.set("answer_font",poll['poll_settings']["answer_font"]); 
                          settings.set("social_image",poll['poll_settings']["social_image"]); 
                          settings.set("social_image_name",poll['poll_settings']["social_image_name"]);  
                          settings.set("question", pollObj);
                          settings.set("user",u);
                          pollObj.set("settings", settings); 
                          settings.save(null, {
                              success: function(sett) {
                                console.log("successfully saved settings .");
                                exports.get_poll_answers(pollObj).then(function(answers_){
                                  if(answers_ && answers_.length > 0 ){
                                    console.log("Answers "+answers_.length);
                                    var aMap = {};
                                    
                                    pollObj.get("answers").forEach(function(ans){
                                         aMap[parseInt(ans['order'])]=ans; 
                                    });
                                    var items_=[];
                                    var suc_=[];
                                    var err_=[]; 
                                    answers_.forEach(function(ansr){
                                       
                                      var answerData = aMap[parseInt(ansr.get('order'))];
                                        console.log("Saving old answer with new data");
                                      
                                      if(answerData!==null && typeof answerData !=='undefined'){
                                        console.log("Answer " +ansr.get('order')+" found.-->"+ answerData['text']+","+ansr.get('text')); 
                                        ansr.set('text', answerData['text']);
                                        ansr.set("question" , pollObj);
                                        ansr.set("user", u); 
                                        items_.push(ansr);
                                        suc_.push("Answer  sucessfully saved. "+ansr.get("text"));
                                        err_.push("Error on saving answer.  "+ansr.get("text")); 
                                      }
                                      else{
                                        ansr.destroy(null, {
                                          success: function(an){
                                            console.log(JSON.stringify(an)+" destoryed.");
                                          },
                                          error: function(an,err){
                                            console.log("Error destroying answer."+err.message);
                                          }
                                        });
                                      }
                                      delete aMap[parseInt(ansr.get('order'))];
                                    }); 
                                       for(var lastIndex=0; lastIndex < 5; lastIndex++){ 
                                             var answerData = aMap[lastIndex];
                                                 if(answerData!==null && typeof answerData !== 'undefined'){
                                                       console.log("New answer to add for last index "+lastIndex);
                                                       var answer_ = new RolliPollAnswer();
                                                                                  answer_.set("order" , lastIndex);
                                                                                  answer_.set("text" ,  answerData['text']);
                                                                                  answer_.set("question" , pollObj);
                                                                                  answer_.set("user", u); 
                                                                                  items_.push(answer_);
                                                                                  suc_.push("Answer  sucessfully saved. "+answer_.get("text"));
                                                                                  err_.push("Error on saving answer.  "+answer_.get("text")); 
                                                          console.log("Saving old answer with new data");
                                                 }
                                             }  
                                     
                                    exports.save_items(items_, suc_,err_).then(function(){
//                                                 console.log("2 -- Items saved successfully.");
                                                returned_callback(pollObj , function(responseMsg){
//                                                       console.log("Returned callback response 2.  "+JSON.stringify(responseMsg['poll']));
                                                       var parseObject = Parse.Object.extend("RolliPoll"); 
            var instanceObject = new parseObject(responseMsg['poll']);
                                                  instanceObject.set("objectId" , pollObj.id);
//                                                       console.log("Returing object "+JSON.stringify(instanceObject)+"  "+instanceObject.id);
                                                  
                                                      deferred.resolve(instanceObject);
                                                });
                                     });
                                    
                                  }
                                  else {
                                    console.log("No Answers found.");
                                    var items_=[];
                                    var suc_=[];
                                    var err_=[];
                                          pollObj.get("answers").forEach(function(ans){
                                            console.log("Answer found");
                                                                      var answer_ = new RolliPollAnswer();
                                                                      answer_.set("order" , ans['order']);
                                                                      answer_.set("text" ,  ans['text']);
                                                                      answer_.set("question" , pollObj);
                                                                      answer_.set("user", u); 
                                                                      items_.push(answer_);
                                                                      suc_.push("Answer  sucessfully saved. "+answer_.get("text"));
                                                                      err_.push("Error on saving answer.  "+answer_.get("text")); 
                                           });
                                          exports.save_items(items_, suc_,err_).then(function(){
                                                console.log("1-Items saved successfully.");
                                                returned_callback(pollObj, function(responseMsg){
                                                  console.log("Response message "+JSON.stringify(responseMsg));
                                                  deferred.resolve(responseMsg);
                                                }) ;
                                          });
                                  }

                                }); 
                              },
                              error: function(sett,err) {
                                 console.log("Error saving settings "+err.message); 
                                 message_r['status']='error';
                                 message_r['message']='Error saving ' + err.message;
                                 deferred.reject(message_r);
                              }
                          });
                };
  
                var callback_versionpollsettings = function(pollObj,message_r,settingsObject,originalPoll){
                  pollObj.set('is_deleted','V');
                  pollObj.set("settings",settingsObject);
                  settingsObject.set('question',pollObj);
                  settingsObject.save(null,{
                    success: function(set){
                            console.log("Successfully versioned settings now saving the original.");
                            message_r['poll']=originalPoll;
                            callback_savesettings(originalPoll,message_r);
                    },
                    error: function(set,err){
                                 console.log("Error saving settings "+err.message); 
                                 message_r['status']='error';
                                 message_r['message']='Error saving ' + err.message;
                                 deferred.reject(message_r); 
                    }
                  }); 
                };
                var callback_saveimpression = function(original_poll){
                                      /**
                                      *  Create linked impression.
                                      */
                                       var impression = new RolliPollImpression(); 
                                       impression.set("impression_value", 1);
                                       impression.set("poll",original_poll); 
                                       impression.save(null, {
                                                           success: function(imp) {
                                                               console.log("successfully saved impression.");
                                                               callback_savesettings(original_poll,{status: "success",poll: original_poll});
                                                           },
                                                           error: function(imp,err) {
                                                            console.log("Error saving impression "+err.message);
                                                            var message_r={};
                                                            message_r['status']='error';
                                                            message_r['message']='Error saving ' + err.message;
                                                            deferred.reject(message_r);
                                                           }
                                                       });
                                    };
  
  
  
      var message_r  ={status: "success"};
       if(old_poll){
                           console.log("Old polls is");
                          console.log(JSON.stringify(old_poll));
         
                          message_r['poll']=old_poll; 
                          old_poll.save(null,
                          {
                            success: function(oldpoll){
                                console.log("Saved orinal poll. About to save the settings.");
                                message_r['poll']=oldpoll;  
//                                 callback_versionpollsettings(savedPoll,message_r, oldPollSettingsObject,old_poll);
//                                 callback_saveimpression(old_poll);
                              callback_savesettings(old_poll,message_r); 
                              
                            },
                            error: function(oldpoll,err){
                                 console.log("Error saving poll "+err.message);
                                 message_r['status']='error';
                                 message_r['message']='Error saving ' + err.message;
                                 deferred.reject(message_r);
                            }
                          });
                      }
                      else{
                        console.log("rollipoll.save is running");
                         rolliPoll.save(null, {    
                                success: function(savedPoll){
                                        console.log("Saved rollipoll id is "+savedPoll.id);
                                          message_r['poll']=savedPoll; 
                                          callback_saveimpression(savedPoll);
                                      },
                                error: function(savedPoll, err){
                                      console.log("Error, saving initial poll "+err.message);
                                      message_r['status']='error';
                                      message_r['message']='Error saving ' + err.message;
                                      deferred.reject(message_r); 
                                }
                         });   
                      }
                                         
   return deferred.promise;
}
// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
	res.jsonp({ message: ' welcome to '+application_context+' root api!' });	
});

router.get('/embed/:pollid', function(req, res) {
 console.log("Retrieving content for poll "+req.params.pollid);
});
router.get('/vote/:pollid', function(req, res) {
 console.log("Retrieving content for poll "+req.params.pollid);
});

router.post('/poll/version',function(req,res){
    var u =req.body.user;
    var questionid  = req.body.objectId;
    var versionToQuestionId = req.body.newObjectId;
          var RolliPoll = Parse.Object.extend("RolliPoll");
            var query = new Parse.Query(RolliPoll); 
            query.equalTo("objectId",questionid);
            query.first({
              success: function(item){
//                qlist.forEach(function(item){
                console.log("Item ");
                console.log(item);
              
                   item.set("is_deleted", "Y"); 
                   console.log("Saving ...");
                   console.log(item);
                   item.save(null, {
                                               success:function(a){console.log("poll deleted saved");
      redisClient.del(questionid , function(err,object){
                console.log("Redis remove completed ."+questionid);
                res.status(200).send("Success "+questionid);
      });
                                                },
                                                error: function(a,err){console.log("poll not deleted ");res.status(500).send(JSON.stringify(err));}
                   });
//                });
              },
              error: function(err){
                  console.log(JSON.stringify(err));
                  res.send("Error retrieving list of polls "+ err.message);
              }
            });  
});
router.post('/poll/delete', function(req,res){
    var u =req.body.user;
    var questionid  = req.body.objectId;
          var RolliPoll = Parse.Object.extend("RolliPoll");
            var query = new Parse.Query(RolliPoll);
 
            query.equalTo("objectId",questionid);
            query.first({
              success: function(item){ 
                console.log("Item ");
                console.log(item);
                   item.set("is_deleted", "Y");
                   console.log("Saving ...");
                   console.log(item);
                   item.save(null, {
                                               success:function(a){console.log("poll deleted saved"); 
         redisClient.del(questionid, function(err,obj){
           console.log("Redis delete "+questionid+" complete.");
           res.status(200).send("Success "+questionid);
         });                                                          },
                                                error: function(a,err){console.log("poll not deleted ");res.status(500).send(JSON.stringify(err));}
                   });
//                });
              },
              error: function(err){
                  console.log(JSON.stringify(err));
                  res.send("Error retrieving list of polls "+ err.message);
              }
            });
});
router.post('/poll/deactivate', function(req,res){
    var u =req.body.user;
    var questionid  = req.body.objectId;
          var RolliPoll = Parse.Object.extend("RolliPoll");
            var query = new Parse.Query(RolliPoll);
 
            query.equalTo("objectId",questionid);
            query.first({
              success: function(item){
//                qlist.forEach(function(item){
                console.log("Item ");
                console.log(item);
                   item.set("is_deactivated", "Y");
                   console.log("Saving ...");
                   console.log(item);
                   item.save(null, {
                                               success:function(a){console.log("poll deactivate saved"); res.status(200).send("Success "+questionid);},
                                                error: function(a,err){console.log("poll not deleted ");res.status(500).send(JSON.stringify(err));}
                   });
//                });
              },
              error: function(err){
                  console.log(JSON.stringify(err));
                  res.send("Error retrieving list of polls "+ err.message);
              }
            });
});
router.post('/poll/activate', function(req,res){
    var u =req.body.user;
    var questionid  = req.body.objectId;
          var RolliPoll = Parse.Object.extend("RolliPoll");
            var query = new Parse.Query(RolliPoll);
 
            query.equalTo("objectId",questionid);
            query.first({
              success: function(item){
//                qlist.forEach(function(item){
                console.log("Item ");
                console.log(item);
                   item.set("is_deactivated", "N");
                   console.log("Saving ...");
                   console.log(item);
                   item.save(null, {
                                               success:function(a){console.log("poll deactivate saved"); res.status(200).send("Success "+questionid);},
                                                error: function(a,err){console.log("poll not deleted ");res.status(500).send(JSON.stringify(err));}
                   });
//                });
              },
              error: function(err){
                  console.log(JSON.stringify(err));
                  res.send("Error retrieving list of polls "+ err.message);
              }
            });
});
router.post('/clearsettings', function(req,res){
    var user  = req.body;
     var query = new Parse.Query(Parse.User);
  query.equalTo("objectId", user['objectId']);  // find all the women
  query.first({
    success: function(u) {
            var RolliPoll = Parse.Object.extend("RolliPoll");
            var query = new Parse.Query(RolliPoll);
            query.equalTo("user", u );
//             query.equalTo("is_deleted","N");
            query.addDescending("createdAt"); 
            query.include("settings");
            query.find({
              success: function(qlist){ 
                    var questionlist = [];
                    qlist.forEach(function(q){
                      questionlist.push(q.id);
                     
                    });
                    exports.clear_validdomains(questionlist).then(function(vals){
                         console.log(JSON.stringify(vals));
                         res.status(200).jsonp(vals);
                      });
              },
              error: function(err){
                res.status(500).jsonp({status: "Error" , error: err});
              }
            });
    },
    error: function(u,err){res.send("Unable to find user "+err.message);}
  });
});
router.post('/polls/find', function(req,res){
    var user  = req.body;
     var query = new Parse.Query(Parse.User);
  query.equalTo("objectId", user['objectId']);  // find all the women
  query.first({
    success: function(u) {
            var RolliPoll = Parse.Object.extend("RolliPoll");
            var query = new Parse.Query(RolliPoll);
            query.equalTo("user", u );
            query.equalTo("is_deleted","N");
            query.addDescending("createdAt"); 
            query.include("settings");
            query.find({
              success: function(qlist){     
                  res.status(200).jsonp(qlist);
              },
              error: function(err){
                res.status(500).jsonp({status: "Error" , error: err});
              }
            });
    },
    error: function(u,err){res.send("Unable to find user "+err.message);}
  });
});
router.get('/poll/find/:qid', function(req,res){
            var questionid  = req.params.qid;
  console.log("REST: /poll/find/:qid Retrieving poll for id "+questionid);
  exports.get_poll_by_id(questionid,"false").then(function(poll){
     console.log("REST: Poll found for id "+questionid);
     res.send(JSON.stringify(poll));
  }); 
}); 

router.get('/poll/vote/:act/:qid/:iid/:aid', function(req,res){
  var questionid_  = req.params.qid;
  var impressionid_  = req.params.iid;
  var answerid_  = req.params.aid;
  var action_  = req.params.act; 
  var return_msg = {status: "Success", action: action_};
  var RolliPollVote = Parse.Object.extend("RolliPollVote");

  exports.get_poll_by_id(questionid_).then(function(poll){
    console.log("Voting - poll is "+JSON.stringify(poll));
        exports.get_poll_impression( impressionid_ ).then(function(impression){
          exports.get_vote(poll,impression).then(function(votes){
                  var votes_=[];
                   console.log('/poll/vote/'+action_+'/'+questionid_+'/'+impressionid_+'/'+answerid_ );
                  if(action_==="vote" && votes && votes.length > 0)
                  {
                     console.log('1/poll/vote/'+action_+'/'+questionid_+'/'+impressionid_+'/'+answerid_ );
                    votes.forEach(function(v){
                       console.log('2/poll/vote/'+action_+'/'+questionid_+'/'+impressionid_+'/'+answerid_ );
                        if(v.get("IS_ACTIVE")=== "Y"){
                          v.set("IS_ACTIVE","N");
                          votes_.push(v);
                        }
                    });
                    if(votes_.length>0){
 
                      console.log("Archiving votes ");
                      exports.archive_votes(votes_).then(function(results){
                          
                        
                       
                        
                         console.log("Voting - action is to vote." );
                     exports.get_poll_answer(answerid_).then(function(answer){
                       return_msg["impression"]=impression;
                       return_msg["answer"]=answer;
                       return_msg["poll"]=poll;
                       if(poll===null)
                        {
                           return_msg["question_id"]=questionid_;
                          return_msg["status"]="Error";
                          return_msg["message"]="Error , poll not found {"+questionid_+"}.";
                           res.jsonp(return_msg);
                        }
                       else if(impression===null)
                        {
                           return_msg["poll"]=poll;
                          return_msg["impression_id"]=impressionid_;
                          return_msg["status"]="Error";
                          return_msg["message"]="Error , impression not found {"+impressionid_+"}.";
                           res.jsonp(return_msg);
                        }
                       else if(answer && answer !==null ){
                          var vote = new RolliPollVote();
                          vote.set("poll", poll);
                          vote.set("impression", impression);
                          vote.set("answer", answer);
                          vote.set("IS_ACTIVE","Y");
                        console.log("Voting - action is to vote. --> Saving the vote." );
                          exports.save_items([vote],["Successfully voted "],["Error voting"]).then(function(){
                                    return_msg["vote"]=vote;
                                 
                                    res.jsonp(  return_msg ) ;
                            });  
                       }
                       else{
                          return_msg["answer_id"]=answerid_;
                          return_msg["status"]="Error";
                          return_msg["message"]="Error , poll and impression found but no answer found{"+answerid_+"}.";
                           res.jsonp(return_msg);
                       }
                        
                        });
                      });
                    }
                   
                   
                  }
                  if(action_==="vote" && (  votes_.length==0) )
                  { 
                    console.log("Voting - action is to vote." );
                     exports.get_poll_answer(answerid_).then(function(answer){
                                    return_msg["impression"]=impression;
                                    return_msg["answer"]=answer;
                                    return_msg["poll"]=poll;
                       if(poll===null)
                        {
                           return_msg["question_id"]=questionid_;
                          return_msg["status"]="Error";
                          return_msg["message"]="Error , poll not found {"+questionid_+"}.";
                           res.jsonp(return_msg);
                        }
                       else if(impression===null)
                        {
                           return_msg["poll"]=poll;
                          return_msg["impression_id"]=impressionid_;
                          return_msg["status"]="Error";
                          return_msg["message"]="Error , impression not found {"+impressionid_+"}.";
                           res.jsonp(return_msg);
                        }
                       else if(answer && answer !==null ){
                          var vote = new RolliPollVote();
                          vote.set("poll", poll);
                          vote.set("impression", impression);
                          vote.set("answer", answer);
                          vote.set("IS_ACTIVE","Y");
                        console.log("Voting - action is to vote. --> Saving the vote." );
                          exports.save_items([vote],["Successfully voted "],["Error voting"]).then(function(){
                                    return_msg["vote"]=vote;
                                 
                                    res.jsonp(  return_msg ) ;
                            });  
                       }
                       else{
                          return_msg["answer_id"]=answerid_;
                          return_msg["status"]="Error";
                          return_msg["message"]="Error , poll and impression found but no answer found{"+answerid_+"}.";
                           res.jsonp(return_msg);
                       }
                        
                        });
                  }
          });
           console.log("Voting - impression is "+JSON.stringify(impression));
 
          if(action_==="undovote")
          {
              console.log("Voting - action is to undo vote." );
              var query = new Parse.Query(RolliPollVote);
              query.equalTo("impression", impression);
              query.equalTo("poll",poll);
              query.equalTo("IS_ACTIVE","Y");
                query.find({
                  success: function(items){  
                    console.log("Found votes 2: "+JSON.stringify(items)); 
                    var items_=[];
                    var suc_=[];
                    var err_ =[];
                    items.forEach(function(item){
                        item.set("IS_ACTIVE", "N"); 
                        items_.push(item);
                        suc_.push("Successfully saved item.");
                        err_.push("Error saving item.");
                      
                    });
                    exports.save_items(items_,suc_,err_).then(function(){
                            res.jsonp(  return_msg ) ;
                    }); 
                  },
                  error: function(err){
                      console.log(JSON.stringify(err)); 
                      return_msg['error']=JSON.stringify(err);
                      return_msg['status']='Error';
                     res.jsonp(  return_msg ) ;
                  }
                });
          }
               
        });   
  });
  
});
router.get('/poll/vote/impression/find/:iid', function(req,res){
  var iid=req.params.iid;
  
  if(iid && typeof(iid)!=='undefined'){
     var RolliPollImpression = Parse.Object.extend("RolliPollImpression");
     var query = new Parse.Query(RolliPollImpression);
      query.equalTo("objectId",iid);
      query.include("poll"); 
      query.find({
        success: function(impressions_){
          impressions_.forEach(function(imp){
              console.log(JSON.stringify(imp));
              console.log(JSON.stringify(imp.get("poll")));
              var poll_ = imp.get("poll");
              poll_.fetch( function(poll){
                exports.get_poll_answers(poll).then(function(answers_){

                      console.log("THEN "+JSON.stringify(poll));
                       exports.get_vote(poll,imp).then(function(votes){  
                        var m={};
                        m['vote']=votes;
                        m['impression']=imp;
                        m['answers']=answers_;
                        m['poll']=poll;
                        m['status']='Success';
                            res.status(200).jsonp(m);
                      });
                });
              });
               
   
          });
                 
        
        },
        error: function(err, impression){
          var m={};
          m['status']='Error';
          m['message']=err;
          res.status(500).jsonp(m);
        }
      });
  }
  else{
    console.log("id for impression is undefined.");
         var m={};
        m['status']="Error";
        m['message']="Error, id for impression is undefined";
        res.status(500).jsonp(m)
  }
});
router.get('/socialrpt/:pid', function(req,res){
      var pid = req.params.pid;
       exports.report_bitly_socialmedia_fbc(pid).then(function(social_response){  
            res.jsonp( social_response );
       });
});
router.get('/su/:url', function(req,res){
      var url = req.params.url;
       exports.save_bitly_external(url).then(function(bitly_response){  
//             res.setHeader('Content-Type', 'application/json');
            res.jsonp( bitly_response );
       });
});
router.get('/poll/impression/find/:qid/:url', function(req,res){
  var questionid  = req.params.qid;
    var url_ = req.params.url;
  if(questionid && typeof(questionid) !="undefined" && questionid!=="undefined")
    {
  console.log("REST: /poll/impression/find/:qid Retrieving poll with impression for id "+questionid);
  exports.get_poll_by_id(questionid,"false").then(function(poll){
    
     var RolliPollImpression = Parse.Object.extend("RolliPollImpression");
    
    
    exports.save_bitly_external(url_).then(function(bitly_response){  
          console.log("save_bitly_external");
        
        
      var query = new Parse.Query(RolliPollImpression);
      var rolliPollImpression_= new RolliPollImpression();
      console.log("save_bitly_external 1");
      
   
                 console.log("save_bitly_external 1.5");
//       console.log("ID is "+objPoll['objectId'] + " "+JSON.stringify(pollPointer) );
        var pointer = poll;   
      rolliPollImpression_.set("poll", poll ); 
        console.log("save_bitly_external4");
      query.equalTo("poll",  poll  );
        console.log("save_bitly_external5");
      console.log("Impression query on poll " );
      query.count({
        success: function(number) {
 exports.save_bitly_parse(bitly_response,poll).then(function(bitly_impression){
   
   
   console.log("REST: /poll/impression/find/:qid  "+questionid+" Impression "+(number+1));
            rolliPollImpression_.set("impression_value" , (number+1));
            rolliPollImpression_.set("poll",poll);
            rolliPollImpression_.set("bitly_impression",bitly_impression);
            rolliPollImpression_.save(null, {
                                                           success:function(impression_){

                                                                       console.log("Saved impression successfully "  );
                                                                 poll.set( "impression" , impression_);
                                                                 var items_=[];
                                                                  var suc_=[];
                                                                  var err_=[]; 
                                                                  items_.push(poll);
                                                                  suc_.push("Saved Impression successfully."); 
                                                                  err_.push("Error saving Poll."); 
                                                                  exports.save_items(items_,suc_,err_).then(function(){
                                                                    exports.get_poll_answers(poll).then(function(answers_){
                                                                      exports.get_vote(poll,impression_).then(function(votes){
                                                                        console.log("Finding impression : votes "+votes.length);       
                              res.jsonp({message: "success getting impression for "+questionid , poll: poll, impression: impression_, answers: answers_, vote: votes, bitly_impression: bitly_impression});                                                                  
 
                                                                        
                                                                      });
                                                                      
                                                                    }); 
                                                                  });                                                             
                                                                       
                                                           },
                                                            error: function(err){
                                                                      console.log("Failed to save impression "+JSON.stringify(err));
                                                                      
                                                            }
                    });              
   
   
   
   
   
   
 }); 
            
         
        },

        error: function(error) {
          console.log("Error on count "+error.message);
           exports.save_items(items_,suc_,err_).then(function(){
              res.jsonp({message: "error getting impression for "+questionid , poll: poll, impression: rolliPollImpression_});
            });
        }
      }); 
      
    });  
  });      
    }
   else
     {
       console.log("No question found - questionid is undefined"); 
       var m={};
       m['status']="Error";
       m['message']="Error, questionid is not defined.";
       res.jsonp(m);
     }
}); 
//exports.report=function(pollid)
router.get('/report/bitly/:pid' , function(request, response){
    exports.report_bitly(request.params.pid).then(function(items){
      response.status(200).jsonp({status:"Success", report: items});
    });
});
router.get('/report/:pid',function(request,response){
  console.log("REST : /report/:pid");
    var pollid=request.params.pid;
        exports.report(pollid,true).then(function(report_){
          exports.percent_filter(report_).then(function(r){ 
             response.status(200).jsonp( r );
          });
             
        }) ;
});
router.get('/report/runtime/:oid/:vc',function(request,response){
  var objectid = request.params.oid;
  var votecount = parseInt(request.params.vc);
  if(votecount > 0){
    exports.get_poll_by_id(objectid).then(function(poll){
                if(request.params.votecount > 0  || inactive_flag==="Y"){
                    var RolliPollVote = Parse.Object.extend("RolliPollVote");
                    var query_vote = new Parse.Query(RolliPollVote);
                    query_vote.equalTo("poll",poll);
                    if(inactive_flag==="N"){
                        query_vote.equalTo("IS_ACTIVE","Y");
                    }
                    console.log("Adding ascending");
                    query_vote.addAscending("createdAt");
                    console.log("First vote of asending list");
                    query_vote.first({
                        success: function(vote){ 
                            console.log(vote);
                            if(vote && vote.get("createdAt")){
                                console.log("Created at value for vote being returned "+vote.get("createdAt"));
                                        response.success( vote );
                            }
                            else{
                                    console.log("Poll date being returned "+poll.get("createdAt"))
                                        response.success(poll);
                            }
                                      
                        },
                        error: function(err,vote){
                            console.log("Error occurred on vote query.");
                            console.log(err);
                            response.error(err);
                        }
                    });
                }
                else{
                        response.success(poll);
                }
  });
  }
  
  
//    var RolliPoll = Parse.Object.extend("RolliPoll");
//     var query = new Parse.Query(RolliPoll);
//     var inactive_flag = request.params.inactive_flag?request.params.inactive_flag:"N";
//     query.equalTo("objectId" , request.params.pollid);
  
//     query.first({
//         success: function(poll){
//                 console.log("Have poll ");
//                 console.log(poll);
    
                      
//         },
//         error: function(err,poll){
//             response.error(err);
//         } 
//     });
  
  
//   Parse.Cloud.run('get_running_time', {pollid: objectid, inactive_flag :"N", votecount: votecount}, {
//     success: function(run_date) {
//       var ms_per_day = 1000*60*60*24;
//       var running_date = run_date['createdAt'];
//       var running_date_object = new Date(running_date);
//       var now_ = new Date();
 
//       console.log(running_date_object+" to "+now_);
 
//       var days_between = parseFloat(  (now_-running_date_object) / ms_per_day ).toFixed(1);

//       response.status(200).jsonp({status: "Success", running_date: run_date['createdAt'], running_days: days_between});
//     },
//     error: function(error) {
//       response.status(500).jsonp({status: "Error" , error: error } );
//     }
//   });
           
});
router.get('/user_settings/:oid', function(request,response){
   var user_oid_ = request.params.oid;
  Parse.Cloud.run('user_settings', {user_id: user_oid_}, {
    success: function(user_settings_) {
//       console.log(JSON.stringify(user_settings_));
      response.status(200).jsonp(user_settings_);
    },
    error: function(error) {
      response.status(500).send("Error calling cloud function "+JSON.stringify(error));
    }
  });
 
});
router.get('/valid_domains/:oid', function(request,response){
   var question_id_ = request.params.oid;
//   console.log("Valid domains oid ==>"+question_id_);
  
  redisClient.get("rp_vd."+question_id_ , function(err,reply){
        if(typeof reply === undefined || reply === null || reply.length === 0){
            Parse.Cloud.run('valid_domains', {question_id: question_id_}, {
              success: function(user_settings_) {
                console.log(JSON.stringify(user_settings_));
                redisClient.set("rp_vd."+question_id_,JSON.stringify(user_settings_));
                response.status(200).jsonp(user_settings_);
              },
              error: function(error) {
                response.status(500).send("Error calling cloud function "+JSON.stringify(error));
              }
            });
        }
        else{   
              console.log("Found Redis {"+question_id_+"}"+reply);
              var parseObject = Parse.Object.extend("RolliPollDefaultSettings"); 
              var instanceObject = new parseObject(JSON.parse(reply)); 
              response.status(200).jsonp(instanceObject); 
        }
  });

 
});
router.post('/profile/save/email/:eid/:oid',function(request,response){
 var poll_settings = request.body;
  var email_id_ = request.params.eid;
  var user_oid_ = request.params.oid;
  poll_settings.email=email_id_;
  poll_settings.user_id =user_oid_;
//   console.log(JSON.stringify(poll_settings));
  Parse.Cloud.run('user_update', poll_settings, {
    success: function(user_o_) {
      response.status(200).jsonp(user_o_);
    },
    error: function(error) {
      response.status(500).send("Error calling cloud function "+JSON.stringify(error));
    }
  });
 
});
router.post('/fb',function(request,response){
   response.statusCode = 302; 
    response.setHeader("Location", "/landingpage.html");
    response.end();
});
router.get('/fb',function(request,response){
   response.statusCode = 302; 
    response.setHeader("Location", "/landingpage.html");
    response.end();
});
 
router.post('/shorten/:qid/:iid',function(request,response){
  var poll_info = request.body;
  var qid=request.params.qid;
  var iid=request.params.iid;
//   console.log(JSON.stringify(request.body) );
//   console.log("Question id is "+qid);
//   console.log("/rollipoll/shorten shorten ..."+poll_info['url']);
//   console.log("Sending preview");
//   console.log("PRevinew completed");
  Bitly.shortenLink(poll_info['url'], function(err, results) {
//     console.log(JSON.stringify(err));
//     console.log(JSON.stringify(results));
        response.status(200).jsonp(results);
    });
});
router.get('/shortenedurl/:qid/:lurl', function(request,response){
  var pollid = request.params.qid;
   var fullUrl = request.params.lurl; 
  exports.get_bitly(pollid,fullUrl).then(function(bitly_){
      response.status(200).jsonp(bitly_);
  });
});
router.get('/pu/:url',function(request,response){
//       console.log("preview url "+request.params.url);
//   var ts_ = Math.floor(new Date() / 1000);
//    var fn_="RPPIC_"+ts_+".jpeg";
//   console.log("Writing to "+fn_);
//       webshot(request.params.url, function(err, renderStream) {
//               var file = fs.createWriteStream(fn_, {encoding: 'binary'});

  
//                 renderStream.on('data', function(data) {
//                   console.log("Sending to s3 aws.");
//                   exports.upload_aws(data, fn_);
// //                   response.redirect('https://s3.amazonaws.com/rollipoll/'+fn_);
// //                file.write(data.toString('binary'), 'binary');
//   //                console.log("Written out.");
//     //              console.log("Writing out the data to the response."); 
//                    response.write('<html><head><title>Generaged Image '+request.params.url+'</title></head><body><img src="https://s3.amazonaws.com/rollipoll/'+fn_+'"/></body></html>')
//    //                 response.write(new Buffer(data).toString('base64'));
//    //                 response.end('"/></body></html>');
                
//         }); 
//       });
exports.preview_upload_website(request.params.url).then(function(url){
  response.write('<html><head><title>Generaged Image '+request.params.url+'</title></head><body><img src="'+url+'"/></body></html>');
//   response.end('"/></body></html>');
});               
});
router.get('/staticfb/:oid/:bid/:aid',function(request,response){
       var oid = request.params.oid;
       var bid = request.params.bid;
       var aid = request.params.aid;
       var iid = request.params.iid;
       var answerText = null;
      var title=" ";
        console.log(oid+","+bid);
  console.log("Poll id is "+oid);
  exports.get_poll_by_id(oid).then(function(poll){
    var question = poll.get("question");
     exports.get_poll_answer(aid).then(function(answer){
        answerText = answer.get("text");
              title= 'I voted \''+answerText+'\'. '+question;
          exports.get_bitly_by_id(oid,bid).then(function(bitlyImpression){
            var url = bitlyImpression.get("url");//request.params.url;
            console.log("url is "+url);
 
            var desc = title;// "";//request.params.desc;
            var settingsImage =  poll.get("poll_settings")['social_image'];
            console.log("settings image "+settingsImage);
            
            response.setHeader('content-type', 'text/html; charset=UTF-8');
            console.log('render http://'+HOSTNAME_+'/rollipoll/staticfb/'+oid+'/'+bid +'/'+aid  );
            var contentHtml = '<html><head><title>'+title+'</title><meta http-equiv="content-type" content="text/html; charset=UTF-8">'+
                            '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>'+

                              ' <meta property="og:title" content="'+title+'">  <meta property="og:type" content="blog"> '+
                             ' <meta property="og:site_name" content="Rollipoll Website"> <meta property="og:description" content="'+desc+'"> '+
                            ' <meta property="og:url" content="http://'+HOSTNAME_+'/rollipoll/staticfb/'+oid+'/'+bid +'/'+aid +'"> ';
                            if(settingsImage!==null || typeof settingsImage!=='undefined'){
                                   contentHtml = contentHtml+ '<meta property="og:image" content="'+settingsImage+'" />' ;
                             }
//                                ' <meta property="og:url" content="'+url+'"> '+
                           
                           contentHtml = contentHtml+'<script>$(document).ready( function(){ '+
                            'if(navigator.userAgent.toLowerCase().indexOf("android") > -1){'+
                                'window.location.href = "'+url+'";'+ 
                             ' } if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){'+
                                '  window.location.href =  "'+url+'";'+ 
                             ' } else { $(location).attr("href","'+url+'"); }'+
 
                             ' })(jQuery)</script>'+
                              '</head><body></body></html>';
               response.end(contentHtml);
              });
   
    }); 
});
});
router.get('/twitter/:oid',function(request,response){
    var oid=request.params.oid;
    exports.search_twitter(oid).then(function(responseTwitter){
        console.log("Received "+JSON.stringify(responseTwitter));
       response.status(200).jsonp( responseTwitter );
    })
});
router.get('/staticftw/:oid/:bid/:aid',function(request,response){
       var oid = request.params.oid;
       var bid = request.params.bid;
       var aid = request.params.aid;
       var iid = request.params.iid;
       var answerText = null;
      var title="I voted ' '. Read the article and vote!";
        console.log(oid+","+bid);
  console.log("Poll id is "+oid);
  exports.get_poll_by_id(oid).then(function(poll){
    var question = poll.get("question");
     exports.get_poll_answer(aid).then(function(answer){
        answerText = answer.get("text");
              title= 'I voted \''+answerText+'\'.'+question+' Read the article and vote!';
          exports.get_bitly_by_id(oid,bid).then(function(bitlyImpression){
            var url = bitlyImpression.get("url");//request.params.url;
            console.log("url is "+url);
 
            var desc = title; //"";//request.params.desc;
            var settingsImage =  poll.get("poll_settings")['social_image'];
            console.log("settings image "+settingsImage);
            response.setHeader('content-type', 'text/html; charset=UTF-8');
            console.log('render http://'+HOSTNAME_+'/rollipoll/staticftw/'+oid+'/'+bid +'/'+aid  );
            var currentUrl = 'http://'+HOSTNAME_+'/rollipoll/staticftw/'+oid+'/'+bid +'/'+aid;
            
            exports.save_bitly_external(currentUrl).then(function(bitly_response){  
               var bitly_response_obj=JSON.parse(bitly_response); 
              console.log("Response is "+JSON.stringify(bitly_response));
              console.log("Bitly Shorten is "+bitly_response_obj['data']['url']);
                var contentHtml = '<html><head><title>'+title+'</title><meta http-equiv="content-type" content="text/html; charset=UTF-8">'+
                            '<script  type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>'+

                              ' <meta name="twitter:url" content="'+bitly_response_obj['data']['url']+'>'+
                              '<meta name="twitter:card" content="photo">  <meta name="twitter:title" content="'+title+'">  '+
                             ' <meta name="twitter:description" content="'+desc+'"> '+ 
                                                       '<script  type="text/javascript">$(document).ready( function(){ '+
                            'if(navigator.userAgent.toLowerCase().indexOf("android") > -1){'+
                                'window.location.href = "'+url+'";'+//http://play.google.com/store/apps/details?id=com.truecaller&hl=en';
                             ' } if(navigator.userAgent.toLowerCase().indexOf("iphone") > -1){'+
                                '  window.location.href =  "'+url+'";'+//'http://itunes.apple.com/lb/app/truecaller-caller-id-number/id448142450?mt=8';
                             ' } else { $(location).attr("href","'+url+'"); }'+
 
                             ' })(jQuery)</script>';
                             if(settingsImage !== null && typeof settingsImage !== "undefined") {
                               contentHtml = contentHtml +  ' <meta name="twitter:image" content="'+settingsImage+'" />';
                             }
                           
                              contentHtml = contentHtml + '</head><body></body></html>';
                          response.end(contentHtml);
            });
          
         });
   
    }); 
});
});
router.post('/save',function(request,response){
    console.log(JSON.stringify(request.body) );
    var poll = request.body;
     var message_r  ={status: "success"};   
  // Simple syntax to create a new subclass of Parse.Object.
  var query = new Parse.Query(Parse.User);
  query.equalTo("objectId", poll['user']['objectId']);  // find all the women
  query.first({
    success: function(u) {
        exports.save_poll(poll,u).then(function(poll_message){
                 
                  var message_r  ={status: "success"};
                  message_r['poll']=poll_message;
//                    console.log("Poll save success!");
//                    console.log(JSON.stringify(message_r) );
                  response.status(200).send(message_r);
 
        } );          

    }
  });
     
});
         


   var path = require('path'),
        fs = require('fs'),
        // Since Node 0.8, .existsSync() moved from path to fs:
        _existsSync = fs.existsSync || path.existsSync,
        formidable = require('formidable'),
        nodeStatic = require('node-static'),
        imageMagick = require('imagemagick'),
        options = {
            tmpDir: __dirname + '/tmp',
            publicDir: __dirname + '/public',
            uploadDir: __dirname + '/public/files',
            uploadUrl: '/files/',
            maxPostSize: 11000000000, // 11 GB
            minFileSize: 1,
            maxFileSize: 10000000000, // 10 GB
            acceptFileTypes: /.+/i,
            // Files not matched by this regular expression force a download dialog,
            // to prevent executing any scripts in the context of the service domain:
            inlineFileTypes: /\.(gif|jpe?g|png)$/i,
            imageTypes: /\.(gif|jpe?g|png)$/i,
            imageVersions: {
                'thumbnail': {
                    width: 80,
                    height: 80
                }
            },
            accessControl: {
                allowOrigin: '*',
                allowMethods: 'OPTIONS, HEAD, GET, POST, PUT, DELETE',
                allowHeaders: 'Content-Type, Content-Range, Content-Disposition'
            },
            /* Uncomment and edit this section to provide the service via HTTPS:
            ssl: {
                key: fs.readFileSync('/Applications/XAMPP/etc/ssl.key/server.key'),
                cert: fs.readFileSync('/Applications/XAMPP/etc/ssl.crt/server.crt')
            },
            */
            nodeStatic: {
                cache: 3600 // seconds to cache served files
            }
        },
        utf8encode = function (str) {
             
            return unescape(encodeURIComponent(str));
        },
        fileServer = new nodeStatic.Server(options.publicDir, options.nodeStatic),
        nameCountRegexp = /(?:(?: \(([\d]+)\))?(\.[^.]+))?$/,
        nameCountFunc = function (s, index, ext) {
            return ' (' + ((parseInt(index, 10) || 0) + 1) + ')' + (ext || '');
        },
        FileInfo = function (file) {
            this.name = file.name;
            this.size = file.size;
            this.type = file.type;
            this.deleteType = 'DELETE';
        },
        UploadHandler = function (req, res, callback) {
            this.req = req;
            this.res = res;
            this.callback = callback;
        },
        serve = function (req, res) {
            res.setHeader(
                'Access-Control-Allow-Origin',
                options.accessControl.allowOrigin
            );
            res.setHeader(
                'Access-Control-Allow-Methods',
                options.accessControl.allowMethods
            );
            res.setHeader(
                'Access-Control-Allow-Headers',
                options.accessControl.allowHeaders
            );
            var handleResult = function (result, redirect) {
                    if (redirect) {
                        res.writeHead(302, {
                            'Location': redirect.replace(
                                /%s/,
                                encodeURIComponent(JSON.stringify(result))
                            )
                        });
                        res.end();
                    } else {
                        res.writeHead(200, {
                            'Content-Type': req.headers.accept
                                .indexOf('application/json') !== -1 ?
                                        'application/json' : 'text/plain'
                        });
                        res.end(JSON.stringify(result));
                    }
                },
                setNoCacheHeaders = function () {
                    res.setHeader('Pragma', 'no-cache');
                    res.setHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
                    res.setHeader('Content-Disposition', 'inline; filename="files.json"');
                },
                handler = new UploadHandler(req, res, handleResult);
            switch (req.method) {
            case 'OPTIONS':
                res.end();
                break;
            case 'HEAD':
            case 'GET':
                console.log("GET");
                if (req.url === '/') {
                    setNoCacheHeaders();
                    if (req.method === 'GET') {
                        handler.get();
                    } else {
                        res.end();
                    }
                } else {
                    console.log("Fileserver.serve");
                    fileServer.serve(req, res);
                }
                break;
            case 'POST':
                console.log("POST");
                setNoCacheHeaders();
                handler.post();
                break;
            case 'DELETE':
                handler.destroy();
                break;
            default:
                res.statusCode = 405;
                res.end();
            }
        };
    fileServer.respond = function (pathname, status, _headers, files, stat, req, res, finish) {
        // Prevent browsers from MIME-sniffing the content-type:
        _headers['X-Content-Type-Options'] = 'nosniff';
        if (!options.inlineFileTypes.test(files[0])) {
            // Force a download dialog for unsafe file extensions:
            _headers['Content-Type'] = 'application/octet-stream';
            _headers['Content-Disposition'] = 'attachment; filename="' +
                utf8encode(path.basename(files[0])) + '"';
        }
        nodeStatic.Server.prototype.respond
            .call(this, pathname, status, _headers, files, stat, req, res, finish);
    };
    FileInfo.prototype.validate = function () {
        if (options.minFileSize && options.minFileSize > this.size) {
            this.error = 'File is too small';
        } else if (options.maxFileSize && options.maxFileSize < this.size) {
            this.error = 'File is too big';
        } else if (!options.acceptFileTypes.test(this.name)) {
            this.error = 'Filetype not allowed';
        }
        return !this.error;
    };
    FileInfo.prototype.safeName = function () {
        // Prevent directory traversal and creating hidden system files:
        this.name = path.basename(this.name).replace(/^\.+/, '');
        // Prevent overwriting existing files:
        while (_existsSync(options.uploadDir + '/' + this.name)) {
            this.name = this.name.replace(nameCountRegexp, nameCountFunc);
        }
    };
    FileInfo.prototype.initUrls = function (req) {
        if (!this.error) {
            var that = this,
                baseUrl = (options.ssl ? 'https:' : 'http:') +
                    '//' + req.headers.host + options.uploadUrl;
            this.url = this.deleteUrl = baseUrl + encodeURIComponent(this.name);
            Object.keys(options.imageVersions).forEach(function (version) {
                if (_existsSync(
                        options.uploadDir + '/' + version + '/' + that.name
                    )) {
                    that[version + 'Url'] = baseUrl + version + '/' +
                        encodeURIComponent(that.name);
                }
            });
        }
    };
    UploadHandler.prototype.get = function () {
        var handler = this,
            files = [];
      console.log("Reading directory fs.readdir");
        fs.readdir(options.uploadDir, function (err, list) {
            list.forEach(function (name) {
                var stats = fs.statSync(options.uploadDir + '/' + name),
                    fileInfo;
                if (stats.isFile() && name[0] !== '.') {
                    fileInfo = new FileInfo({
                        name: name,
                        size: stats.size
                    });
                    console.log("fileInfo.initUrls");
                    fileInfo.initUrls(handler.req);
                    files.push(fileInfo);
                }
            });
            handler.callback({files: files});
        });
    };
    UploadHandler.prototype.post = function () {
        var handler = this,
            form = new formidable.IncomingForm(),
            tmpFiles = [],
            files = [],
            map = {},
            counter = 1,
            redirect,
            finish = function () {
                counter -= 1;
                if (!counter) {
                    files.forEach(function (fileInfo) {
                        fileInfo.initUrls(handler.req);
                    });
                    handler.callback({files: files}, redirect);
                }
            };
        form.uploadDir = options.tmpDir;
        form.on('fileBegin', function (name, file) {
            console.log("******Pushing temp file "+file.path);
            console.log(handler.req);
            tmpFiles.push(file.path);
            var fileInfo = new FileInfo(file, handler.req, true);
            console.log("Safe Name is called");
            console.log(JSON.stringify(fileInfo));
            fileInfo.safeName();
            map[path.basename(file.path)] = fileInfo;
            console.log("Fils pushing "+JSON.stringify(fileInfo) );
            files.push(fileInfo);
        }).on('field', function (name, value) {
          console.log("on.field");
            if (name === 'redirect') {
                redirect = value;
            }
        }).on('file', function (name, file) {
                console.log("on.file map");
            console.log(JSON.stringify(map));
          
            var fileInfo = map[path.basename(file.path)];
            fileInfo.size = file.size;
            if (!fileInfo.validate()) {
                fs.unlink(file.path);
                return;
            }
            console.log("renameSync on "+file.path+" "+options.uploadDir+" "+fileInfo.name);
            fs.renameSync(file.path, options.uploadDir + '/' + fileInfo.name);
            if (options.imageTypes.test(fileInfo.name)) {
                Object.keys(options.imageVersions).forEach(function (version) {
                    counter += 1;
                    var opts = options.imageVersions[version];
                    imageMagick.resize({
                        width: opts.width,
                        height: opts.height,
                        srcPath: options.uploadDir + '/' + fileInfo.name,
                        dstPath: options.uploadDir + '/' + version + '/' +
                            fileInfo.name
                    }, finish);
                });
            }
        }).on('aborted', function () {
            tmpFiles.forEach(function (file) {
                fs.unlink(file);
            });
        }).on('error', function (e) {
            console.log(e);
        }).on('progress', function (bytesReceived, bytesExpected) {
          console.log('progress '+bytesReceived+','+bytesExpected);
            if (bytesReceived > options.maxPostSize) {
                handler.req.connection.destroy();
            }
        }).on('end', finish).parse(handler.req);
    };
    UploadHandler.prototype.destroy = function () {
        var handler = this,
            fileName;
        if (handler.req.url.slice(0, options.uploadUrl.length) === options.uploadUrl) {
            fileName = path.basename(decodeURIComponent(handler.req.url));
            if (fileName[0] !== '.') {
                fs.unlink(options.uploadDir + '/' + fileName, function (ex) {
                    Object.keys(options.imageVersions).forEach(function (version) {
                        fs.unlink(options.uploadDir + '/' + version + '/' + fileName);
                    });
                    handler.callback({success: !ex});
                });
                return;
            }
        }
        handler.callback({success: false});
    };
app.route('/uploadstd' ).post(function(req,res){
  console.log("uploadstd -->");
  console.log(req);
  console.log(req.addListener); 
         var handleResult = function (result, redirect) {
           console.log(JSON.stringify(result)); 
             var fn_='./public/files/'+result["files"][0]["name"] ;
              var renderStream = fs.createReadStream(fn_);
              var ts_ = Math.floor(new Date() / 1000);
              var fnp_="RPPIC_"+ts_;
               console.log("Upload "+fnp_+result["files"][0]["name"] +" to aws s3.");
                result['files'][0]['url']='https://s3.amazonaws.com/rollipoll/'+fnp_+result["files"][0]["name"] ;
               result['files'][0]['deleteUrl ']='https://s3.amazonaws.com/rollipoll/'+fnp_+result["files"][0]["name"] ;
console.log("Upload "+JSON.stringify(result));
           
              exports.upload_aws(renderStream, fnp_+result["files"][0]["name"] );
                    if (redirect) {
                        res.writeHead(302, {
                            'Location': redirect.replace(
                                /%s/,
                                encodeURIComponent(JSON.stringify(result))
                            )
                        });
                        res.end();
                    } else {
                        res.writeHead(200, {
                            'Content-Type': req.headers.accept
                                .indexOf('application/json') !== -1 ?
                                        'application/json' : 'text/plain'
                        });
                        res.end(JSON.stringify(result));
                    }
                };
 var handler = new UploadHandler(req, res , handleResult);
  handler.post();
  
//   res.jsonp({status: "success"});
});
app.route('/upload')
    .post(function (req, res, next) {

        var fstream;
        req.pipe(req.busboy);
         
        req.busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
          
            console.log("Uploading: " + filename);
console.log("UPLOAD");
            if(typeof file.fileRead === "undefined"){
              file.fileRead = [];
              filename = filename.split(' ').join('_');
            }
           
           file.on('data', function(data) {
//             console.log('File [' + fieldname + '] got ' + data.length + ' bytes');
             file.fileRead.push(data);
          }); 
           file.on('error', function(err) {
            console.log('Error while buffering the stream: ', err);
           });

            file.on('end', function() { 
              var finalBuffer = Buffer.concat(this.fileRead);
              var ts_ = Math.floor(new Date() / 1000);
              exports.upload_aws(finalBuffer,'RP_'+ts_+ filename );
               res.jsonp({
                      "files": [
                        {
                          "name": 'RP_'+ts_+ filename ,
                          "size": finalBuffer.length,
                          "type": mimetype,
                          "deleteType": "DELETE", 
                          "url": "https://s3.amazonaws.com/rollipoll/"+"RP_"+ts_+ filename ,
                          "deleteUrl ": "https://s3.amazonaws.com/rollipoll/"+"RP_"+ts_+ filename
                        }
                      ]
                    } );
            });
           // fstream.on('close', function () {    
           //     console.log("Upload Finished of " + filename);     
              
              
//                 res.send('fileuploaddone');           //where to go next
//             });
        });
       req.busboy.on('error', function(err) {
          console.error('Error while parsing the form: ', err);

        });
    });
// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use(application_context, router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log(application_name+' Listening on port {'+port+'} application context '+application_context);

